﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GalleryUIItem : MonoBehaviour
{
    public Image image;
    public Button button;
    public Text bottomHeaderText;

    //Material mat;
    public void Load(GalleryItemData data, System.Action onButton)
    {
        int number = LevelLoader.GetLevelCumulativeNumber(data.ai,data.li);
        //bottomHeaderText.text = string.Format("{0}  {1}", data.date, data.time);
        bottomHeaderText.text = string.Format("Level {0}", number);
        //if(mat == null)
        //{
        //    mat = new Material(Shader.Find("Sprites/Default"));
        //}
        //image.material = mat;
        string path = data.GetFullImagePath();

        if (File.Exists(path))
        {
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            Texture2D t2d = new Texture2D(256, 256);
            Sprite sp = Sprite.Create(t2d, new Rect(0, 0, 256, 256), new Vector2(128, 128));
            t2d.LoadImage(bytes);
            //mat.SetTexture("_MainTex", t2d);
            image.sprite = sp;
        }
        else
        {

            //mat.SetTexture("_MainTex", null);
            image.sprite = null;
        }


        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() =>
        {
            onButton?.Invoke();
            MainGameManager.instance.StartLevel(LoadType.GALLERY, data.ai, data.li, data.levelDefinition, Resources.Load<GameObject>(data.levelDefinition.resourcePath));
        });
    }
}
