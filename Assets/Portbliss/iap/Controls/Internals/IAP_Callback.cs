﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;
using System.IO;

namespace Portbliss.IAP.Internal
{
    public class IAP_Callback : MonoBehaviour, IStoreListener
    {
        internal event Func2 OnFireAnyCallback;
        static bool logEnabled;
        static IAP_Controller IAPController;
        internal void InjectDep(IAP_Controller iap_Controller)
        {
            IAPController = iap_Controller;
            logEnabled = iap_Controller.LogEnabled;
        }

        void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            LogUtil.Green("OnInitialized: PASS", logEnabled);
            IAPController.SetStoreData(controller, extensions);
            OnFireAnyCallback?.Invoke("", false, false);
        }

        void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            LogUtil.Red("OnInitializeFailed InitializationFailureReason:" + error, logEnabled);
            OnFireAnyCallback?.Invoke("", false, false);
        }

        PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs args)
        {
            LogUtil.Green("ProcessPurchase for id: " + args.purchasedProduct.definition.id, logEnabled);
            OnFireAnyCallback?.Invoke(args.purchasedProduct.definition.id, true, true);
            if (IAPController.IsThisNoAdProduct(args.purchasedProduct.definition.id))
            {
                IAP_Controller.hasAdPurchased = true;
                GameConfig.hasIAP_NoAdPurchasedHD.value = true;
            }
            
            Debug.Log("<color='red'>reciept is: "+ args.purchasedProduct.receipt);

            //string fPath = Path.Combine(Application.persistentDataPath, "recieptdata.txt");
            //File.WriteAllText(fPath, args.purchasedProduct.receipt);
            return PurchaseProcessingResult.Complete;
        }

        void IStoreListener.OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            string msg = string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason);
            LogUtil.Red(msg, logEnabled);
            OnFireAnyCallback?.Invoke(product.definition.storeSpecificId, true, false);
        }
    }
}