﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrippiesController : MonoBehaviour
{
    private MorfMesh cmesh;
    private Vector3[] vs;
   

    public DrippingPoint[] points;

    public Transform dropFromPosTR;


    private void Start()
    {
        cmesh = this.GetComponent<MorfMesh>();
        vs = cmesh.vertices;

        List<DrippingPoint> dpList = new List<DrippingPoint>();
        foreach (Transform item in transform)
        {
            DrippingPoint dp = item.GetComponent<DrippingPoint>();
            if (dp && dp.gameObject.activeSelf && dp.enabled)
            {
                dpList.Add(dp);
                dp.Initialize(cmesh,dropFromPosTR.position);
            }
           
        }
        points = dpList.ToArray();

        for (int i = 0; i < cmesh.vCount; i++)
        {
            DrippingPoint closestPoint = null;
            float minDistance = float.MaxValue;
            for (int j = 0; j < points.Length; j++)
            {
                float x = vs[i].x - points[j].position.x;
                float z = vs[i].z - points[j].position.z;
                float distanceFromPoint = Mathf.Sqrt(x*x+ z*z);
                if (distanceFromPoint <minDistance )//&& distanceFromPoint <points[j].maxRange)
                {
                    minDistance = distanceFromPoint;
                    closestPoint = points[j];
                }
            }

            if (closestPoint != null)
            {
                closestPoint.AddVertex(i);
            }
        }
        foreach (var item in points)
        {
            item.FinishInitialization();
        }

    }

    private void Update()
    {

        for (int i = 0; i < points.Length; i++)
        {
            points[i].OnControllerUpdate();
        }
    }
}
