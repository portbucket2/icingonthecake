﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;

public class LevelLoader : MonoBehaviour
{
    public bool testMode = false;
    public bool allLevelsUnlocked = false;
    public int numberofexceptions = 40;
    public GameObject commonPrefab;
    [Header("Common Setup")]
    public List<AreaData> areaData;

    internal List<AreaDefinition> areaDefinitions = new List<AreaDefinition>();

    //public HardData<int> starCount;
    //public HardData<int> areaIndex;
    //public HardData<int> currentLevelIndex;

    public static LevelLoader instance;

    public HardData<int> randomAreaIndex;
    public HardData<int> randomLevelIndex;

    public List<HardData<int>> randomlevelcollection;
    public HardData<int> LastRandomIndex;

    int csv_choice 
    {
        get 
        {
            return 0;// ABManager.GetValue(ABtype.Level_Order); 
        }
    }

    public void Awake()
    {
        if (instance)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            if (!testMode) allLevelsUnlocked = false;
            randomAreaIndex = new HardData<int>("RAI", 0);
            randomLevelIndex = new HardData<int>("RLI", 0);
            DontDestroyOnLoad(this.transform.root.gameObject);

            for (int i = 0; i < areaData.Count; i++)
            {
                areaDefinitions.Add(new AreaDefinition(areaData[i],i, csv_choice));
            }

            for (int a = 0; a < areaDefinitions.Count; a++)
            {
                AreaDefinition area = areaDefinitions[a];
                area.lastUnlockedLevelIndex = new HardData<int>(string.Format("UNLOCKED_PROGRSS_{0}", a), 0);
                area.stars = new List<HardData<int>>();
                for (int l = 0; l < area.levelDefinitions.Count; l++)
                {
                    area.stars.Add(new HardData<int>(string.Format("stars_{0}_{1}", a, l), 0));
                }
            }
            //Debug.Log("ad count" + areaDefinitions.Count);
            randomlevelcollection = new List<HardData<int>>();

            for (int i = 0; i < numberofexceptions * 2; i++)
            {
                randomlevelcollection.Add(new HardData<int>(string.Format("RANDOM_INDEX_{0}_{1}", (int)(i / 2), i % 2 == 0 ? "AREA" : "LEVEL"), -1));
            }
            LastRandomIndex = new HardData<int>("LAST_INDEX", 0);
        }


    }
    public void AddtoRandomException(int aI, int lI)
    {
        randomlevelcollection[LastRandomIndex.value * 2].value = aI;
        randomlevelcollection[LastRandomIndex.value * 2 + 1].value = lI;
    }

    public void changerandomindex()
    {
        if (LastRandomIndex.value >= numberofexceptions - 1)
        {
            LastRandomIndex.value = 0;
        }
        else
            LastRandomIndex.value++;
    }
    public int GetStarFor(LevelDefinition definition)
    {
        return areaDefinitions[definition.sequence_AI].stars[definition.sequence_LI].value;
    }
    public void SetStarFor(LevelDefinition definition, int star)
    {
        areaDefinitions[definition.sequence_AI].stars[definition.sequence_LI].value = star;
    }
    public int GetTotalStarCount()
    {

        int count = 0;
        foreach (AreaDefinition area in areaDefinitions)
        {
            foreach (HardData<int> star in area.stars)
            {
                count += star.value;
            }
        }
        return count;
    }
    public bool IsAreaUnlocked(int areaIndex)
    {
        if (areaDefinitions[areaIndex].starToEnter > GetTotalStarCount()) return false;
        if (areaIndex == 0) return true;
        else if (areaDefinitions[areaIndex - 1].lastUnlockedLevelIndex.value >= (areaDefinitions[areaIndex - 1].levelDefinitions.Count ))
        {
            return true;
        }
        else return false;
    }

    public static string GetLevelName(int areaI, int levelI)
    {
        int cumulativeIndex = 0;
        for (int i = 0; i < areaI; i++)
        {
            cumulativeIndex += instance.areaDefinitions[i].levelDefinitions.Count;
        }
        if (instance != null)
        {
            {

                return string.Format("Level {0}", cumulativeIndex + levelI + 1);

            }
        }
        else
        {
            return string.Format("Level {0}-{1}", areaI + 1, levelI + 1);
        }
    }
    public static int GetLevelCumulativeNumber(int areaI, int levelI)
    {
        if (instance != null)
        {
            int cumulativeIndex = 0;
            for (int i = 0; i < areaI; i++)
            {
                cumulativeIndex += instance.areaDefinitions[i].levelDefinitions.Count;
            }
            return cumulativeIndex + levelI + 1;
        }
        else
        {
            return levelI + 1;
        }
    }
    public int GetLastLevelCumulativeNumber()
    {
        return GetLevelCumulativeNumber(Last_ai, Last_li);
    }
    public void LoadTheLatestLevel()
    {
        LoadResourceAndLevel_Normal(Last_ai,Last_li);
    }
    public void LoadResourceAndLevel_Normal(int ai, int li)
    {
        LevelDefinition definition = FetchAppropriateDefinition(ai, li);
        GameObject resource = Resources.Load<GameObject>(definition.resourcePath);
        MainGameManager.instance.StartLevel(LoadType.NORMAL, ai,li, definition, resource);
    }

    public static LevelDefinition FetchAppropriateDefinition(int ai, int li)
    {

        if (ai == instance.areaDefinitions.Count - 1)
        {
            if (li >= instance.areaDefinitions[ai].levelDefinitions.Count)
            {
                ai = instance.randomAreaIndex.value;
                li = instance.randomLevelIndex.value;
            }
        }

        return instance.areaDefinitions[ai].levelDefinitions[li];
    }

    public static int Last_ai
    {
        get
        {

            if (!instance) return -1;


            int lastProperAreaIndex = 0;
            for (int i = 0; i < instance.areaDefinitions.Count; i++)
            {
                AreaDefinition area = instance.areaDefinitions[i];
                if (!instance.IsAreaUnlocked(i))
                {
                    return lastProperAreaIndex;
                }
                else if (area.levelDefinitions.Count > 0)
                {
                    lastProperAreaIndex = i;
                }
            }
            if (instance.areaDefinitions.Count == 0) return -1;
            else return lastProperAreaIndex;
        }
    }
    public static int Last_li
    {
        get
        {
            int ai = Last_ai;
            return instance.areaDefinitions[ai].lastUnlockedLevelIndex.value;
        }
    }

    public static int GetNextLevel_ai(int current_ai, int current_li)
    {
        AreaDefinition currentArea = instance.areaDefinitions[current_ai];
        if (current_li + 1 < currentArea.levelDefinitions.Count)
        {
            return current_ai;
        }
        else
        {
            if (current_ai + 1 < instance.areaDefinitions.Count)
            {
                return current_ai + 1;
            }
            else
            {
                return current_ai;
            }
        }
    }
    public static int GetNextLevel_li(int current_ai, int current_li)
    {
        AreaDefinition currentArea = instance.areaDefinitions[current_ai];
        if (current_li + 1 < currentArea.levelDefinitions.Count)
        {
            return current_li + 1;
        }
        else
        {
            if (current_ai + 1 < instance.areaDefinitions.Count)
            {
                return 0;
            }
            else
            {
                return current_li + 1;
            }
        }
    }

}

[System.Serializable]
public class AreaData
{
    public int starToEnter = 0;
    public TextAsset[] sequenceAndVariantData_CSV;
}
public class AreaDefinition
{
    public int starToEnter=0;
    public List<LevelDefinition> levelDefinitions;

    public AreaDefinition(AreaData data, int areaI, int csvChoice)
    {
        if (data == null) Debug.LogError("null prefab");

        starToEnter = data.starToEnter;
        levelDefinitions = new List<LevelDefinition>();
        CSVRow[] sequenceData = CSVReader.ReadCSVAsset(data.sequenceAndVariantData_CSV[csvChoice], '|');
        Debug.Log(sequenceData.Length + " data length");
        for (int sequence_LI = 0; sequence_LI < sequenceData.Length; sequence_LI++)
        {
            //int prefab_LI = int.Parse(sequenceData[sequence_LI].fields[0]) - 1;
            //string variant = sequenceData[sequence_LI].fields[2];
            levelDefinitions.Add(new LevelDefinition(sequenceData[sequence_LI], areaI,sequence_LI));
        }
    }

    [NonSerialized]
    public List<HardData<int>> stars;
    [NonSerialized]
    public HardData<int> lastUnlockedLevelIndex;
}

[System.Serializable]
public class LevelDefinition
{
    //public string title;
    //public string detail;
    //public int prefab_AI;
    //public int prefab_LI;
    public string title;
    public int initialColorIndex;
    public int sequence_AI;
    public int sequence_LI;

    public int areaPathID;
    public string phaseSequenceID;
    public string baseBreakdownID;
    public string seamTypeID;
    public string colorVariantID;
    public string decorationID;
    public string decorationColorVariantID;
    public string dataFolder_subPath;
    public string resourcePath;
    public string resourcePath_dataFolder;

    //public string decorationPath;

    public string worldAvgTimeText;

    public int GetLayerCount()
    {

        if (baseBreakdownID.Contains("T")) return 3;
        else if (baseBreakdownID.Contains("A")) return 2;
        else if (baseBreakdownID.Contains("S")) return 1;
        else return -1;
    }
    public string GetGamePlayType()
    {

        if (phaseSequenceID.Contains("Flat"))
        {
            if (phaseSequenceID.Contains("Ice")) return "ice & smooth";
            else return "smooth";
        }
        else return "error_gameplaytype";
    }

    public LevelDefinition()
    {
        //throw new Exception("This constructor is obsolete");
    }
    public LevelDefinition(CSVRow levelData, int areaI,int sequence_LI)//, int prefab_LI, string variant)
    {
        //this.prefab_AI = areaI;
        //this.prefab_LI = prefab_LI;//unique path
        this.sequence_AI = areaI;
        this.sequence_LI = sequence_LI;//sequential
        //this.variant = variant;
        initialColorIndex = 0;

        areaPathID = areaI + 1;
        for (int i = 0; i < levelData.fields.Length; i++)
        {
            switch (i)
            {
                case 0:
                    phaseSequenceID = levelData.fields[i];
                    break;
                case 1:
                    baseBreakdownID = levelData.fields[i];
                    break;
                case 2:
                    seamTypeID = levelData.fields[i];
                    dataFolder_subPath = levelData.fields[i];
                    break;
                case 3:
                    colorVariantID = levelData.fields[i];
                    break;
                case 4:
                    title = levelData.fields[i];
                    break;
                case 5:
                    worldAvgTimeText = levelData.fields[i];
                    break;
                case 6:
                    int.TryParse( levelData.fields[i],out initialColorIndex);
                    break;
                case 7:
                    decorationID = levelData.fields[i];
                    break;
                case 8:
                    decorationColorVariantID = levelData.fields[i];
                    break;
            }
        }
        //Debug.Log(colorVariantID);
        resourcePath = string.Format("Area {0}/{1}_{2}/{3}{4}", areaPathID, baseBreakdownID, phaseSequenceID, seamTypeID, colorVariantID);
        resourcePath_dataFolder = string.Format("Area {0}/{1}_{2}/{3}", areaPathID, baseBreakdownID, phaseSequenceID, dataFolder_subPath);
        //decorationPath = string.Format("Decorations/{0}/{1}", baseBreakdownID, decorationID);
        //resourcePath = string.Format("Area {0}/{1}_{2}/Level {4}", areaPathID, baseBreakdownID, phaseSequenceID, seamTypeID,colorVariantID);
        //resourcePath_dataFolder =  string.Format("Area {0}/{1}_{2}/{3}_{4}_data", areaPathIndex, baseBreakdownID, phaseSequenceID, seamTypeID, colorVariantID);
    }
    public string GetAssetPathForDecoID(string decoID)
    {
        return string.Format("Decorations/{0}/{1}", baseBreakdownID, decoID);
    }
}
