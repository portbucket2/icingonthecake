﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.IAP
{
    public class IAP_RestoreButtonControl : MonoBehaviour
    {
        [SerializeField] Button restoreIAP_Btn;
        [SerializeField] Button[] allIapButtons;
        [SerializeField] GameObject restoreLoadingEffect;
        [SerializeField] GameObject whiteBG;
        // Start is called before the first frame update
        void Start()
        {

#if UNITY_ANDROID
            restoreIAP_Btn.gameObject.SetActive(false);
#elif UNITY_IOS

            restoreIAP_Btn.gameObject.SetActive(!GameConfig.hasIAP_Restored_iOS.value);
            restoreIAP_Btn.onClick.RemoveAllListeners();
            restoreIAP_Btn.onClick.AddListener(() =>
            {
                restoreLoadingEffect.SetActive(true);
                IAP_Controller.instance.RestoreIAPForApple((success) =>
                {
                    restoreLoadingEffect.SetActive(false);
                    if(success)
                    {
                        restoreIAP_Btn.gameObject.SetActive(false);
                        foreach(var b in allIapButtons)
                        {
                            b.gameObject.SetActive(false); 
                        }
                    }
                });
            });
#endif
            if (GameConfig.hasIAP_NoAdPurchasedHD.value == true)
            {
                whiteBG.SetActive(false);
            }
            else
            {
                StartCoroutine(CheckForNoAdIAP());
            }
        }

        IEnumerator CheckForNoAdIAP()
        {
            while (IAP_Controller.hasAdPurchased == false)
            {
                yield return null;
            }
            whiteBG.SetActive(false);
        }
    }
}