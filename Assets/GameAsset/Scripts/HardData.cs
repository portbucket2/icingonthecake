﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace FishSpace
{
    public static class HardDataCleaner
    {
        public static void Clean()
        {
            HardData<int>.ResetAllHardData();
            HardData<float>.ResetAllHardData();
            HardData<string>.ResetAllHardData();
            HardData<bool>.ResetAllHardData();
        }
    }
    public class HardData<T>
    {
        string savedKey;
        T localValue;
        SettingTypes type;

        public static List<string> keyList = new List<string>();
        public static void ResetAllHardData()
        {
            foreach (string key in keyList)
            {
                PlayerPrefs.HasKey(key);
                PlayerPrefs.DeleteKey(key);
            }
        }

        public HardData(string Key, T initValue){
            #region defineType
            if (typeof(T) == typeof(bool)) 
            {
                type = SettingTypes._bool;
            }
            else if (typeof(T) == typeof(int)) 
            {
                type = SettingTypes._int;
            }
            else if (typeof(T) == typeof(float)) 
            {
                type = SettingTypes._float;
            }
            else if (typeof(T) == typeof(string)) 
            {
                type = SettingTypes._string;
            }
            else if ( typeof ( T ) == typeof ( DateTime ) )
            {
                type = SettingTypes._dateTime;
            }
            else 
            {
                type = SettingTypes._UNDEFINEDTYPE;
                Debug.LogError ("Undefined setting type!!!");
            }
            #endregion
            savedKey = Key;
            if (keyList.Contains(savedKey))
                Debug.LogWarningFormat("Duplicate keys: {0}!",savedKey);
            keyList.Add(savedKey);
            localValue = initValue;
            loadFromPref ();
        }

        public T value
        {
            set
            { 
                localValue = value;
                saveToPref ();
            }
            get
            {
                return localValue;
            }
        }
        public string GetKey()
        {
            return savedKey;
        }

        void saveToPref()
        {
            switch (type) {
                default:
                    Debug.LogError ("Pref saving not defined for this type");
                    break;
                case SettingTypes._bool:
                    {
                        bool locBoolValue = (bool)Convert.ChangeType (localValue, typeof(bool));
                        int prefValue = (locBoolValue ? 1 : 0);
                        PlayerPrefs.SetInt (savedKey, prefValue);
                    }
                    break;
                case SettingTypes._int:
                    {
                        int locValue = (int)Convert.ChangeType (localValue, typeof(int));
                        PlayerPrefs.SetInt (savedKey, locValue);
                    }
                    break;      
                case SettingTypes._float:
                    {
                        float locValue = (float)Convert.ChangeType (localValue, typeof(float));
                        PlayerPrefs.SetFloat (savedKey, locValue);
                    }
                    break;
                case SettingTypes._string:
                    {
                        string locValue = (string)Convert.ChangeType (localValue, typeof(string));
                        PlayerPrefs.SetString (savedKey, locValue);
                    }
                    break;
                case SettingTypes._dateTime:
                    {
                        //todo

                        DateTime dateValue = (DateTime) Convert.ChangeType ( localValue, typeof ( DateTime ) );

                        int year = dateValue.Year; int month = dateValue.Month; int day = dateValue.Day;
                        int hour = dateValue.Hour; int minute = dateValue.Minute; int second = dateValue.Second;
                        //var s = "2006-10-31T11:17:50Z";
                        //var t = typeof ( DateTime );
                        //var d = Convert.ChangeType ( s, t, CultureInfo.InvariantCulture );

                        //if ( t == typeof ( DateTime ) )
                        //    d = TimeZoneInfo.ConvertTimeToUtc ( (DateTime) d );

                        //Console.WriteLine ( d.ToString () );
                        string dateValueStr = year + "##" + month + "##" + day + "##" + hour + "##" + minute + "##" + second;
                        //Debug.LogWarning ( "data to be saved: " + dateValueStr );
                        //string locValue = (string) Convert.ChangeType ( localValue, typeof ( string ) );
                        PlayerPrefs.SetString ( savedKey, dateValueStr );
                    }
                    break;
            }
        }
        void loadFromPref()
        {
            switch (type) {
                default:
                    Debug.LogError ("Pref loading not defined for this type");
                    break;
                case SettingTypes._bool:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        int prefValue = PlayerPrefs.GetInt (savedKey);
                        bool prefBool = (prefValue != 0);
                        localValue = (T) Convert.ChangeType (prefBool, typeof(T));
                    }
                    break;
                case SettingTypes._int:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        int prefValue = PlayerPrefs.GetInt (savedKey);
                        localValue = (T) Convert.ChangeType (prefValue, typeof(T));
                    }
                    break;
                case SettingTypes._float:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        float prefValue = PlayerPrefs.GetFloat (savedKey);
                        localValue = (T) Convert.ChangeType (prefValue, typeof(T));
                    }
                    break;
                case SettingTypes._string:
                    if (PlayerPrefs.HasKey (savedKey)) {
                        string prefValue = PlayerPrefs.GetString (savedKey);
                        localValue = (T) Convert.ChangeType (prefValue, typeof(T));
                    }
                    break;
                case SettingTypes._dateTime:
                    if ( PlayerPrefs.HasKey ( savedKey ) )
                    {
                        //todo
                        string prefValue = PlayerPrefs.GetString ( savedKey );
                        string[] allStrs = Regex.Split ( prefValue, "##" );
                        int year = GetValue ( allStrs[0] );
                        int month = GetValue ( allStrs[1] );
                        int day = GetValue ( allStrs[2] );
                        int hour = GetValue ( allStrs[3] );
                        int minute = GetValue ( allStrs[4] );
                        int second = GetValue ( allStrs[5] );

                        DateTime dateValue = new DateTime ( year, month, day, hour, minute, second );
                        Debug.LogWarning ( "the date data from player pref: " + dateValue );
                        localValue = (T) Convert.ChangeType ( dateValue, typeof ( T ) );
                    }
                    break;
            }
        }

        int GetValue ( string valueStr )
        {
            int v = -1;
            int.TryParse ( valueStr, out v );
            return v;
        }

        public enum SettingTypes
        {
            _bool,
            _int,
            _float,
            _string,
            _dateTime,
            _UNDEFINEDTYPE
        }
    }

}