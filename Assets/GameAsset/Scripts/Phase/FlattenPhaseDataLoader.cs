﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlattenPhaseDataLoader : ScriptableObject
{
    public List<MeshCol> meshColKeeps = new List<MeshCol>();
    //public List<Mesh> meshList = new List<Mesh>();
   // public List<Color[]> colors = new List<Color[]>();
}

[System.Serializable]
public class MeshCol
{
    public Color[] vcols;
    public MeshCol()
    {

    }
    public MeshCol(Color[] cols)
    {
        vcols = cols;
    }
}