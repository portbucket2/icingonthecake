﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EasyButtonMenuController : MonoBehaviour
{
    [SerializeField] Button mainButton;
    [SerializeField] Button cancelButton;
    [SerializeField] GameObject content;
    private void Awake()
    {
        mainButton.onClick.AddListener(()=>content.SetActive(true));
        cancelButton.onClick.AddListener(() => content.SetActive(false));
    }
}
