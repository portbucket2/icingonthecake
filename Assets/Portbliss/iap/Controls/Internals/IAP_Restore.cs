﻿using System;
using FRIA;
using UnityEngine.Purchasing;

namespace Portbliss.IAP.Internal
{
    internal static class IAP_Restore
    {
        static bool logEnabled;
        static IAP_Controller IAPController;
        internal static bool IsRestoring;
        internal static void InjectDep(IAP_Controller iAPController)
        {
            IAPController = iAPController;
            logEnabled = iAPController.LogEnabled;
            IsRestoring = false;
        }

        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        internal static void RestorePurchases(Action<bool> OnRestore)
        {
#if UNITY_IOS

            // ... begin restoring purchases
            LogUtil.Green("RestorePurchases started ...", logEnabled);

            // Fetch the Apple store-specific subsystem.
            var apple = IAPController.StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            IsRestoring = true;
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                LogUtil.Green("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.", logEnabled);
                OnRestore?.Invoke(result);
                IsRestoring = false;
                GameConfig.hasIAP_Restored_iOS.value = true;
            });
#else
            OnRestore?.Invoke(true);
#endif
        }
    }
}