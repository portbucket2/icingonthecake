﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldIceMesh : MonoBehaviour
{
    public  Mesh m;
    public Vector3[] vertices;
    public Vector3[] normals;
    public void Init(Mesh m)
    {
        this.m = m;
        vertices = m.vertices;
        normals = m.normals;
    }
}
