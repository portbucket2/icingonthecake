using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using FRIA;
using Portbliss.IAP.Internal;
using System.Collections;
using Portbliss.Ad;
using UnityEngine.UI;
using UnityEngine.Purchasing.Security;
using com.adjust.sdk.purchase;
using com.adjust.sdk;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
namespace Portbliss.IAP
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class IAP_Controller : MonoBehaviour
    {
        public static bool hasAdPurchased { get; set; }
        const bool logEnabled = true;
        [SerializeField] bool useConsumable = false, useNonConsumable = false, useSubscription = false;
        [SerializeField] List<string> consumableProdIDs_Android = new List<string>(), nonConsumableProdIDs_Android = new List<string>();
        [SerializeField] List<SubscriptionDesc> subscriptionDescriptions_Android = new List<SubscriptionDesc>();

        [SerializeField] List<string> consumableProdIDs_iOS = new List<string>(), nonConsumableProdIDs_iOS = new List<string>();
        [SerializeField] List<SubscriptionDesc> subscriptionDescriptions_iOS = new List<SubscriptionDesc>();

        public static IAP_Controller instance;
        IStoreController con;
        IExtensionProvider prov;
        internal IStoreController StoreController { get { return con; } }
        internal IExtensionProvider StoreExtensionProvider { get { return prov; } }
        internal void SetStoreData(IStoreController conArg, IExtensionProvider provArg) { this.con = conArg; this.prov = provArg; }
        internal bool IsConsumableValid(string prodID)
        {
            List<string> consumableProdIDs = consumableProdIDs_Android;
#if UNITY_ANDROID
            consumableProdIDs = consumableProdIDs_Android;
#else
            consumableProdIDs = consumableProdIDs_iOS;
#endif
            return consumableProdIDs.Contains(prodID);
        }

        internal bool IsNonConsumableValid(string prodID)
        {
            List<string> nonConsumableProdIDs = nonConsumableProdIDs_Android;
#if UNITY_ANDROID
            nonConsumableProdIDs = nonConsumableProdIDs_Android;
#else
            nonConsumableProdIDs = nonConsumableProdIDs_iOS;
#endif
            return nonConsumableProdIDs.Contains(prodID);
        }

        internal bool IsSubscriptionValid(string prodID)
        {
            List<SubscriptionDesc> subscriptionDescriptions = subscriptionDescriptions_Android;
#if UNITY_ANDROID
            subscriptionDescriptions = subscriptionDescriptions_Android;
#else
            subscriptionDescriptions = subscriptionDescriptions_iOS;
#endif

            var obj = subscriptionDescriptions.Find((desc) => { return desc.SubscriptionProdID == prodID; });
            return !(obj == null);
        }
        internal string CurrentlyBeingPurchasedProductID { get; private set; }
        internal bool LogEnabled { get { return logEnabled; } }

        Action<bool> OnComplete;
        [SerializeField] GameObject loadingEffect, topMostScreenBlocker, waitingObj;
        bool IsInitialized { get { return instance == null ? false : 
                    (instance.StoreController != null && instance.StoreExtensionProvider != null); } }
        IAP_Consumable iap_consumable;
        IAP_NonConsumable iap_nonConsumable;
        IAP_Subscription iap_subscription;
        IAP_Callback callbackScript;
        [SerializeField] string noAdNonConsumableID_IfAny_Android, noAdNonConsumableID_IfAny_iOS;
        string noAdNonConsumableID_IfAny;
        [SerializeField] bool testModeDelayNoAdPurchaseOn = true;
        [SerializeField] Button buyBtn, cancelBtn;
        [Space(10)]
        [SerializeField] bool useValidationForNonconsumableAndSubscription;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartIAP_System();
                DontDestroyOnLoad(this);
            }
            else
            {
                DestroyImmediate(this);
            }
        }

        public bool IsThisNoAdProduct(string prod)
        {
#if UNITY_ANDROID
            return prod == noAdNonConsumableID_IfAny_Android;
#else
            return prod == noAdNonConsumableID_IfAny_iOS;
#endif
        }

        void StartIAP_System()
        {
            
            loadingEffect.SetActive(false);
            hasAdPurchased = false;
            topMostScreenBlocker.SetActive(false);
            List<string> nonConsumableProdIDs = nonConsumableProdIDs_Android;
#if UNITY_ANDROID
            nonConsumableProdIDs = nonConsumableProdIDs_Android;
            noAdNonConsumableID_IfAny = noAdNonConsumableID_IfAny_Android;
#else
            nonConsumableProdIDs = nonConsumableProdIDs_iOS;
            noAdNonConsumableID_IfAny = noAdNonConsumableID_IfAny_iOS;
#endif

            if (nonConsumableProdIDs.Contains(noAdNonConsumableID_IfAny) == false)
            {
                nonConsumableProdIDs.Add(noAdNonConsumableID_IfAny);
            }

            if (StoreController == null)
            {
                LogUtil.Green("Now we will try to initialize IAP.", logEnabled);
                if (IsInitialized)
                {
                    return;
                }
                LogUtil.Yellow("IAP not initialized, so we will really init it.", logEnabled);
                var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
                if (useConsumable)
                {
                    List<string> consumableProdIDs = consumableProdIDs_Android;
#if UNITY_ANDROID
                    consumableProdIDs = consumableProdIDs_Android;
#else
                    consumableProdIDs = consumableProdIDs_iOS;
#endif

                    foreach (var p in consumableProdIDs)
                    {
                        LogUtil.Green("adding consumable prod ID:" + p + "", logEnabled);
                        builder.AddProduct(p, ProductType.Consumable);
                    }
                }

                if (useNonConsumable)
                {
#if UNITY_ANDROID
                    nonConsumableProdIDs = nonConsumableProdIDs_Android;
#else
                    nonConsumableProdIDs = nonConsumableProdIDs_iOS;
#endif
                    foreach (var p in nonConsumableProdIDs)
                    {
                        LogUtil.Green("adding non-consumable prod ID:" + p + "", logEnabled);
                        builder.AddProduct(p, ProductType.NonConsumable);
                    }
                }

                if (useSubscription)
                {
                    List<SubscriptionDesc> subscriptionDescriptions = subscriptionDescriptions_Android;
#if UNITY_ANDROID
                    subscriptionDescriptions = subscriptionDescriptions_Android;
#else
                    subscriptionDescriptions = subscriptionDescriptions_iOS;
#endif

                    foreach (var d in subscriptionDescriptions)
                    {
                        LogUtil.Green("adding subscription prod ID:" + d.SubscriptionProdID + "", logEnabled);
                        builder.AddProduct(d.SubscriptionProdID, ProductType.Subscription, new IDs(){
                { d.AppleSubscriptionName, AppleAppStore.Name },
                { d.GoogleSubscriptionName, GooglePlay.Name },
            });
                    }
                }

                LogUtil.Green("calling IAP API init...", logEnabled);
                iap_consumable = new IAP_Consumable(this);
                iap_nonConsumable = new IAP_NonConsumable(this);
                iap_subscription = new IAP_Subscription(this);
                callbackScript = GetComponent<IAP_Callback>();
                if (callbackScript == null)
                {
                    callbackScript = gameObject.AddComponent<IAP_Callback>();
                }
                callbackScript.InjectDep(this);
                IAP_Core.InjectDep(this);
                IAP_Restore.InjectDep(this);
                UnityPurchasing.Initialize(callbackScript, builder);
            }
            callbackScript.OnFireAnyCallback += CallbackScript_OnFireAnyCallback;
            
        }

        void OnDisable()
        {
            callbackScript.OnFireAnyCallback -= CallbackScript_OnFireAnyCallback;
        }

        void CallbackScript_OnFireAnyCallback(string productID, bool isItPurchaseEvent, bool purchaseSuccess)
        {
            if (string.IsNullOrEmpty(productID) == false
                && isItPurchaseEvent && CurrentlyBeingPurchasedProductID == productID)
            {
                if (purchaseSuccess)
                {
                    LogUtil.Green("from callback, ExitIAP_WithSuccess() for id: " + productID, logEnabled);
                    ExitIAP_WithSuccess(productID);
                }
                else
                {
                    LogUtil.Red("from callback, ExitIAP_Withfail() for id: " + productID, logEnabled);
                    ExitIAP_Withfail();
                }
            }
        }

        internal void BuyIAP_Product<T>(string productID, Action<bool> OnComplete)
            where T : IIAP_Buy
        {
            instance.OnComplete = OnComplete; instance.CurrentlyBeingPurchasedProductID = productID;
            LogUtil.Green("let us start the full IAP process for id: " + productID, logEnabled);
            if (IsInitialized == false)
            {
                LogUtil.Red("BuyProductID FAIL. Not initialized.", logEnabled);
                ExitIAP_Withfail();
            }
            else
            {
                GetProperIAP_Object<T>().BuyProduct(productID);
            }
        }

        void Buy_NoAdCore(Action<bool> OnComplete)
        {
            BuyIAP_Product<IAP_NonConsumable>(noAdNonConsumableID_IfAny, (success) =>
            {
                if (success)
                {
                    GameConfig.hasIAP_NoAdPurchasedHD.value = true;
                    LogUtil.Yellow("Buy_NoAdCore e 'GameConfig.hasIAP_NoAdPurchasedHD.value = true' set hoise", true);
                    AdController.HideBanner();
                }
                OnComplete?.Invoke(success);
            });
        }

        public void Buy_NoAd(Action<bool> OnComplete)
        {
            loadingEffect.SetActive(true);
            topMostScreenBlocker.SetActive(false);
            buyBtn.interactable = true;
            cancelBtn.interactable = true;
            waitingObj.SetActive(false);
            buyBtn.onClick.RemoveAllListeners();
            buyBtn.onClick.AddListener(() =>
            {
                buyBtn.interactable = false;
                cancelBtn.interactable = false;
                topMostScreenBlocker.SetActive(true);
                waitingObj.SetActive(true);
                if (testModeDelayNoAdPurchaseOn)
                {
                    WaitXSeconds(4, () =>
                    {
                        Buy_NoAdCore((success) =>
                        {
                            buyBtn.interactable = true;
                            cancelBtn.interactable = true;
                            loadingEffect.SetActive(false);
                            topMostScreenBlocker.SetActive(false);
                            waitingObj.SetActive(false);
                            OnComplete?.Invoke(success);

                            if(success && useValidationForNonconsumableAndSubscription == false)
                            {
                                Product product = StoreController.products.WithID(noAdNonConsumableID_IfAny);
                                AnalyticsAssistant.LogPurchase(product.metadata.isoCurrencyCode, product.metadata.localizedPrice + "", product.transactionID);
                            }

                        });
                    });
                }
                else
                {
                    waitingObj.SetActive(true);
                    Buy_NoAdCore((success) =>
                    {
                        buyBtn.interactable = true;
                        cancelBtn.interactable = true;
                        loadingEffect.SetActive(false);
                        topMostScreenBlocker.SetActive(false);
                        waitingObj.SetActive(false);
                        OnComplete?.Invoke(success);

                        if (success && useValidationForNonconsumableAndSubscription == false)
                        {
                            Product product = StoreController.products.WithID(noAdNonConsumableID_IfAny);
                            AnalyticsAssistant.LogPurchase(product.metadata.isoCurrencyCode, product.metadata.localizedPrice + "", product.transactionID);
                        }
                    });
                }
            });

            cancelBtn.onClick.RemoveAllListeners();
            cancelBtn.onClick.AddListener(() =>
            {
                loadingEffect.SetActive(false);
            });
        }

        IIAP_Buy GetProperIAP_Object<T>()
        {
            if (typeof(T) == typeof(IAP_Consumable)) { return iap_consumable; }
            else if (typeof(T) == typeof(IAP_NonConsumable)) { return iap_nonConsumable; }
            else { return iap_subscription; }
        }

        public void RestoreIAPForApple(Action<bool> OnRestore = null)
        {
            // If Purchasing has not yet been set up ...
            bool wrongPlatform = !(Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer);
            if (!instance.IsInitialized || wrongPlatform)
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                LogUtil.Red("RestorePurchases FAIL. Not initialized or wrong platform.", logEnabled);
                if (wrongPlatform) { OnRestore?.Invoke(true); }
                else { OnRestore?.Invoke(false); }
                return;
            }
            else
            {
                LogUtil.Green("lets us initiate IAP restore process for apple platform", logEnabled);
                IAP_Restore.RestorePurchases(OnRestore);
            }
        }
        
        internal void ExitIAP_Withfail()
        {
            LogUtil.Red("ExitIAP_Withfail()", logEnabled);
            OnComplete?.Invoke(false);
            OnComplete = null;
            loadingEffect.SetActive(false);
            CurrentlyBeingPurchasedProductID = "";
        }

        internal void ExitIAP_WithSuccess(string productID)
        {
            Dictionary<string, string> additionalParametes = new Dictionary<string, string>();
            Product product = StoreController.products.WithID(productID);
            string price = "";
            string currency = "";
            if (product != null && product.metadata != null)
            {
                price = product.metadata.localizedPrice + "";
                currency = product.metadata.isoCurrencyCode;
            }

            bool willDoValidation = (product.definition.type == ProductType.NonConsumable || product.definition.type == ProductType.Subscription)
                    && useValidationForNonconsumableAndSubscription;
            bool allDataOk = false;

#if UNITY_ANDROID

            IAP_RecieptObject_Android recieptObject = null;
            IAP_PayloadObject_Android payloadObj = null;

            if (product != null)
            {
                try
                {
                    recieptObject = JsonUtility.FromJson<IAP_RecieptObject_Android>(product.receipt);
                }
                catch (Exception ex1)
                {
                    LogUtil.Red("'IAP_RecieptObject_Android' object json perse error: " + ex1.Message, logEnabled);
                }

                if (recieptObject != null)
                {
                    try
                    {
                        payloadObj = JsonUtility.FromJson<IAP_PayloadObject_Android>(recieptObject.Payload);
                    }
                    catch (Exception ex2)
                    {
                        LogUtil.Red("'IAP_PayloadObject_Android' object json perse error: " + ex2.Message, logEnabled);
                    }
                }
            }

            allDataOk = product != null && recieptObject != null && payloadObj != null && product.hasReceipt
                && string.IsNullOrEmpty(product.receipt) == false && productID == CurrentlyBeingPurchasedProductID
                && string.IsNullOrEmpty(payloadObj.json) == false && string.IsNullOrEmpty(payloadObj.signature) == false
                && string.IsNullOrEmpty(product.metadata.localizedPrice + "") == false
                && string.IsNullOrEmpty(product.metadata.isoCurrencyCode) == false;
#elif UNITY_IOS

            allDataOk = product != null && product.hasReceipt
                && string.IsNullOrEmpty(product.receipt) == false && productID == CurrentlyBeingPurchasedProductID
                && string.IsNullOrEmpty(product.metadata.localizedPrice + "") == false
                && string.IsNullOrEmpty(product.metadata.isoCurrencyCode) == false;
#endif
            

            if (willDoValidation)
            {
                
                if (allDataOk)
                {
                    var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
        AppleTangle.Data(), Application.identifier);

#if UNITY_ANDROID
                    LogUtil.Red("now we will try to validate IAP with adjust sdk! platform: android", true);
                    // Purchase verification request on Android.
                    GooglePlayReceipt gReceipt = null;
                    var g_result = validator.Validate(product.receipt);
                    foreach (IPurchaseReceipt r in g_result)
                    {
                        if (r.productID != productID) { continue; }
                        gReceipt = r as GooglePlayReceipt;
                        LogUtil.Red("we have got the reciept data!", true);
                        break;
                    }

                    if (gReceipt == null)
                    {
                        LogUtil.Red("google reciept object is null!! ERROR!!!!!!!!!", true);
                        OnComplete?.Invoke(true);
                        OnComplete = null;
                        loadingEffect.SetActive(false);
                        CurrentlyBeingPurchasedProductID = "";
                    }
                    else
                    {
                        
                        AdjustPurchase.VerifyPurchaseAndroid(productID, gReceipt.purchaseToken, recieptObject.Payload, (verificationInfo) => {

                            LogUtil.Red("so we varified this purchase on android and lets see the result from verification process - message: " + verificationInfo.Message
                                + " and status code: " + verificationInfo.StatusCode + " and verification state: " + verificationInfo.VerificationState, true);

                            if (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed)
                            {

                                string purchaseEventTokenAndroid = "mzap7z";
                                AdjustEvent adjustEvent = new AdjustEvent(purchaseEventTokenAndroid);
                                double revAmount = 0;
                                double.TryParse(price, out revAmount);
                                adjustEvent.setRevenue(revAmount, currency);
                                adjustEvent.setTransactionId(recieptObject.TransactionID);
                                Adjust.trackEvent(adjustEvent);
                                LogUtil.Red("We have told the adjust sdk to report revenue after IAP verification. platform: android", true);
                            }

                            Debug.Log("<color='red'>purchase validated for id: " + productID + "</color>");
                            LogUtil.Red("ExitIAP_WithSuccess() for id: " + productID + " after validation, success? "
                                + (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed), logEnabled);
                            OnComplete?.Invoke(verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed);
                            OnComplete = null;
                            loadingEffect.SetActive(false);
                            CurrentlyBeingPurchasedProductID = "";
                        });
                    }

                    
#elif UNITY_IOS
                    LogUtil.Red("now we will try to validate IAP with adjust sdk! platform: iOS", true);
                    // Purchase verification request on iOS.
                    AdjustPurchase.VerifyPurchaseiOS(product.receipt, product.transactionID, productID, (verificationInfo) =>
                    {
                        LogUtil.Red("so we varified this purchase on iOS and lets see the result from verification process - message: " + verificationInfo.Message
                            + " and status code: " + verificationInfo.StatusCode + " and verification state: " + verificationInfo.VerificationState, true);

                        if (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed)
                        {
                            string purchaseEventTokeniOS = "9m1js7";
                            AdjustEvent adjustEvent = new AdjustEvent(purchaseEventTokeniOS);
                            double revAmount = 0;
                            double.TryParse(price, out revAmount);
                            adjustEvent.setRevenue(revAmount, currency);
                            adjustEvent.setTransactionId(product.transactionID);
                            Adjust.trackEvent(adjustEvent);
                            LogUtil.Red("We have told the adjust sdk to report revenue after IAP verification. platform: iOS", true);
                        }

                        Debug.Log("<color='red'>purchase validated for id: " + productID + "</color>");
                        LogUtil.Red("ExitIAP_WithSuccess() for id: " + productID + " after validation, success? "
                            + (verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed), logEnabled);
                        OnComplete?.Invoke(verificationInfo.VerificationState == ADJPVerificationState.ADJPVerificationStatePassed);
                        OnComplete = null;
                        loadingEffect.SetActive(false);
                        CurrentlyBeingPurchasedProductID = "";
                    });
#endif
                }
                else
                {
                    ValidationfailCall(productID);
                }
                
            }
            else
            {
                LogUtil.Green("ExitIAP_WithSuccess() for id: " + productID + ". No validation required!", logEnabled);
                OnComplete?.Invoke(true);
                OnComplete = null;
                loadingEffect.SetActive(false);
                CurrentlyBeingPurchasedProductID = "";
            }
        }

        void ValidationfailCall(string productID)
        {
            LogUtil.Green("ExitIAP_WithSuccess() for id: " + productID+" before validation fail.", logEnabled);
            OnComplete?.Invoke(false);
            OnComplete = null;
            loadingEffect.SetActive(false);
            CurrentlyBeingPurchasedProductID = "";
        }

        internal void WaitForRestoreIfAny(Action OnComplete)
        {
            StartCoroutine(WaitForRestore(OnComplete));
        }

        IEnumerator WaitForRestore(Action OnComplete)
        {
            while (IAP_Restore.IsRestoring)
            {
                yield return null;
            }
            OnComplete?.Invoke();
        }

        internal static bool HasRestoredIAP()
        {
            return GameConfig.hasIAP_Restored_iOS.value;
        }

        IEnumerator WaitForInitializationCOR(Action OnComplete)
        {
            while (IsInitialized == false)
            {
                yield return null;
            }
            OnComplete?.Invoke();
        }

        void WaitForInit(Action OnComplete)
        {
            StartCoroutine(WaitForInitializationCOR(OnComplete));
        }

        void WaitXSeconds(float delay, Action OnComplete)
        {
            StartCoroutine(WaitXSecondsCOR(delay, OnComplete));
        }

        IEnumerator WaitXSecondsCOR(float delay, Action OnComplete)
        {
            yield return new WaitForSeconds(delay);
            OnComplete?.Invoke();
        }

        public void IsProductPurchased(string productID, Action<bool> OnGetPurchaseInfo)
        {
            WaitForInit(() =>
            {
                Product p = instance.StoreController.products.WithID(productID);
                bool purchased = p.hasReceipt == true && string.IsNullOrEmpty(p.transactionID) == false;
                OnGetPurchaseInfo?.Invoke(purchased);
            });
        }

        public void IsNoAdPurchased(Action<bool> OnGetPurchaseInfo)
        {
            IsProductPurchased(noAdNonConsumableID_IfAny, OnGetPurchaseInfo);
        }

        internal bool IsProductPurchased(string productID)
        {
            Product p = instance.StoreController.products.WithID(productID);
            return p.hasReceipt == true && string.IsNullOrEmpty(p.transactionID) == false;
        }
    }
}
