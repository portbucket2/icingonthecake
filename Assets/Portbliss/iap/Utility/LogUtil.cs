﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.IAP.Internal
{
    public static class LogUtil
    {
        public static void Green(string v, bool logEnabled)
        {
            if (logEnabled)
            {
                Debug.Log("<color='green'>" + v + "</color>");
            }
        }

        public static void Red(string v, bool logEnabled)
        {
            if (logEnabled)
            {
                Debug.Log("<color='red'>" + v + "</color>");
            }
        }

        public static void Yellow(string v, bool logEnabled)
        {
            if (logEnabled)
            {
                Debug.Log("<color='yellow'>" + v + "</color>");
            }
        }

        public static void Magenta(string v, bool logEnabled)
        {
            if (logEnabled)
            {
                Debug.Log("<color='magenta'>" + v + "</color>");
            }
        }
    }
}