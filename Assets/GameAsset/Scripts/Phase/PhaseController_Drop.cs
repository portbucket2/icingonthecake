﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PhaseController_Drop : PhaseController
{
    public DropController dc;

    public override GamePhase phase { get { return GamePhase.DropPatty; } }

    public override string ProgressText
    {
        get
        {
            return "Tap to Drop!";
        }
    }
    public Transform phaseCamTrans;
    public override void SetCam(float val, float rate)
    {

        RotationSpeedManager.SetNewTargets(phaseCamTrans, rate);
    }


    public override void OnPhaseStart()
    {
        base.OnPhaseStart();
        dc.OnPhaseStart();
    }

    public override void OnPhaseEnd()
    {
        base.OnPhaseEnd();
        dc.OnPhaseEnd();
    }
    public override float CalculateProgress()
    {
        return dc.progress;
    }
}
