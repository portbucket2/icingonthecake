﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MiniCamController : MonoBehaviour
{

    public static MiniCamController instance;
    // Start is called before the first frame update
    public GameObject displayObj;
    void Start()
    {
        instance = this;
        displayObj.SetActive(false);
        //rend.enabled = false;
    }


    public static void SetState(bool enabled)
    {

        instance.displayObj.SetActive(enabled);
        //rend.enabled = enabled;
    }

}
