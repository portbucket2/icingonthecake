﻿using System;
using System.Collections;
using System.Collections.Generic;
using Portbliss.IAP;
using FRIA;
using UnityEngine;

public class LevelPrefabManager : MonoBehaviour
{

    internal Theme theme = Theme.BLUE;
    public static event System.Action<Theme> newPrefabLoading;
    public static LevelPrefabManager currentLevel;

    public bool suspendedExternally;

    public int areaI { get; private set; }
    public int levelI { get; private set; }

    public LevelDefinition levelDefinition;
    public int sequence_areaIndex { get { return levelDefinition.sequence_AI; } }
    public int sequence_levelIndex { get { return levelDefinition.sequence_LI; } }
    public static bool hasLevelProgressOccured = false;
    int stars = 0;
    //public event System.Action onPreDestroy;
    System.Action onEnd;


    public VideoBonusType bonusType
    {
        get
        {
            if (cumulitiveLevelNo == 5)
                return VideoBonusType.ICING;
            else if (cumulitiveLevelNo % 5 == 2 && cumulitiveLevelNo>2)
                return VideoBonusType.LEVEL;
            else if (cumulitiveLevelNo % 3 == 0)
                return VideoBonusType.TOOL;
            else
                return VideoBonusType.NONE;
            
        }

    }
    public InLevelCanvasManager canvMan
    {
        get
        {
            return MainGameManager.instance.levelCanvas;
        }
    }

    //public void AnimateNextLevelButton()
    //{
    //    if (canvMan.nextButtonAnim)
    //    {
    //        canvMan.nextButtonAnim.SetTrigger("go");
    //    }
    //}

    public int cumulitiveLevelNo;

    public LoadType loadType;
    public void LoadAs(LoadType loadType, int areaIndex, int levelIndex, LevelDefinition deifinition, GameObject prefab, System.Action onEnd)
    {
        currentLevel = this;
        this.loadType = loadType;
        areaI = areaIndex;
        levelI = levelIndex;
        this.levelDefinition = deifinition;
        this.onEnd = onEnd;
        cumulitiveLevelNo = LevelLoader.GetLevelCumulativeNumber(areaI, levelI);

        
        GameObject innerResource = Instantiate(prefab, Vector3.zero, Quaternion.identity, this.transform);
        IThemeScript themescript = innerResource.GetComponentInChildren<IThemeScript>();

        if (ColorKeeper.SHOULD_RANDOMIZE_THEME)
        {
            theme = (Theme)  UnityEngine.Random.Range(0,System.Enum.GetValues(typeof(Theme)).Length);
            if (themescript != null)
            {
                themescript.SetTheme(theme);
            }
        }
        else
        { 
            //Debug.Log(themescript);
            if (themescript != null)
            {
                theme = themescript.GetTheme();
                //Debug.Log(themescript);
            }
        }


        //Debug.Log(theme);
        canvMan.LevelStart(this);
        newPrefabLoading?.Invoke(theme);

        //        if (cumulitiveLevelNo == 3)
        //        {
        //            Debug.Log("Review will be requested");
        //#if UNITY_IOS
        //        //MASHA VAI
        //        UnityEngine.iOS.Device.RequestStoreReview();
        //#endif
        //        }

    }

    public void UnLoad()
    {
        //onPreDestroy?.Invoke();
        currentLevel = null;
        Destroy(gameObject);
        Resources.UnloadUnusedAssets();
        onEnd?.Invoke();
    }

    public void ReportEarlyCompletion (int reportedStars)
    {
        int starC = LevelLoader.instance.GetStarFor(levelDefinition);
        if (starC < reportedStars)
        {
            LevelLoader.instance.SetStarFor(levelDefinition, reportedStars);
        }
        LevelRandomizer();
    }
    
    public void OnComplete(bool success, string infoText = null, System.Action onMinimize=null)
    {
        if (success)
        {
            int starC = LevelLoader.instance.GetStarFor(levelDefinition);
            if (starC >= stars)
            {
                stars = starC;
            }
            else
            {
                LevelLoader.instance.SetStarFor(levelDefinition, stars);
            }

            LevelRandomizer();
            canvMan.LoadSucces(infoText, onMinimize);
        }
        else
        {
            canvMan.LoadFail();
        }
        canvMan.SetStars(stars);
        if (loadType != LoadType.GALLERY) AnalyticsAssistant.LevelCompletedAppsFlyerIfItIsProperLevel(cumulitiveLevelNo);
    }


    void LevelRandomizer()
    {
        if (!checkcontainment(sequence_areaIndex, sequence_levelIndex))
        {
            LevelLoader.instance.AddtoRandomException(sequence_areaIndex, sequence_levelIndex);
            LevelLoader.instance.changerandomindex();
        }
        if (levelI >= LevelLoader.instance.areaDefinitions[areaI].lastUnlockedLevelIndex.value)
        {
            LevelLoader.instance.areaDefinitions[areaI].lastUnlockedLevelIndex.value = levelI + 1;

            int arI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions.Count);
            int lvlI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions[arI].levelDefinitions.Count);

            int j = 0;
            while (checkcontainment(arI, lvlI) && j < 100)
            {
                arI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions.Count);
                lvlI = UnityEngine.Random.Range(0, LevelLoader.instance.areaDefinitions[arI].levelDefinitions.Count);
                j++;
            }

            LevelLoader.instance.randomAreaIndex.value = arI;
            LevelLoader.instance.randomLevelIndex.value = lvlI;
        }


    }
    bool checkcontainment(int aI, int lI)
    {
        for (int i = 0; i < LevelLoader.instance.numberofexceptions; i++)
        {
            if (LevelLoader.instance.randomlevelcollection[i * 2].value == aI && LevelLoader.instance.randomlevelcollection[i * 2 + 1].value == lI)
            {
                return true;
            }
        }

        return false;
    }
    public void AddStar()
    {
        stars++;
        canvMan.SetStars(stars);

        int starC = LevelLoader.instance.GetStarFor(levelDefinition);
        if (starC < stars)
        {
            LevelLoader.instance.SetStarFor(levelDefinition, stars);
        }
    }

    public void OnNext()
    {
        if (suspendedExternally) return;
        UnLoad();

        //Portbliss.Social.GifShareControl.EndRecord();
        
        if (loadType == LoadType.GALLERY)
        {
            //Debug.Log("OnNext of gallery type");
            LevelLoader.instance.LoadResourceAndLevel_Normal(LevelLoader.Last_ai, LevelLoader.Last_li);
            LevelPrefabManager.hasLevelProgressOccured = false;
        }
        else
        {
            MainGameManager.willShowAd_IAP = !(GameConfig.hasIAP_NoAdPurchasedHD.value);

            if (MainGameManager.willShowAd_IAP)
            {
                try
                {
                    TimedAd.AdIteration((success) =>
                    {
#if (UNITY_IOS)
                    if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
                    {
                        UnityEngine.iOS.Device.RequestStoreReview();
                        MainGameManager.ratingShown.value = true;
                    }
#endif
                    });
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning(e.Message);
#if (UNITY_IOS)
                    if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
                    {
                        UnityEngine.iOS.Device.RequestStoreReview();
                        MainGameManager.ratingShown.value = true;
                    }
#endif
                }
            }
            else
            {
#if (UNITY_IOS)
                    if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
                    {
                        UnityEngine.iOS.Device.RequestStoreReview();
                        MainGameManager.ratingShown.value = true;
                    }
#endif
            }


            LevelPrefabManager.hasLevelProgressOccured = true;
#if (UNITY_ANDROID)
            if (cumulitiveLevelNo >= 3 && !MainGameManager.ratingShown.value)
            {
                MainGameManager.instance.levelCanvas.LoadRatingWindow();
                MainGameManager.ratingShown.value = true;
            }
#endif

            int ai = LevelLoader.GetNextLevel_ai(areaI, levelI);
            int li = LevelLoader.GetNextLevel_li(areaI, levelI);
            LevelLoader.instance.LoadResourceAndLevel_Normal(ai, li);
            GameConfig.hasRewardedVideoAdBeenShown = false;
        }
    }

    public void OnReset()
    {
        if (suspendedExternally) return;
        hasLevelProgressOccured = false;
        int ai = currentLevel.areaI;
        int li = currentLevel.levelI;
        UnLoad();
        switch (loadType)
        {
            case LoadType.NORMAL:
                {
                    AnalyticsAssistant.LevelRestarted(cumulitiveLevelNo, levelDefinition.GetGamePlayType(), levelDefinition.GetLayerCount());
                    LevelLoader.instance.LoadResourceAndLevel_Normal(ai, li);
                }
                break;
            case LoadType.GALLERY:
                {

                    GalleryItemData data = GalleryManager.recentGalleryItemData;
                    MainGameManager.instance.StartLevel(LoadType.GALLERY, data.ai, data.li, data.levelDefinition, Resources.Load<GameObject>(data.levelDefinition.resourcePath));

                }
                break;
            default:
                break;
        }


    }


    public void OnLevels()
    {
        if (suspendedExternally) return;
        UnLoad();
        
    }
}
public enum Theme
{
    RED = 0,
    GREEN = 1,
    BLUE = 2,
    YELLOW = 3,
}
public interface IThemeScript
{
    Theme GetTheme();
    void SetTheme(Theme theme);
}

public enum LoadType
{
    NORMAL = 1,
    GALLERY = 2,
}

public enum VideoBonusType
{
    NONE = 0,
    LEVEL = 1,
    TOOL = 2,
    ICING = 3,
}