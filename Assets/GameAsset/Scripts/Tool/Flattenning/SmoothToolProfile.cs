﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class SmoothToolProfile 
{
    public int id;
    public string title;
    public Sprite proPic;
    public int active_index;

    private HardData<bool> isUnlockedHD;

    public SmoothToolProfile() { }
    public SmoothToolProfile(int id) { this.id = id; active_index = id; }
    public bool IsUnlocked 
    { 
        get 
        {
            if (isUnlockedHD == null)
            {
                isUnlockedHD = new HardData<bool>(string.Format("TOOL_SMOOTH_{0}_UNLOCKED",id),id == 0);
            }
            return isUnlockedHD.value;
        } 
    }
    public void Unlock()
    {
        if (isUnlockedHD == null)
        {
            isUnlockedHD = new HardData<bool>(string.Format("TOOL_SMOOTH_{0}_UNLOCKED", id), id == 0);
        }
        isUnlockedHD.value = true;
    }

    public void Init()
    {
        if (isUnlockedHD == null)
        {
            isUnlockedHD = new HardData<bool>(string.Format("TOOL_SMOOTH_{0}_UNLOCKED", id), id == 0);
        }
    }

}


#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(SmoothToolProfile))]
public class SmoothToolProfileEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUIUtility.labelWidth = 0;
        label.text = string.Format("");

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float elementHeight = (position.height - 10) / 3;
        float imageOffset = position.height;

        float labelWidth = 100;

        var label_idRect = new Rect(position.x, position.y, labelWidth, elementHeight);
        var label_priceRect = new Rect(position.x, position.y + elementHeight + 5, labelWidth, elementHeight);
        var label_integerRect = new Rect(position.x, position.y + elementHeight * 2 + 10, labelWidth, elementHeight);

        var idRect = new Rect(position.x + labelWidth, position.y, position.width - imageOffset - labelWidth, elementHeight);
        var priceRect = new Rect(position.x + labelWidth, position.y + elementHeight + 5, position.width - imageOffset - labelWidth, elementHeight);
        var integerRect = new Rect(position.x + labelWidth, position.y + elementHeight * 2 + 10, position.width - imageOffset - labelWidth, elementHeight);
        var imageRect = new Rect(position.x + position.width - imageOffset, position.y, imageOffset, position.height);

        EditorGUI.LabelField(label_idRect, "ID");
        EditorGUI.LabelField(label_priceRect, "Title");
        EditorGUI.LabelField(label_integerRect, "active_index");

        //EditorGUI.LabelField(labelRect, "isRight?");
        EditorGUI.PropertyField(idRect, property.FindPropertyRelative("id"), GUIContent.none);
        EditorGUI.PropertyField(priceRect, property.FindPropertyRelative("title"), GUIContent.none);
        EditorGUI.PropertyField(integerRect, property.FindPropertyRelative("active_index"), GUIContent.none);
        //EditorGUI.PropertyField(imageRect, property.FindPropertyRelative("itemSprite"), GUIContent.none);
        property.FindPropertyRelative("proPic").objectReferenceValue = EditorGUI.ObjectField(imageRect, property.FindPropertyRelative("proPic").objectReferenceValue, typeof(Sprite), false);


        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * 3 + 10;
    }
}

#endif