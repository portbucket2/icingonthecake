﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using FRIA;

public class AreaLoader : MonoBehaviour
{
    public Text areaTitle;
    public GameObject levelUIPrefab;
    public RectTransform contentPanel;

    public event System.Action onRefresh;

    public bool centreObjectIfNecessaryFor3ItemColumn = true;

    int areaIndex;
    private List<LevelUIItemLoader> levelitems = new List<LevelUIItemLoader>();
    public void Load(int areaIndex)
    {
        this.areaIndex = areaIndex;
        areaTitle.text = string.Format("Area {0}",areaIndex+1);
        AreaDefinition area = LevelLoader.instance.areaDefinitions[areaIndex];


        for (int i = levelitems.Count-1; i >=0 ; i--)
        {
            Pool.Destroy(levelitems[i].gameObject);
            levelitems.RemoveAt(i);
        }
        int N = area.levelDefinitions.Count;
        int m = N % 3;
        bool addOne = centreObjectIfNecessaryFor3ItemColumn && m == 1;
        //Debug.Log(addOne);

        for (int i = 0; i < area.levelDefinitions.Count+ (addOne?1:0); i++)
        {
            Transform tr = Pool.Instantiate(levelUIPrefab).transform;
            tr.SetParent(contentPanel);
            tr.localScale = Vector3.one;
            tr.rotation = Quaternion.identity;

            levelitems.Add(tr.GetComponent<LevelUIItemLoader>());
        }
        Refresh(addOne);
    }

    public void Refresh(bool hasExtra)
    {
        onRefresh?.Invoke();





        if (hasExtra)

        {
            //Debug.Log("B");
            for (int i = 0; i < levelitems.Count; i++)
            {
                if (i == levelitems.Count - 2)
                {
                    levelitems[i].LoadAsDummy();
                }
                else if (i == levelitems.Count - 1)
                {
                    levelitems[i].Load(areaIndex, i-1);
                }
                else
                {
                    levelitems[i].Load(areaIndex, i);
                }
            }
        }
        else
        {
            //Debug.Log("A");
            for (int i = 0; i < levelitems.Count; i++)
            {
                levelitems[i].Load(areaIndex, i);
            }
        }
    }
}