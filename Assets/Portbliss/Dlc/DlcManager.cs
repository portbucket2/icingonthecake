﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace Portbliss.DLC
{
    public enum DLC_UPDATE_CHECK_MODE { N_HoursCheck, ContinuousCheck, NoCheck }
    public class DlcManager : MonoBehaviour
    {
        static DlcManager instance;
        
        //const bool logEnabled = true;
        public static string BaseSrvUrlWithEndSlash
        {
            get
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                return instance.baseSrvUrlWithEndSlash+"Android/";
#elif UNITY_IOS && !UNITY_EDITOR
                return instance.baseSrvUrlWithEndSlash+"iOS/";
#elif UNITY_EDITOR_OSX
                return instance.baseSrvUrlWithEndSlash+"mac/";
#elif UNITY_EDITOR_WIN
                return instance.baseSrvUrlWithEndSlash + "win/";
#else
                return instance.baseSrvUrlWithEndSlash + "unknown-platform/";
#endif
            }
        }
        [SerializeField] bool enableSystem = true;
        public static bool EnableSystem { get { return instance.enableSystem; } }
        [SerializeField] string baseSrvUrlWithEndSlash;
        public static string DiskDlcFolderName { get { return instance.diskDlcFolderName; } }
        [SerializeField] string diskDlcFolderName;
        [SerializeField] bool isManifestImportant = false, isCrcCheckImportant = false, isVersionImportant = true, willUseSharedBundle = false;
        public static bool WillUseSharedBundle { get { return instance.willUseSharedBundle; } }
        public static bool IsCrcCheckImportant { get { return instance.isCrcCheckImportant; } }
        [SerializeField] DLC_UPDATE_CHECK_MODE updateMode = DLC_UPDATE_CHECK_MODE.N_HoursCheck;
        AllDlcDescription dlcDescription;
        public static AllDlcDescription AllDlcDescription { get { return instance.dlcDescription; } }
        [SerializeField] int dlcLevelStart = 101, totalNumberOfDlcLevels = 100;
        [SerializeField] float hoursForUpdateCheck = 480;
        [SerializeField] int versionDownloadAttemptMax = 5, manifestDownloadAttemptMax = 5, crcDownloadAttemptMax = 5, dlcDownloadAttemptMax = 5;
        
        public static int VersionDownloadAttemptMax { get { return instance.versionDownloadAttemptMax; } }
        public static int ManifestDownloadAttemptMax { get { return instance.manifestDownloadAttemptMax; } }
        public static int CrcDownloadAttemptMax { get { return instance.crcDownloadAttemptMax; } }
        public static int DlcDownloadAttemptMax { get { return instance.dlcDownloadAttemptMax; } }
        VersionControl versionControl;
        ManifestControl manifestControl;
        CrcControl crcControl;
        DlcDownloadStoreLoadControl downloadStoreLoadCoreControl;
        AssetBundleManifest dlcManifest;
        DescriptionOfOriginalCrcData dlcCrcData;
        public static DescriptionOfOriginalCrcData CrcData { get { return instance.dlcCrcData; } }
        DlcUIView view;
        [Space(10)]
        [SerializeField] bool crcLogEnabled, versionLogEnabled, manifestLogEnabled, dlcLogEnabled;
        public static bool CrcLogEnabled { get { return instance.crcLogEnabled; } }
        public static bool VersionLogEnabled { get { return instance.versionLogEnabled; } }
        public static bool ManifestLogEnabled { get { return instance.manifestLogEnabled; } }
        public static bool DlcLogEnabled { get { return instance.dlcLogEnabled; } }

        void Awake()
        {
            if (dlcLogEnabled) { Debug.Log("persistent data path: " + Application.persistentDataPath); }
            
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);

                if (enableSystem == false) { return; }
                view = GetComponent<DlcUIView>();
                view.InitView();
                dlcDescription = null;
                

                versionControl = gameObject.GetComponent<VersionControl>();
                if (versionControl == null)
                {
                    versionControl = gameObject.AddComponent<VersionControl>();
                }

                manifestControl = gameObject.GetComponent<ManifestControl>();
                if (manifestControl == null)
                {
                    manifestControl = gameObject.AddComponent<ManifestControl>();
                }

                crcControl = gameObject.GetComponent<CrcControl>();
                if (crcControl == null)
                {
                    crcControl = gameObject.AddComponent<CrcControl>();
                }

                downloadStoreLoadCoreControl = gameObject.GetComponent<DlcDownloadStoreLoadControl>();
                if (downloadStoreLoadCoreControl == null)
                {
                    downloadStoreLoadCoreControl = gameObject.AddComponent<DlcDownloadStoreLoadControl>();
                }
                if (dlcDescription == null)
                {
                    dlcDescription = new AllDlcDescription(dlcLevelStart, totalNumberOfDlcLevels);
                }
                StopAllCoroutines();
                StartCoroutine(StartDLC_EntryPoint());
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }

        public static void ForceRestartDlcDownload()
        {
            instance.StopAllCoroutines();
            instance.versionControl.StopAllCoroutines();
            instance.crcControl.StopAllCoroutines();
            instance.manifestControl.StopAllCoroutines();
            instance.downloadStoreLoadCoreControl.StopAllCoroutines();
            instance.view.StopAllCoroutines();
            if (AllDlcDescription != null)
            {
                AllDlcDescription.isDlcBeingDownloaded = false;
            }
            instance.StartCoroutine(instance.StartDLC_EntryPoint());
        }

        /// <summary>
        /// This will load a level. If the level is builtin app, then this will load that.
        /// Or if it is in dlc then it will load that too. If all these fails due to 'not downloaded' yet,
        /// then we will show a UI that loading is being done on background.
        /// </summary>
        /// <param name="areaI_prefab"></param>
        /// <param name="levelI_Prefab"></param>
        /// <returns></returns>
        public static void LoadA_Level(LevelDefinition levelDef, Action<GameObject> OnLoad)
        {
            throw new NotImplementedException();
            int areaI_prefab = 0;// levelDef.prefab_AI;
            int levelI_Prefab = 0;// levelDef.prefab_LI; 
            string variant = "";// levelDef.variant;


            if (string.IsNullOrEmpty(variant))
            {
                GameObject lvl = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}", areaI_prefab + 1, levelI_Prefab + 1));

                //Debug.LogErrorFormat("action is null: {0}, found: {1}", OnLoad == null, lvl);
                OnLoad?.Invoke(lvl);
                return;
            }
            else
            {
                GameObject lvl = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}_{2}", areaI_prefab + 1, levelI_Prefab + 1, variant));


                //Debug.LogErrorFormat("var:{2}, action is null: {0}, found: {1}", OnLoad == null, lvl,variant);
                OnLoad?.Invoke(lvl);
                return;
            }
            //===============================================================

            //int levelNum = LevelLoader.GetCumulativeLevelIndex(areaI_prefab, levelI_Prefab) + 1;
            //Debug.Log("<color='red'>baler number: "+levelNum+"</color>");
            ////GameObject level = null;
            //if (instance.enableSystem)
            //{
            //    if (levelNum <= instance.dlcLevelStart)
            //    {
            //        var level = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}", areaI_prefab + 1, levelI_Prefab + 1));
            //        OnLoad?.Invoke(level);
            //        return;
            //    }
            //    else
            //    {
            //        int patchedNum = levelNum - 1;
            //        if (instance.dlcLogEnabled) { Debug.Log("<color='yellow'>load operation on asset bundle</color>"); }

            //        AssetBundle lvMainBundle = null;
            //        DLC_Descriptor thisDesc = DlcManager.AllDlcDescription.GetDescriptorForLevel(patchedNum);
            //        GameObject level = null;
            //        if (thisDesc != null && thisDesc.DoesItExist())
            //        {
            //            if (instance.dlcLogEnabled) { Debug.Log("<color='green'>main level bundle has been obtained from local file location.</color>"); }
            //            AssetBundle.UnloadAllAssetBundles(false);
            //            Resources.UnloadUnusedAssets();

            //            if (instance.willUseSharedBundle)
            //            {
            //                string shPath = Path.Combine(Application.persistentDataPath, DiskDlcFolderName + "/shared.gpkg");
            //                AssetBundle sharedBundle = AssetBundle.LoadFromFile(shPath);
            //            }


            //            lvMainBundle = AssetBundle.LoadFromFile(thisDesc.DiskUrl);
            //            string fName = "Level " + patchedNum;
            //            level = lvMainBundle.LoadAsset<GameObject>(fName);
            //        }

            //        if (level == null)
            //        {
            //            Debug.Log("<color='red'>let us show the download UI</color>");
            //            DlcUIView.ShowProgressUIOnLoadFail(patchedNum, OnLoad);
            //        }
            //        else
            //        {
            //            OnLoad?.Invoke(level);
            //        }
            //    }
            //}
            //else
            //{
            //    var level = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}", areaI_prefab + 1, levelI_Prefab + 1));
            //    OnLoad?.Invoke(level);
            //    return;
            //}
        }


        /// <summary>
        /// this must be started from level loading system
        /// </summary>
        public static void OnLoadALevel()
        {
            if (instance.enableSystem)
            {
                if (AllDlcDescription != null && AllDlcDescription.isDlcBeingDownloaded == false)
                {
                    instance.StopAllCoroutines();
                    instance.StartCoroutine(instance.StartDLC_EntryPoint());
                }
            }
        }

        IEnumerator StartDLC_EntryPoint()
        {
            dlcDescription.isDlcBeingDownloaded = true;
            string dataPath = Path.Combine(Application.persistentDataPath, diskDlcFolderName);
            DirectoryInfo dlcDirInfo = new DirectoryInfo(dataPath);
            if (dlcDirInfo.Exists == false)
            {
                if (dlcLogEnabled) { Debug.Log("<color='green'>data folder has been created.</color>"); }
                dlcDirInfo.Create();
            }

            yield return new WaitForSeconds(3.7f);
            bool mandatoryDlCondition = AllDlcDescription.HasAllDlcOpBeenDone == false ||
                AllDlcDescription.IsThereAnyCrcOnDiskAndIsItValid() == false ||
                AllDlcDescription.IsThereAnyManifestOnDiskAndItisValid() == false ||
                AllDlcDescription.IsThereAnyLevelFiles() == false ||
                AllDlcDescription.IsVersionDlTimeError.value == true;
            bool nDayCondition = (updateMode == DLC_UPDATE_CHECK_MODE.N_HoursCheck) && dlcDescription.HasTimeBeenPassed(0, (int)hoursForUpdateCheck);
            bool contdCondition = (updateMode == DLC_UPDATE_CHECK_MODE.ContinuousCheck);
            if (dlcLogEnabled) { Logger.LogDownloadCondition(mandatoryDlCondition, nDayCondition, contdCondition); }
            if (isVersionImportant == false && DlcManager.AllDlcDescription.HasAllDlcOpBeenDone == false)
            {
                if (dlcLogEnabled) { Debug.Log("<color='green'>Version control is not important, lets download the dlc. Lets start with manifest.</color>"); }
                //start manifest download operation
                StartManifest();
            }
            else if (mandatoryDlCondition || nDayCondition || contdCondition)
            {
                StartWithVersionCheck();
            }
            else
            {
                if (dlcLogEnabled) { Debug.Log("<color='yellow'>we dont need to download anything.</color>"); }
                //terminate operation
                dlcDescription.isDlcBeingDownloaded = false;
            }
        }

        void StartWithVersionCheck(string debugMsg = "")
        {
            if (dlcLogEnabled) { Debug.Log("<color='green'>let us ask version control system if we need update or not---additional debug message: "+debugMsg+"</color>"); }
            versionControl.CheckVersion((shouldDowloadOrUpdate) =>
            {
                if (shouldDowloadOrUpdate)
                {
                    if (dlcLogEnabled) { Debug.Log("<color='green'>version control system says that it is success and we need to work on manifest</color>"); }
                    //start manifest download operation
                    StartManifest();
                }
                else
                {
                    //terminate operation
                    dlcDescription.isDlcBeingDownloaded = false;
                    if (dlcLogEnabled)
                    {
                        Debug.Log("<color='yellow'>version control system says either we do not need to update or" +
          " there is an error checking for update. Thus we simply abort the dlc download process.</color>"); Logger.LogDlcFiles();
                    }
                }
            });
        }

        void StartManifest()
        {
            manifestControl.DownloadAndStoreIfReq_AndGetManifest((manifestData) =>
            {
                dlcManifest = manifestData;
                if (isManifestImportant && manifestData == null)
                {
                    if (dlcLogEnabled) { Debug.Log("<color='red'>we failed in from manifest system and we must abort " +
                        ", since we could not find valid manifest data And manifest is really important!</color>"); }
                    //terminate operation
                    dlcDescription.isDlcBeingDownloaded = false;
                }
                else
                {
                    if (dlcLogEnabled) { Debug.Log("<color='green'>we returned from manifest system and it was roughy a success</color>"); }
                    //start crc thing
                    crcControl.DownloadAndStoreIfReq_AndGetCRC_Data((crcData) =>
                    {
                        dlcCrcData = crcData;
                        if (isCrcCheckImportant && crcData == null)
                        {
                            if (dlcLogEnabled) { Debug.Log("<color='red'>we failed in from crc system and we must abort, " +
                                "since we could not find valid crc data and crc check is important!</color>"); }
                            //terminate operation
                            dlcDescription.isDlcBeingDownloaded = false;
                        }
                        else
                        {
                            if (dlcLogEnabled) { Debug.Log("<color='green'>we returned from crc system and it was a success</color>"); }
                            //start operating on each dlc files
                            downloadStoreLoadCoreControl.DownloadAndStoreIfReq((allDlcOpSuccess) =>
                            {
                                if (allDlcOpSuccess)
                                {
                                    if (dlcLogEnabled) { Debug.Log("<color='green'>we returned from core dlc download system and it was a success</color>"); }
                                    dlcDescription.SetAsComplete();
                                }
                                else
                                {
                                    if (dlcLogEnabled)
                                    {
                                        Debug.Log("<color='yellow'>we failed in from core dlc download system and we abort." +
                          " Or we deliberately halt the download process for now.</color>");
                                    }
                                }
                                dlcDescription.isDlcBeingDownloaded = false;
                            });
                        }
                    });
                }
            });
        }
    }
}