﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SettingsScriptableCreator
{
    [UnityEditor.MenuItem("Assets/Create/Settings/Icing")]
    public static void Create1()
    {
        IcingSettings so = ScriptableObject.CreateInstance<IcingSettings>();
        UnityEditor.AssetDatabase.CreateAsset(so, "Assets/GameAsset/Settings/Icing X.asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }

    [UnityEditor.MenuItem("Assets/Create/Settings/Operation")]
    public static void Create2()
    {
        OperationSettings so = ScriptableObject.CreateInstance<OperationSettings>();
        UnityEditor.AssetDatabase.CreateAsset(so, "Assets/GameAsset/Settings/Operations X.asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }
}
