﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractionPointerController : MonoBehaviour
{
    public RectTransform pointerTrans;
    public Image progressFill;
    public Animator progressAnim;
    public GameObject progressOnlyObject;
    public GameObject allProgressRootObject;

    public ToolBehaviourController  toolController;
    public float maxPointerSpeedLimit =750;
    RectTransform rectTrans;
    Vector2 canvasCentreDifference;
    public CanvasScaler canvScaler;
    float reverseScaleFactor;

    Vector2 edgeOffset
    {
        get
        {
            if (MainGameManager.settings.editorPaintMode)
            {
                return Vector2.zero;
            }
            else
            {
                return new Vector2(0, canvScaler.referenceResolution.y / 8.0f);
            }
        }

    }


    public RectTransform intialMousePosRef;
    void Start()
    {
        rectTrans = this.transform.GetComponent<RectTransform>();
        reverseScaleFactor = canvScaler.referenceResolution.y/ Screen.height;
        canvasCentreDifference =  new Vector2(Screen.width,Screen.height)*reverseScaleFactor / 2;
        pointerTrans.anchoredPosition = new Vector2(0, canvScaler.referenceResolution.y / 8.0f);
        toolController.savedMousePos = (intialMousePosRef.anchoredPosition+ canvasCentreDifference) / reverseScaleFactor;
        //warmUpProgress = 0;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Break();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            LevelPrefabManager.currentLevel.OnReset();
        }
        if (Input.GetMouseButtonDown(0) && TriggerAreaDetector.pressedOn)
        {
            if (DropController.runningDropPhase)
            {
                DropController.runningDropPhase.OnTap();
            }

            MoveTheToolPointer();

        }
        else if (Input.GetMouseButton(0) && TriggerAreaDetector.pressedOn)
        {
            MoveTheToolPointer(maxPointerSpeedLimit);
        }

        else
        {
            pointerTrans.gameObject.SetActive(false);
            toolController.NoKnifeUpdate();

        }



    }

    Vector2 targetPos;
    Vector2 targetMousePos;




    void MoveTheToolPointer(float maxSpeed = float.MaxValue)
    {
        targetPos = (Vector2)Input.mousePosition * reverseScaleFactor - canvasCentreDifference;

        pointerTrans.gameObject.SetActive(true);
        float maxDist = maxSpeed * Time.deltaTime;

        Vector2 walkVec = targetPos - rectTrans.anchoredPosition;

        if (walkVec.magnitude > maxDist)
        {
            rectTrans.anchoredPosition = rectTrans.anchoredPosition + walkVec.normalized * maxDist;
        }
        else
        {
            rectTrans.anchoredPosition = rectTrans.anchoredPosition + walkVec;
        }

        targetMousePos = (rectTrans.anchoredPosition + edgeOffset + canvasCentreDifference)/reverseScaleFactor;

        toolController.KnifeMoveUpdate(targetMousePos, OnToolPrepareProgress);
    }
    public void OnToolPrepareProgress(float progress)
    {
        allProgressRootObject.SetActive(PhaseManager.phase == GamePhase.AddCream);
        if (progressFill) progressFill.fillAmount = progress;
        if (progressAnim)
        {
            progressAnim.SetLayerWeight(1, 1 - progress);
            progressAnim.SetLayerWeight(2, progress);
        }
        if (progressOnlyObject)
        {
            progressOnlyObject.SetActive(progress>0&& progress<1);
        }
    }
}
