﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Ad
{
    public class ButtonOpensLink : MonoBehaviour
    {

        public string URLtoOpen;
        // Start is called before the first frame update
        void Start()
        {
            GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => Application.OpenURL(URLtoOpen));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}