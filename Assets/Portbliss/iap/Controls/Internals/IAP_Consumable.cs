﻿using Portbliss.IAP.Internal;

namespace Portbliss.IAP
{
    public class IAP_Consumable : IIAP_Buy
    {
        bool logEnabled;
        IAP_Controller IAPController;
        internal IAP_Consumable(IAP_Controller iAPController)
        {
            IAPController = iAPController;
            logEnabled = iAPController.LogEnabled;
        }

        public void BuyProduct(string productID)
        {
            IAPController.WaitForRestoreIfAny(() =>
            {
                if (IAPController.IsConsumableValid(productID))
                {
                    LogUtil.Green("lets try to buy consumable product: " + productID, logEnabled);
                    IAP_Core.BuyProductID(productID);
                }
                else
                {
                    LogUtil.Red("you are trying to buy consumable product ID: " + productID +
                          "which are not defined in the controller script. Won't be successful!", logEnabled);
                }
            });
        }
    }
}