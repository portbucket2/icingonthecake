﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrippingPoint : MonoBehaviour
{
    private const float STOP_HEIGHT = 0.001f;
    public float speed;
    internal float maxRange;
    internal Vector3 position;

    internal MorfMesh cmesh;
    private List<int> registeredVertexList = new List<int>();
    private int[] registeredVertexArray;
    private float[] registederVertexSpeedRatio;
    //private Vector3[] vertices;


    private float target_H;
    private float target_T;
    private void Awake()
    {

        visualMesh.enabled = false;
        maxRange = visualMesh.transform.localScale.x/2;
        //Debug.Log(range);
    }


    public void Initialize(MorfMesh cm,Vector3 dropPos)
    {
        cmesh = cm;
        //this.vertices = cm.vertices;
        position = cm.transform.InverseTransformPoint(this.transform.position);
        registederVertexSpeedRatio = new float[cm.vertices.Length];
        for (int i = 0; i < registederVertexSpeedRatio.Length; i++)
        {
            registederVertexSpeedRatio[i] = 1;
        }
        target_H =  cm.transform.InverseTransformPoint(dropPos).y - position.y;
        target_T = (2 * target_H) / speed;
    }
    public void AddVertex(int i)
    {
        registeredVertexList.Add(i);
    }
    public void FinishInitialization()
    {
        registeredVertexArray = registeredVertexList.ToArray();
        registeredVertexList = null;
        Debug.Log(this.transform.name + registeredVertexArray.Length);
    }

    public MeshRenderer visualMesh;


    public void OnControllerUpdate()
    {
        int vi = 0;
        float distance;
        float distanceY;
        float distanceXZ;
        // Vector3 v3 = Vector3.zero;
        float x = 0, y = 0, z = 0;
        float dt = Time.deltaTime;
        float speed;
        float dampingLerp;
        float closeness;
        float closeness_Damp;
        for (int i = 0; i < registeredVertexArray.Length; i++)
        {
            vi = registeredVertexArray[i];
            if (cmesh.vxMovementCompletion[0][vi]<1f) continue;
            x = cmesh.vertices[vi].x - position.x;
            y = cmesh.vertices[vi].y - position.y;
            z = cmesh.vertices[vi].z - position.z;
            distanceXZ = Mathf.Sqrt(x*x + z*z);
            distance = Mathf.Sqrt(x * x + y*y+ z * z);
            //v3.x = x / distance;
            //v3.y = y / distance;
            //v3.z = z / distance;
            distanceY = y > 0 ? y : -y;
            

            if (distance>STOP_HEIGHT && distanceXZ < maxRange && Vector3.Angle(cmesh.normals[vi],Vector3.down)<=90)
            {
                closeness = Mathf.Pow(Mathf.Clamp01(1 - (distanceXZ / maxRange)), 0.15f);
                speed = Mathf.Min(closeness, registederVertexSpeedRatio[i]) *this.speed * dt;
                registederVertexSpeedRatio[i] = Mathf.Clamp01(registederVertexSpeedRatio[i] -(dt/target_T));//

                //cmesh.vertices[vi].x = cmesh.vertices[vi].x - speed * (x / distance)* radialDist;
                cmesh.vertices[vi].y = cmesh.vertices[vi].y - speed ;// * closeness;// (y/distance)* radialDist;
                //cmesh.vertices[vi].z = cmesh.vertices[vi].z - speed * (z / distance)* radialDist;
                //dampingLerp = Mathf.Lerp(1, 0, dt * 90);

                //closeness_Damp = Mathf.Lerp(dampingLerp ,1, closeness);
                //cmesh.normals[vi].y *= dampingLerp;
                cmesh.normals[vi].y = Mathf.Lerp(cmesh.normals[vi].y, 0, (1- registederVertexSpeedRatio[i])*(closeness));
            }


        }
        cmesh.meshFilter.mesh.vertices = cmesh.vertices;
        cmesh.meshFilter.mesh.normals = cmesh.normals;
    }
}


