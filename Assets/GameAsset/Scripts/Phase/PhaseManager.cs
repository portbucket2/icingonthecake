﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GamePhase
{
    //GameOver_Sucess = -1,
    AddCream = 00,
    Flatten = 10,

    DropPatty = 20,

    Decorate = 30,
}
public class PhaseManager : MonoBehaviour,IThemeScript
{
    #region globals
    public const string PAINT_LAYER_NAME = "PaintLayer";
    public const string PAINT_LAYER2_NAME = "PaintLayer2";
    //public const string CLEAN_LAYER_NAME = "CleanLayer";
    public static LayerMask paintLayerMask
    {
        get
        {
            return LayerMask.GetMask(PAINT_LAYER_NAME);
            //switch (phase)
            //{
            //    default:
            //    case GamePhase.AddCream:
            //        return LayerMask.GetMask(PAINT_LAYER_NAME);
            //    case GamePhase.Flatten:
            //        return LayerMask.GetMask(CLEAN_LAYER_NAME);
            //}
        }
    }
    public static LayerMask paintLayerMask2
    {
        get
        {
            return LayerMask.GetMask(PAINT_LAYER2_NAME);
            //switch (phase)
            //{
            //    default:
            //    case GamePhase.AddCream:
            //        return LayerMask.GetMask(PAINT_LAYER_NAME);
            //    case GamePhase.Flatten:
            //        return LayerMask.GetMask(CLEAN_LAYER_NAME);
            //}
        }
    }
    public static int paintLayer2
    {
        get
        {
            return LayerMask.NameToLayer(PAINT_LAYER2_NAME);
            //switch (phase)
            //{
            //    default:
            //    case GamePhase.AddCream:
            //        return LayerMask.GetMask(PAINT_LAYER_NAME);
            //    case GamePhase.Flatten:
            //        return LayerMask.GetMask(CLEAN_LAYER_NAME);
            //}
        }
    }
    public static int paintLayer
    {
        get
        {
            return LayerMask.NameToLayer(PAINT_LAYER_NAME);
            //switch (phase)
            //{
            //    default:
            //    case GamePhase.AddCream:
            //        return LayerMask.NameToLayer(PAINT_LAYER_NAME);
            //    case GamePhase.Flatten:
            //        return LayerMask.NameToLayer(CLEAN_LAYER_NAME);
            //}
        }
    }


    public static int currentPhaseIndex;
    public static PhaseController CurrentPhaseController
    {
        get
        {
            if (!PhaseManager.instance) return null;
            return instance.gamePhaseSequence[currentPhaseIndex];
        }
    }

    public static GamePhase phase
    {
        get
        {
                return CurrentPhaseController.phase;
        }
    }
    public static int gamePhaseCounts
    {
        get { return instance.gamePhaseSequence.Count; }
    }
    public static bool IsAtLastPhase
    {
        get
        {
            return currentPhaseIndex == gamePhaseCounts - 1;
        }
    }

    public static PhaseManager instance;
    public static float unitLength;


    #endregion
    public GameObject decoration;
    public Theme levelTheme = Theme.BLUE;
    public string customLevelTitle;
    public Material sampleMat;

    public Vector3 baseRotationSpeed = new Vector3(0, 135, 0);
    public float gridDivisionUnit = 0.15f;

    public List<PhaseController> gamePhaseSequence;

    public bool forceUseDefaultCamTrans;
    public Transform defaultCamTrans;



    float allGamePlayStartTime=-1;
    float allGameplayEndTime=-1;
    public int GetAllPhaseTotalTime()
    {
        if (allGameplayEndTime < 0)
        {
            throw new System.Exception("All game phases are not complete!");
        }
        return (int)(allGameplayEndTime - allGamePlayStartTime);
    }
    void Awake()
    {
        //Debug.LogFormat("Serialize check {0}",  typeof(Color32).IsSerializable);

        instance = this;
        int phaseSetID = 0;
        for (int i = gamePhaseSequence.Count - 1; i >= 0; i--)
        {
            if (!gamePhaseSequence[i].gameObject.activeInHierarchy) gamePhaseSequence.RemoveAt(i);
        }
        for (int i = 0; i < gamePhaseSequence.Count; i++)
        {
            if (gamePhaseSequence[i].phase == GamePhase.AddCream) phaseSetID++;
            gamePhaseSequence[i].phaseSetID = phaseSetID;
        }
        for (int i = gamePhaseSequence.Count-1; i>=0 ; i--)
        {
            if (!gamePhaseSequence[i].enabled) gamePhaseSequence.RemoveAt(i);
        }
        if (LevelPrefabManager.currentLevel.loadType == LoadType.GALLERY)
        {
            while (gamePhaseSequence[0].phase != GamePhase.Decorate)
            {
                gamePhaseSequence.RemoveAt(0);
            }
            //Portbliss.Social.GifShareControl.BeginRecord();
            (GetPhase(GamePhase.AddCream) as PhaseController_Icing).CreateBodyDoubleForGallery(GalleryManager.recentGalleryItemData.vcolData);
            GameProgressManager.instance.DoCompletionCelebration();
            FRIA.Centralizer.Add_DelayedMonoAct(this,()=> { (gamePhaseSequence[0] as PhaseController_Decorate).LoadDefaultDecoration(decorationSaveData: null); },1.5f);
        }

        allPhaseOver = false;
        allGameplayOver = false;
        currentPhaseIndex = -1;
        unitLength = gridDivisionUnit;
        MoveToNextPhase();

        allGamePlayStartTime = Time.time;
    }

    public PhaseController GetPhase(GamePhase phase)
    {
        foreach (var item in gamePhaseSequence)
        {
            if (item.phase == phase) return item;
        }

        switch (phase)
        {
            case GamePhase.AddCream:
                return instance.transform.GetComponentInChildren<PhaseController_Icing>();
            case GamePhase.Flatten:
                return instance.transform.GetComponentInChildren<PhaseController_Flattening>();
            case GamePhase.DropPatty:
                return instance.transform.GetComponentInChildren<PhaseController_Drop>();
            case GamePhase.Decorate:
                return instance.transform.GetComponentInChildren<PhaseController_Decorate>();
            default:
                return null;
        }

    }
    
    public List<Transform> meshRoots;
    public void EditorInit()
    {
        foreach (Transform root in meshRoots)
        {
            foreach (Transform item in root)
            {
                if (!item.gameObject.activeSelf) continue;
                MeshRenderer rend = item.GetComponent<MeshRenderer>();
                if (rend)
                {
                    rend.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
                    rend.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
                    rend.motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;
                }
            }


        }
        gamePhaseSequence.Clear();
        foreach (Transform item in transform)
        {
            PhaseController pc = item.GetComponent<PhaseController>();
            if (pc && item.gameObject.activeSelf)
            {
                gamePhaseSequence.Add(pc);
                pc.EditorInit(gridDivisionUnit);
            }


            PhaseController_Icing ipc =pc as PhaseController_Icing;
            if (ipc && !ipc.vertexCountTaken)
            {
                ipc.targetCount = ipc.targetChoppedMesh.vCount * ipc.defaultVertexCountToTargetConvertionRate;
            }
        }

    }




    public static event System.Action onPhaseEdges;
    public static bool allPhaseOver = false;
    public static bool allGameplayOver = false;
    public void MoveToNextPhase()
    {
        //Debug.Log("<color='blue'> Called next phase</color>");
        if (allPhaseOver) return;
        if(currentPhaseIndex>=0)CurrentPhaseController.OnPhaseEnd();
        onPhaseEdges?.Invoke();
        if (IsAtLastPhase)
        {
            //Debug.Log("<color='blue'> last phase ended</color>");
            allPhaseOver = true;
            return;
        }
        currentPhaseIndex++;
        for (int i = 0; i < gamePhaseSequence.Count; i++)
        {
            PhaseController pc = gamePhaseSequence[i];

            if (i == currentPhaseIndex)
            {
                if (pc.phaseCollider) pc.phaseCollider.gameObject.SetActive(true);
            }
            else if(i < currentPhaseIndex && pc.phase == GamePhase.Flatten && CurrentPhaseController.phase == GamePhase.Flatten)
            {
                if(pc.phaseCollider) pc.phaseCollider.gameObject.SetActive(true);
            }
            else
            {
                if (pc.phaseCollider) pc.phaseCollider.gameObject.SetActive(false);
            }
        }
        if (PhaseManager.CurrentPhaseController.phase == GamePhase.Decorate)
        {
            allGameplayOver = true;
            allGameplayEndTime = Time.time;
        }
       // Debug.Log("<color='blue'> New Phase Started</color>");
        //PhaseManager.CurrentPhaseController.PrintVCount();
        CurrentPhaseController.OnPhaseStart();
    }


    public static void FlattenPhaseTemporalCompletion()
    {
        LevelDefinition def = LevelPrefabManager.currentLevel.levelDefinition;
        AnalyticsAssistant.LevelCompleted(LevelPrefabManager.currentLevel.cumulitiveLevelNo, GameProgressManager.instance.StarCount, def.GetGamePlayType(), def.GetLayerCount());

        GameProgressManager.instance.DoCompletionCelebration();
        PhaseManager.instance.forceUseDefaultCamTrans = true;
        LevelPrefabManager.currentLevel.suspendedExternally = true;
        FRIA.Centralizer.Add_DelayedMonoAct(PhaseManager.instance, OnGameplayComplete, 2.75f, true);

    }
    public static void OnGameplayComplete()
    {
        GameProgressManager.instance.DemandPhaseAdvance();
        LevelPrefabManager.currentLevel.suspendedExternally = false;
        MainGameManager.instance.runningLevelManager.OnComplete(true, "Well Done", null);

        
    }

    public static float GetStarProgress()
    {
        float progs=0;
        int count=0;
        for (int i = 0; i < PhaseManager.instance.gamePhaseSequence.Count; i++)
        {
            PhaseController_Flattening flatPhase = PhaseManager.instance.gamePhaseSequence[i] as PhaseController_Flattening;
            if (flatPhase)
            {
                progs += flatPhase.icingPhaseRef.CalculateIcingMatch();
                count++;
            }
        }
        return progs / count;
    }
    public static int GetProgressStarCount()
    {

        float starProgress = GetStarProgress();

        if (starProgress >= 0.90f) return 3;
        else if (starProgress >= 0.50f) return 2; 
        else if (starProgress >= 0.20f) return 1; 
        else return 0;
    }

    Theme IThemeScript.GetTheme()
    {
        return levelTheme;
    }

    void IThemeScript.SetTheme(Theme theme)
    {
        this.levelTheme = theme;
    }



}





public enum ColChannel
{
    R,G,B,A
}

