﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

namespace Portbliss.DLC
{
    /// <summary>
    /// Must be added to the same game object as the dlcmanager is attached with
    /// </summary>
    public class DlcUIView : MonoBehaviour
    {
        [SerializeField] bool logEnabled = true;
        const bool loadAsap = true;
        static DlcUIView instance;
        [SerializeField] GameObject progressUI, debugUI, blockerPanel;
        [SerializeField] bool isDebugMode = false;
        [SerializeField] Image overallProgImgDebug, thisTaskProgImgDebug;
        [SerializeField] Text statusTxtDebug, percentCompleteTxt, connectionErrorTxt;
        [SerializeField] Button restartBtn, restartBtnDebug, restartForTimeout;
        [SerializeField] float levelReadyCheckInterval = 0.4f;
        [SerializeField] float zeroProgressButtonTime = 7f, otherProgressButtonTime = 5f;
        float timer = 0;
        int checkLevelNum;
        bool willDoPeriodicCheck = false;
        float lastProgress;
        
        public void InitView()
        {
            if (instance == null)
            {
                instance = this;
                progressUI.SetActiveCustom(false);
                blockerPanel.SetActiveCustom(false);
                debugUI.SetActiveCustom(false);
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                DestroyImmediate(this);
                return;
            }

            instance.lastProgress = 0;
            timerOut = 0;
            if (isDebugMode)
            {
                debugUI.SetActiveCustom(true);
                progressUI.SetActiveCustom(false);
                blockerPanel.SetActiveCustom(false);

                instance.willDoPeriodicCheck = true;
                percentCompleteTxt.text = "0.00%";
                connectionErrorTxt.text = "";
                connectionErrorTxt.gameObject.SetActiveCustom(false);
                percentCompleteTxt.gameObject.SetActiveCustom(true);

                instance.timer = 0f;
                if (restartBtn != null)
                {
                    instance.restartBtn.interactable = false;
                }

                if (restartBtnDebug != null)
                {
                    instance.restartBtnDebug.interactable = false;
                }
            }

            if (restartBtn != null)
            {
                restartBtn.onClick.RemoveAllListeners();
                restartBtn.onClick.AddListener(() =>
                {
                    //SceneManager.LoadScene(0);
                });
            }
            

            if (restartBtnDebug != null)
            {
                restartBtnDebug.onClick.RemoveAllListeners();
                restartBtnDebug.onClick.AddListener(() =>
                {
                    //SceneManager.LoadScene(0);
                });
            }

            if (restartForTimeout != null)
            {
                restartForTimeout.onClick.RemoveAllListeners();
                restartForTimeout.onClick.AddListener(() =>
                {
                    //SceneManager.LoadScene(0);
                    restartForTimeout.gameObject.SetActiveCustom(false);
                    DlcManager.ForceRestartDlcDownload();
                });
            }


            if (loadAsap)
            {
                if (restartBtn != null)
                {
                    restartBtn.gameObject.SetActiveCustom(false);
                }

                if (restartBtnDebug != null)
                {
                    restartBtnDebug.gameObject.SetActiveCustom(false);
                }
            }
        }

        float timerOut = 0;
        private void Update()
        {
            if (DlcManager.EnableSystem == false) { return; }
            if (willDoPeriodicCheck && DlcManager.AllDlcDescription != null && DlcManager.AllDlcDescription.ProgressDescription != null)
            {
                float prg = DlcManager.AllDlcDescription.ProgressDescription.GetCurrentProgress();
                float diff = Mathf.Abs(lastProgress - prg);
                if (diff <= 0.0001f)
                {
                    timerOut += Time.deltaTime;
                    float diffZero = Mathf.Abs(prg - 0f);
                    if (diffZero <= 0.0001f)
                    {
                        if (timerOut > zeroProgressButtonTime)
                        {
                            timerOut = 0;
                            restartForTimeout.gameObject.SetActiveCustom(true);
                        }
                    }
                    else
                    {
                        if (timerOut > otherProgressButtonTime)
                        {
                            timerOut = 0;
                            restartForTimeout.gameObject.SetActiveCustom(true);
                        }
                    }
                }
                else
                {
                    timerOut = 0;
                }
                lastProgress = prg;


                timer += Time.deltaTime;
                if (timer > levelReadyCheckInterval)
                {
                    timer = 0f;
                    DlcManager.OnLoadALevel();
                    var desc = DlcManager.AllDlcDescription.GetDescriptorForLevel(checkLevelNum);
                    if (desc != null && desc.DoesItExist())
                    {
                        if (restartBtn != null)
                        {
                            restartBtn.interactable = true;
                        }

                        if (restartBtnDebug != null)
                        {
                            restartBtnDebug.interactable = true;
                        }

                        if (isDebugMode)
                        {
                            debugUI.SetActiveCustom(true);
                        }
                        else
                        {
                            debugUI.SetActiveCustom(false);
                        }

                        progressUI.SetActiveCustom(false);
                        blockerPanel.SetActiveCustom(false);
                        willDoPeriodicCheck = false;

                        if (loadAsap)
                        {
                            //SceneManager.LoadScene(0);
                            if (logEnabled) { Debug.Log("<color='green'>main level bundle has been obtained from local file location.</color>"); }
                            AssetBundle.UnloadAllAssetBundles(false);
                            Resources.UnloadUnusedAssets();

                            if (DlcManager.WillUseSharedBundle)
                            {
                                string shPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName + "/shared.gpkg");
                                AssetBundle sharedBundle = AssetBundle.LoadFromFile(shPath);
                            }

                            var lvMainBundle = AssetBundle.LoadFromFile(desc.DiskUrl);
                            string fName = "Level " + checkLevelNum;
                            var level = lvMainBundle.LoadAsset<GameObject>(fName);

                            _CB?.Invoke(level);
                            _CB = null;
                        }
                        instance.timerOut = 0;
                    }
                }

                if (isDebugMode)
                {
                    if (DlcManager.AllDlcDescription != null && DlcManager.AllDlcDescription.ProgressDescription != null)
                    {
                        var prog = DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription;
                        if (prog != null && thisTaskProgImgDebug != null && overallProgImgDebug != null & statusTxtDebug != null)
                        {
                            statusTxtDebug.text = prog.TaskName;
                            thisTaskProgImgDebug.fillAmount = prog.GetProgress();
                            overallProgImgDebug.fillAmount = DlcManager.AllDlcDescription.ProgressDescription.GetOverallProgress();
                        }
                    }
                }
                else
                {
                    if (DlcManager.AllDlcDescription != null && DlcManager.AllDlcDescription.ProgressDescription != null && percentCompleteTxt != null)
                    {
                        float currentProg = DlcManager.AllDlcDescription.ProgressDescription.GetCurrentProgress();
                        //0.9901 or 99.01  0.9902 or 99.02
                        if (currentProg < 0.999f)
                        {
                            if (DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError)
                            {
                                connectionErrorTxt.text = "Connection Error!" + Environment.NewLine + " Please check your internet connection!";
                                percentCompleteTxt.text = "0.00%";
                                connectionErrorTxt.gameObject.SetActiveCustom(true);
                                percentCompleteTxt.gameObject.SetActiveCustom(false);
                            }
                            else
                            {
                                percentCompleteTxt.text = ((currentProg * 100f).ToString("00.00") + "%");
                                connectionErrorTxt.text = "";
                                connectionErrorTxt.gameObject.SetActiveCustom(false);
                                percentCompleteTxt.gameObject.SetActiveCustom(true);
                            }
                        }
                    }
                }  
            }
        }

        Action<GameObject> _CB;
        public static void ShowProgressUIOnLoadFail(int levelNum, Action<GameObject> OnLoad)
        {
            if (DlcManager.EnableSystem == false) { return; }
            instance.lastProgress = 0;
            instance.checkLevelNum = levelNum;
            instance.willDoPeriodicCheck = true;
            instance.timer = 0f;
            instance._CB = OnLoad;
            instance.percentCompleteTxt.text = "0.00%";
            instance.connectionErrorTxt.text = "";
            instance.connectionErrorTxt.gameObject.SetActiveCustom(false);
            instance.percentCompleteTxt.gameObject.SetActiveCustom(true);
            instance.timerOut = 0;
            if (instance.restartBtn != null)
            {
                instance.restartBtn.interactable = false;
            }

            if (instance.restartBtnDebug != null)
            {
                instance.restartBtnDebug.interactable = false;
            }
            
            if (instance.isDebugMode)
            {
                instance.debugUI.SetActiveCustom(true);
                instance.progressUI.SetActiveCustom(false);
                instance.blockerPanel.SetActiveCustom(false);
                if (instance.thisTaskProgImgDebug != null && instance.overallProgImgDebug != null & instance.statusTxtDebug != null)
                {
                    instance.statusTxtDebug.text = "Please Wait...";
                    instance.thisTaskProgImgDebug.fillAmount = 0;
                    instance.overallProgImgDebug.fillAmount = 0;
                }
            }
            else
            {
                instance.debugUI.SetActiveCustom(false);
                instance.progressUI.SetActiveCustom(true);
                instance.blockerPanel.SetActiveCustom(true);
                if (instance.percentCompleteTxt != null)
                {
                    instance.percentCompleteTxt.text = "0%";
                }   
            }
        }
        
    }
}