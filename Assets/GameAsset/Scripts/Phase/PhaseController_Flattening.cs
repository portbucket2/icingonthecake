﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class PhaseController_Flattening : PhaseController
{
    [Header("PhaseSpecific")]
    public PhaseController_Icing icingPhaseRef;
    public override GamePhase phase { get { return GamePhase.Flatten; } }
    public bool needsIceLoading { get { return !icingPhaseRef.enabled; } }

    internal List<OldIceMesh> meshList = new List<OldIceMesh>();
    public override string ProgressText
    {
        get
        {
            if (opCon.progress >= targetCount)
                return "Good Job!";
            else
                return "Smooth It Out";
        }
    }

    public void LoadGalleryMesh()
    {

    }
    public override void OnPhaseStart()
    {
        base.OnPhaseStart();

        if (needsIceLoading)
        {
            foreach (ColChannel cc in System.Enum.GetValues(typeof(ColChannel)))
            {
                string c;
                switch (cc)
                {
                    case ColChannel.R:
                        c = "R";
                        break;
                    case ColChannel.G:
                        c = "G";
                        break;
                    case ColChannel.B:
                        c = "B";
                        break;
                    case ColChannel.A:
                        c = "A";
                        break;
                    default:
                        c = "X";
                        break;
                }
                string s = string.Format("{0}/p{1}_{2}", LevelPrefabManager.currentLevel.levelDefinition.resourcePath_dataFolder, phaseSetID, c);
                //Debug.Log(s);
                Mesh m =  Resources.Load<Mesh>(s);
                //Debug.Log(m);
                if (m)
                {
                    GameObject mgo = new GameObject(cc.ToString());
                    mgo.transform.position = phaseCollider.transform.position;
                    //mgo.SetLayer("PrimaryOnly");
                    mgo.transform.parent = this.transform;
                    MeshFilter mf = mgo.AddComponent<MeshFilter>();
                    mf.mesh = m;
                    Material material = new Material(PhaseManager.instance.sampleMat);
                    material.color = icingPhaseRef.cols[(int)cc];
                    MeshRenderer mrend = mgo.AddComponent<MeshRenderer>();
                    mrend.material = material;
                    mrend.receiveShadows = false;
                    mrend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    OldIceMesh oim = mgo.AddComponent<OldIceMesh>();
                    meshList.Add(oim);
                    oim.Init(mf.mesh);
                }
                else meshList.Add(null);
            }
        }

        Color map = new Color(0, 0, 0, 0);
        for (int c = 0; c < morfMeshes.Count; c++)
        {
            if (needsIceLoading)
            {
                //Debug.Log(fdata);
                morfMeshes[c].ResetLoadedVcols(fdata.meshColKeeps[c].vcols);
            }
            else
            {
                //morfMeshes[c].CheckSecondaryCols();
            }
            Color[] vcolors = morfMeshes[c].meshFilter.mesh.colors;
            for (int i = 0; i < vcolors.Length; i++)
            {
                map += vcolors[i];
            }
        }



        Material mat = morfMeshes[0].GetComponent<MeshRenderer>().sharedMaterial;
        if (icingPhaseRef.defaultColorIndex >= 0)
        {

            mat.SetColor("_Color0", icingPhaseRef.cols[icingPhaseRef.defaultColorIndex]);
        }
        else
        {
            Color R = new Color(0, 0, 0, 0), G = new Color(0, 0, 0, 0), B = new Color(0, 0, 0, 0), A = new Color(0, 0, 0, 0);

            colList.Clear();
            for (int i = 0; i < icingPhaseRef.cols.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        R = icingPhaseRef.cols[i];
                        colList.Add(R, map.r);
                        break;
                    case 1:
                        G = icingPhaseRef.cols[i];
                        colList.Add(G, map.g);
                        break;
                    case 2:
                        B = icingPhaseRef.cols[i];
                        colList.Add(B, map.b);
                        break;
                    case 3:
                        A = icingPhaseRef.cols[i];
                        colList.Add(A, map.a);
                        break;
                    default:
                        break;
                }
                mat.SetColor(string.Format("_Color{0}", i + 1), icingPhaseRef.cols[i]);
            }
            Color finalCol = colList.Roll();
            mat.SetColor("_Color0", finalCol);
        }

        
    }
    public override void OnPhaseEnd()
    {
        base.OnPhaseEnd();
        phaseCollider.gameObject.SetLayer (PhaseManager.paintLayer2,false);
    }
    //public Vector3 snapMeshScale = Vector3.zero;

    //bool meshScaleWasForced = false;
    //public void ForceScaleNow()
    //{
    //    if (meshScaleWasForced) return;

    //    meshScaleWasForced = true;
    //    phaseMesh.transform.localScale = snapMeshScale;
    //}

}
