﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessBoxController : MonoBehaviour
{
    static bool vipToken = false;
    public static bool ConsumeVIPToken()
    {
        if (vipToken)
        {
            vipToken = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public GameObject successObject;
    public Button bannerNextButton;


    [Header("Match Screen")]
    public List<GameObject> group1;
    public List<Animator> stars;
    public Button buttonProceed_match;


    [Header("Report Screen")]
    public List<GameObject> group2;
    public Image progressImage;
    public Text progressText;
    public Text yourTimeText;
    public Text wolrdTimeText;
    public Text finalMathText;
    public Button buttonRV_decor;
    public Button buttonProceed_report;

    [Header("Post Decorate Screen")]
    public Button shareOnNetworkButton;
    public Button skipToNextButton;
    public GameObject decoratePanel;





    [Header("RV Level")]
    public GameObject rvLevelPanel;
    public Button buttonGET_rvlevel;
    public Button buttonSKIP_rvlevel;

    [Header("RV Fake Icing")]
    public GameObject rvFakeIcingPanel;
    public Button buttonGET_rvFakeIcing;
    public Button buttonSKIP_rvFakeIcing;

    [Header("RV Tool")]
    public Image toolImage;
    public GameObject rvToolPanel;
    public Button buttonGET_rvtool;
    public Button buttonSKIP_rvtool;

    [Header("Unlock Notifier")]
    public GameObject unlockNotiferRoot;
    public Image unlockNotifierImage;
    private void OnDisable()
    {

        foreach (var item in group1) item.SetActive(false);
        foreach (var item in group2) item.SetActive(false);
    }
    public enum RVPlacement
    {
        decoration = 1,
        bonus_level = 2,
        bonus_tool = 3,
        bonus_fakeIcing =4,
    }
    private void Awake()
    {
        unlockNotiferRoot.SetActive(false);
        buttonProceed_match.onClick.RemoveAllListeners();
        buttonProceed_match.onClick.AddListener(ProceedFromMatch);

        buttonProceed_report.onClick.RemoveAllListeners();
        buttonProceed_report.onClick.AddListener(ProceedFromReport);

       



        buttonRV_decor.onClick.RemoveAllListeners();
        buttonRV_decor.onClick.AddListener(()=> { OnRV(buttonRV_decor, RVPlacement.decoration); });

        buttonSKIP_rvlevel.onClick.RemoveAllListeners();
        buttonSKIP_rvlevel.onClick.AddListener(NextLevel);
        buttonSKIP_rvtool.onClick.RemoveAllListeners();
        buttonSKIP_rvtool.onClick.AddListener(NextLevel);
        buttonSKIP_rvFakeIcing.onClick.RemoveAllListeners();
        buttonSKIP_rvFakeIcing.onClick.AddListener(NextLevel);

        buttonGET_rvlevel.onClick.RemoveAllListeners();
        buttonGET_rvlevel.onClick.AddListener(() => OnRV(buttonGET_rvlevel, RVPlacement.bonus_level)); 
        buttonGET_rvtool.onClick.RemoveAllListeners();
        buttonGET_rvtool.onClick.AddListener(() => OnRV(buttonGET_rvtool, RVPlacement.bonus_tool));
        buttonGET_rvFakeIcing.onClick.RemoveAllListeners();
        buttonGET_rvFakeIcing.onClick.AddListener(() => OnRV(buttonGET_rvFakeIcing, RVPlacement.bonus_fakeIcing));


        shareOnNetworkButton.onClick.RemoveAllListeners();
        shareOnNetworkButton.onClick.AddListener(OnShareButton);

        skipToNextButton.onClick.RemoveAllListeners();
        skipToNextButton.onClick.AddListener(()=>{
            //MainPanelSetActive(true);
            ProceedFromReport();
        });

        LevelPrefabManager.newPrefabLoading += OnNewPrefabLoad;
    }

    void OnNewPrefabLoad(Theme theme)
    {
        FRIA.Centralizer.Add_DelayedMonoAct(this, () => {
            if (smoothProfile != null && smoothProfile.IsUnlocked)
            {
                unlockNotiferRoot.SetActive(true);
                unlockNotifierImage.sprite = smoothProfile.proPic;
            }
            smoothProfile = null;
        }, 0.75f);
    }
    private void OnDestroy()
    {
        LevelPrefabManager.newPrefabLoading -= OnNewPrefabLoad;
    }
    void OnShareButton()
    {
        Debug.Log("<color='cyan'>Sharing to facebook: needs implementation</color>");
        //Portbliss.Social.GifShareControl.EndRecord();
        //Portbliss.Social.GifShareControl.ShareGif();
    }

    void OnRV(Button button, RVPlacement type)
    {
        button.interactable = false;

        AnalyticsAssistant.LogRewardedVideoAdStart(LevelPrefabManager.currentLevel.cumulitiveLevelNo);

        if (Application.isEditor)
        {
            OnAdSuccess(true, button, type);
        }
        else
        {
            Portbliss.Ad.AdController.ShowRewardedVideoAd((bool success)=> { OnAdSuccess(success, button, type); });
        }
    }

    void OnAdSuccess(bool success, Button butt, RVPlacement type)
    {
        if(!success)
        {

            butt.interactable = true;
            return;
        }
        else 
        {
            AnalyticsAssistant.LogRewardedVideoAdComplete(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
            switch (type)
            {
                case RVPlacement.decoration:
                    MainPanelSetActive(false);
                    PhaseController_Decorate decPhase = (PhaseManager.CurrentPhaseController as PhaseController_Decorate);
                    if (decPhase)
                    {
                        decPhase.LoadDefaultDecoration(GalleryManager.instance.SaveGalleryItemData());
                    }
                    break;
                case RVPlacement.bonus_level:
                    VipToken = true;
                    NextLevel();
                    break;
                case RVPlacement.bonus_fakeIcing:
                    NextLevel();
                    break;
                case RVPlacement.bonus_tool:
                    smoothProfile.Unlock();
                    SmoothToolCanvasManager.Instance.OnSelection(smoothProfile);
                    NextLevel();
                    break;
            }

        }
        GameConfig.hasRewardedVideoAdBeenShown = success;
    }

    void MainPanelSetActive(bool active)
    {
        successObject.SetActive(active);
        MaxRecController.Active(active);
    }

    public void Show_Success()
    {

        rvLevelPanel.SetActive(false);
        rvToolPanel.SetActive(false);
        rvFakeIcingPanel.SetActive(false);
        MainPanelSetActive(true);

        foreach (var item in group2) item.SetActive(false);
        foreach (var item in group1) item.SetActive(true);

        float matchRatio = PhaseManager.GetStarProgress();
        StopAllCoroutines();
        StartCoroutine(AccuracyCo(matchRatio));

        finalMathText.text = string.Format("<size=45>Match</size>\n<color=#961672>{0}%</color>", (int)(matchRatio * 100));
        yourTimeText.text = string.Format("<size=45>Time</size>\n<color=#961672>{0} <size=36>sec</size></color>", PhaseManager.instance.GetAllPhaseTotalTime());
        wolrdTimeText.text = string.Format("<size=44>World Avg</size>\n<color=#961672>{0} <size=36>sec</size></color>", LevelPrefabManager.currentLevel.levelDefinition.worldAvgTimeText);


        for (int i = 0; i < PhaseManager.GetProgressStarCount(); i++)
        {
            stars[i].ResetTrigger("go");
            stars[i].SetTrigger("reset");
        }
        for (int i = 0; i < PhaseManager.GetProgressStarCount(); i++)
        {
            int index = i;
            FRIA.Centralizer.Add_DelayedMonoAct(this, () => stars[index].SetTrigger("go"), index * 0.5f + 0.1f);
        }

    }
    IEnumerator AccuracyCo(float fraction)
    {
        progressImage.fillAmount = 0;
        progressText.text = string.Format("Match: 0%");
        yield return new WaitForSeconds(0.5f);
        float T = 2f;
        float st = Time.time;

        float progress;
        while (Time.time<st+T)
        {
            progress = Mathf.Clamp01((Time.time-st)/ T)*fraction;
            progressImage.fillAmount = progress;
            progressText.text = string.Format("Match: {0}%", (int)(progress*100));
            yield return null;
        }
        progress = fraction;
        progressImage.fillAmount = progress;
        progressText.text = string.Format("Match: {0}%", (int)(progress*100));
    }

    public void ProceedFromMatch()
    {
        buttonRV_decor.interactable = true;
        foreach (var item in group1) item.SetActive(false);
        foreach (var item in group2) item.SetActive(true);
    }

    SmoothToolProfile smoothProfile;

    public static bool VipToken { get => vipToken; set => vipToken = value; }
    public void ProceedFromReport()
    {
        switch (LevelPrefabManager.currentLevel.loadType)
        {
            case LoadType.GALLERY:
                {
                    decoratePanel.SetActive(false);
                    NextLevel();
                }
                break;
            case LoadType.NORMAL:
                switch (LevelPrefabManager.currentLevel.bonusType)
                {
                    case VideoBonusType.LEVEL:
                        {
                            CommonOnBonusScreen();
                            rvLevelPanel.SetActive(true);
                            buttonGET_rvlevel.interactable = true;
                        }
                        break;
                    case VideoBonusType.TOOL:
                        {
                            CommonOnBonusScreen();
                            smoothProfile = SmoothToolCanvasManager.Instance.NextProfile();
                            toolImage.sprite = smoothProfile.proPic;
                            rvToolPanel.SetActive(true);
                            buttonGET_rvtool.interactable = true;
                        }
                        break;
                    case VideoBonusType.ICING:
                        {
                            CommonOnBonusScreen();
                            rvFakeIcingPanel.SetActive(true);
                            buttonGET_rvFakeIcing.interactable = true;
                        }
                        break;
                    case VideoBonusType.NONE:
                        {
                            NextLevel();
                        }
                        break;

                }
                break;
        }


    }
    void CommonOnBonusScreen()
    {
        MainPanelSetActive(false);
        LevelPrefabManager.currentLevel.gameObject.SetActive(false);
        //starmod.SetActive(false);
        //foreach (var item in group1) item.SetActive(false);
        //foreach (var item in group2) item.SetActive(false);
    }
    public void NextLevel()
    {
        HideAll();
        LevelPrefabManager.currentLevel.OnNext();

        //Debug.Log("GOT NEXTTTT");
    }

    public void HideAll()
    {
        rvToolPanel.SetActive(false);
        rvLevelPanel.SetActive(false);
        rvFakeIcingPanel.SetActive(false);
        MainPanelSetActive(false);
        this.bannerNextButton.gameObject.SetActive(false); 
        decoratePanel.SetActive(false);
        //Debug.Log("GOT NEXTTTT");
        GameProgressManager.instance.confettiSystem1.Clear();
        GameProgressManager.instance.confettiSystem2.Clear();
        foreach ( ParticleSystem p in GameProgressManager.instance.confettiChristmas )
        {
            p.Stop ();
            p.Clear ();
        }

        
    }


}
