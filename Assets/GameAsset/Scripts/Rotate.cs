﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateForReporter : MonoBehaviour
{
    public Vector3 angles;

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(angles * Time.deltaTime);
    }
}
