﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VertexGroup
{
    public Vector3 groupPostion;
    public int[] vertexIndices;

    [System.NonSerialized] List<int> tempVertexIndices = new List<int>();

    public VertexGroup(float x, float y, float z, float scale)
    {
        groupPostion = new Vector3(x * scale, y * scale, z * scale);
    }


    public void Add(int i)
    {
        tempVertexIndices.Add(i);
    }
    public void Consolidate()
    {
        //Debug.Log(tempVertexIndices.Count);
        vertexIndices = tempVertexIndices.ToArray();
        tempVertexIndices = null;
    }
}