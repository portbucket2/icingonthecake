﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestABController : MonoBehaviour
{
    public GameObject root;

    public Button applyButton;
    // Start is called before the first frame update
    public static bool isAB_trialValueSetupPending = false;
    IEnumerator Start()
    {
        root.SetActive(false);
        if (SpecialBuildManager.enableABTestUI && modules.Count>0)
        {
            isAB_trialValueSetupPending = true;
            while (!ABManager.isDataFetchComplete)
            {
                yield return null;
            }
            root.SetActive(true);
            LoadModules();
            applyButton.onClick.AddListener(OnApply);
            for (int i = modules.Count -1 ; i >=0; i--)
            {
                if (!modules[i].loaded)
                {
                    modules[i].gameObject.SetActive(false);
                    modules.RemoveAt(i);
                }
            }
            if (modules.Count == 0) OnApply();
        }

    }
    public List<TestABDataChangeUI> modules;
    void LoadModules()
    {
        LoadModuleWith_AdConfig(0);
        //if(BuildSceneInfo.Instance.sequenceMode == SequenceMode.AB) LoadModuleWith_LevelSequence(0);
        //LoadModuleWith_StepCountChoice(0);
        //LoadModuleWith_AdIntSuccess(1);
        //LoadModuleWith_AdIntFail(2);
        //homecomingModule.Load(ABtype.home_return_freq, new string[] {"0","1","2","3","4","5"} , 
        //    (int selectionIndex)=> 
        //    {
        //        switch (selectionIndex)
        //        {
        //            default:
        //                return string.Format("every {0} level",selectionIndex);
        //            case 0:
        //                return string.Format("disable auto-return");
        //            case 1:
        //                return string.Format("every level");
        //        }
        //    });
        //reviveModule.Load(ABtype.coin_revive_enable, new string[] { "0", "1"},
        //    (int selectionIndex) =>
        //    {
        //        switch (selectionIndex)
        //        {
        //            default:
        //            case 1:
        //                return string.Format("enabled");
        //            case 0:
        //                return string.Format("disabled");
        //        }
        //    });
    }
    void LoadModuleWith_AdConfig(int i)
    {
        modules[i].Load(ABtype.Ad_Config, new string[] { "1", "2", "3" },
            (int selectionIndex) =>
            {
                switch (selectionIndex)
                {
                    default:
                        return string.Format("config {0}", selectionIndex+1);
                    case 0:
                        return string.Format("lvl 1, 15 s");
                    case 1:
                        return string.Format("lvl 3, 15 s");
                    case 2:
                        return string.Format("lvl 1, 10 s");

                }
            });
    }






    void OnApply()
    {
        foreach (var item in modules)
        {
            if(item.loaded) item.OnApply();
        }
        root.SetActive(false);
        isAB_trialValueSetupPending = false;
    }

}
