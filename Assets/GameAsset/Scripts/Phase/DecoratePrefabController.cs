﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class DecoratePrefabController : MonoBehaviour
{
    public string variant = "a";
    private string decoID = "a";
    public float baseAnimationFallHeight = 5;
    public List<BasicDecoPlacementAnimData> basicAnimatationItems;

    public float StartAnimate(System.Action callback)
    {
        float maxDelay = 0;
        for (int i = 0; i < basicAnimatationItems.Count; i++)
        {
            StartCoroutine(BasicFallToPlaceAnimation(basicAnimatationItems[i]));
            if (basicAnimatationItems[i].delay + basicAnimatationItems[i].animateTime > maxDelay) maxDelay = basicAnimatationItems[i].delay + basicAnimatationItems[i].animateTime;
        }

        FRIA.Centralizer.Add_DelayedMonoAct(this,()=> { callback?.Invoke(); },maxDelay);

        return maxDelay;
    }

    private IEnumerator BasicFallToPlaceAnimation(BasicDecoPlacementAnimData data)
    {
        Vector3 finalPos = data.tr.position;
        Vector3 initialPos = data.tr.position + new Vector3(0,baseAnimationFallHeight,0);
        data.tr.position = initialPos;
        //Debug.Log("<color = 'blue'> initing</color>");
        yield return new WaitForSeconds(data.delay);
        //Debug.Log("<color = 'blue'> starting</color>");
        float startTime = Time.time;
        float progress;
        while (Time.time<startTime+ data.animateTime)
        {
            progress = Mathf.Clamp01((Time.time-startTime)/ data.animateTime);
            data.tr.position = Vector3.Lerp(initialPos, finalPos, progress);
            yield return null;
        }
        //Debug.Log("<color = 'blue'> ending</color>");
        data.tr.position = finalPos;
    }

    private List<Material> relevantMats;


    public void Initiate(string variant, string decoID, bool sharedMat)
    {
        this.variant = variant;
        this.decoID = decoID;
        relevantMats = new List<Material>();


        foreach (var item in transform.GetComponentsInChildren<MeshRenderer>())
        {
            relevantMats.AddRange(sharedMat?item.sharedMaterials:item.materials);
        }
        LoadColorVariation();
    }

    //public DecorationColorVariation colorVarData;
    string ColVarPath
    {
        get
        {
            return string.Format("Decorations/{0}/{1}/{2}",
             LevelPrefabManager.currentLevel.levelDefinition.baseBreakdownID,
             decoID,
             variant);
        }
    }
    public void LoadColorVariation()
    {
        DecorationColorVariation colorVarData = Resources.Load<DecorationColorVariation>(ColVarPath);
        //Debug.LogFormat("<color='blue'>{0},{1}</color>",colorVarData.standardMatData.Length,relevantMats.Count);
        if (colorVarData)
        {
            for (int i = 0; i < colorVarData.standardMatData.Length; i++)
            {
                relevantMats[i].SetColor("_Color", colorVarData.standardMatData[i].mainColor);
                relevantMats[i].SetColor("_EmissionColor",colorVarData.standardMatData[i].emissionColor);
                relevantMats[i].SetFloat("_Metallic", colorVarData.standardMatData[i].metallic);
                relevantMats[i].SetFloat("_Glossiness", colorVarData.standardMatData[i].smoothness);
            }
        }
    }

#if UNITY_EDITOR
    public void SaveColorVariation()
    {
        DecorationColorVariation colorVarData = ScriptableObject.CreateInstance<DecorationColorVariation>();
        colorVarData.standardMatData = new StandardMatData[relevantMats.Count];
        for (int i = 0; i < relevantMats.Count; i++)
        {
            colorVarData.standardMatData[i] = new StandardMatData();
            colorVarData.standardMatData[i].mainColor = relevantMats[i].GetColor("_Color");
            colorVarData.standardMatData[i].emissionColor = relevantMats[i].GetColor("_EmissionColor");
            colorVarData.standardMatData[i].metallic = relevantMats[i].GetFloat("_Metallic");
            colorVarData.standardMatData[i].smoothness = relevantMats[i].GetFloat("_Glossiness");
        }
        AssetDatabase.CreateAsset(colorVarData, string.Format("Assets/Resources/{0}.asset", ColVarPath));
        AssetDatabase.SaveAssets();
    }
#endif
}
[System.Serializable]
public class BasicDecoPlacementAnimData
{
    public Transform tr;
    public float delay;
    public float animateTime;
}

#if UNITY_EDITOR
[CustomEditor(typeof(DecoratePrefabController))]
public class DecoPrefabController_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        DecoratePrefabController decoCon = (DecoratePrefabController)target;
        if (GUILayout.Button("Save Colors", GUILayout.Height(50)))
        {
            decoCon.SaveColorVariation();
        }
        if (GUILayout.Button("Load Colors", GUILayout.Height(50)))
        {
            decoCon.LoadColorVariation();
        }
    }
}
#endif