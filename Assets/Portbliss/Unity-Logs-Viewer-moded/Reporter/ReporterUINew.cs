﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.DebugCode
{
    [ExecuteInEditMode]
    public class ReporterUINew : MonoBehaviour
    {
        [SerializeField] Image panel;
        [SerializeField] GameObject startDebugging, stopDebugging;
        void Bind()
        {
            panel.raycastTarget = false;
            panel.enabled = false;
            startDebugging.SetActive(true);
            stopDebugging.SetActive(false);
            var btnStart = startDebugging.GetComponent<Button>();
            btnStart.onClick.RemoveAllListeners();
            btnStart.onClick.AddListener(() =>
            {
                Reporter.instance.StartReporter();
            });

            var btnStop = stopDebugging.GetComponent<Button>();
            btnStop.onClick.RemoveAllListeners();
            btnStop.onClick.AddListener(() =>
            {
                Reporter.instance.StopReporter();
            });
        }

        // Start is called before the first frame update
        void Start()
        {
            Bind();
        }

        private void OnEnable()
        {
            Bind();
        }
    }
}