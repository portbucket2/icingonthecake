﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriList<T>
{
    private T[] arr;
    public int maxX { get; private set; }
    public int maxY { get; private set; }
    public int maxZ { get; private set; }
    public int minX { get; private set; }
    public int minY { get; private set; }
    public int minZ { get; private set; }


    public int length;
    int X_Length;
    int Y_Length;
    int Z_Length;

    int X_Offset;
    int Y_Offset;
    int Z_Offset;

    //int N;
    //int P;
    public TriList(int minX, int maxX, int minY, int maxY, int minZ, int maxZ)
    {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.minZ = minZ;
        this.maxZ = maxZ;

        X_Length = maxX - minX + 1;
        Y_Length = maxY - minY + 1;
        Z_Length = maxZ - minZ + 1;

        X_Offset = minX;
        Y_Offset = minY;
        Z_Offset = minZ;
        //N = maxDim;
        //P = indexOffset;
        length = X_Length * Y_Length * Z_Length;
        //Debug.LogFormat("GroupCount = {0}", length);
        arr = new T[length];
        //Debug.LogFormat("GroupCount = {0}", arr.Length);
    }

    // Define the indexer to allow client code to use [] notation.
    public T this[int i, int j, int k]
    {
        get
        {
            i -= X_Offset;
            j -= Y_Offset;
            k -= Z_Offset;
            if (i >= X_Length || j >= Y_Length || k >= Z_Length)
            {
                throw new System.Exception("index is out of limit");
            }
            return arr[i * Y_Length * Z_Length + j * Z_Length + k];
        }
        set
        {
            i -= X_Offset;
            j -= Y_Offset;
            k -= Z_Offset;
            if (i >= X_Length || j >= Y_Length || k >= Z_Length)
            {
                throw new System.Exception("index is out of limit");
            }
            arr[i * Y_Length * Z_Length + j * Z_Length + k] = value;
        }
    }
    public T this[int i]
    {
        get
        {
            return arr[i];
        }
        set
        {
            arr[i] = value;
        }
    }
}