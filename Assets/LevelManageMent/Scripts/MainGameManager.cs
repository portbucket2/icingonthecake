﻿using Portbliss.Ad;
using Portbliss.IAP;
using System;
using System.Collections;
using System.Collections.Generic;
using FRIA;
using UnityEngine;

public class MainGameManager : MonoBehaviour
{
    public static QuickSettings settings;
    public static MainGameManager instance;
    public InLevelCanvasManager levelCanvas;
    public List<GameObject> loaderObjects;
    public List<GameObject> levelObjects;

    public AutoAreaLoader areaLoader;
    public QuickSettings settingsRef;
    // Start is called before the first frame update
    public GameObject singularPlayButton;
    public GameObject multiplePlayArray;
    public static float firstBootTime { get; private set; }
    public static HardData<bool> ratingShown;
    public static bool willShowAd_IAP { get; set; }
    void Awake()
    {
        Application.targetFrameRate = 300;
        instance = this;
        foreach (var item in levelObjects) item.SetActive(false);
        settings = settingsRef;
        firstBootTime = Time.time;
        LevelPrefabManager.hasLevelProgressOccured = true;

        willShowAd_IAP = true;
        willShowAd_IAP = !(GameConfig.hasIAP_NoAdPurchasedHD.value);
        ratingShown = new HardData<bool>("RATING_ASKED", false);
    }

    IEnumerator WaitAndShowBanner()
    {
        while (true)
        {
            if(AdController.IsSDK_Ready && UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 1
                    && AdController.gdpr_done)
            {
                break; 
            }
            yield return null;
        }

        if (willShowAd_IAP == false)
        {
            AdController.HideBanner();
        }
        else if (AdController.IsBanerAdShowing == false)
        {
            AdController.ShowBanner();
        }

    }
    private void Start()
    {
        willShowAd_IAP = !(GameConfig.hasIAP_NoAdPurchasedHD.value);
        if (GameConfig.IAP_PurchasedOrNot_HasBeenCheckedHD.value == false)
        {
            GameConfig.IAP_PurchasedOrNot_HasBeenCheckedHD.value = true;
            if (IAP_Controller.instance != null)
            {
                IAP_Controller.instance.IsNoAdPurchased((success) =>
                {
                    if (success)
                    {
                        GameConfig.hasIAP_NoAdPurchasedHD.value = true;
                        willShowAd_IAP = false;
                    }
                });
            }  
        }

        if (LevelLoader.instance.testMode)
        {
            singularPlayButton.SetActive(false);
            multiplePlayArray.SetActive(true);
        }
        else
        {
            singularPlayButton.SetActive(true);
            multiplePlayArray.SetActive(false);
        }
        StartCoroutine(WaitAndShowBanner());
    }

    public LevelPrefabManager runningLevelManager
    {
        get
        {
            return LevelPrefabManager.currentLevel;
        }
    }


    public void StartLevel(LoadType loadType, int areaIndex,int levelIndex, LevelDefinition levelDefinition, GameObject resourcePrefab)
    {
        //Debug.Log("A");

        if (runningLevelManager) runningLevelManager.UnLoad();
        if(loadType != LoadType.GALLERY) AnalyticsAssistant.LevelStarted(LevelLoader.GetLevelCumulativeNumber(areaIndex, levelIndex),levelDefinition.GetGamePlayType(), levelDefinition.GetLayerCount());
        foreach (var item in levelObjects) item.SetActive(true);
        GameObject prefab = LevelLoader.instance.commonPrefab;
        Instantiate(prefab, Vector3.zero, Quaternion.identity).GetComponent<LevelPrefabManager>().LoadAs(loadType, areaIndex,levelIndex, levelDefinition, resourcePrefab, EndLevel);

        foreach (var item in loaderObjects) item.SetActive(false);
    }
    public void EndLevel()
    {
        foreach (var item in levelObjects) item.SetActive(false);
        foreach (var item in loaderObjects) item.SetActive(false);
        areaLoader.RestartLoader();

    }

}

public class TimedAd
{
    public static TimedAd instance;
    public float lastTime = 0;
    //public const float interval = 10;
    public static void InitSys()
    {
        if (instance == null)
        {
            instance = new TimedAd();
            instance.lastTime = 0;
        }
    }


    static void AdCore(Action<bool> OnComplete)
    {
        instance.lastTime = Time.time;
        IdleChecker.instance.allowPeriodicHand = false;
        AdController.ShowInterstitialAd((success) =>
        {
            instance.lastTime = Time.time;
            IdleChecker.instance.resetEverything();
            GameConfig.adWatchCountHD.value = GameConfig.adWatchCountHD.value + 1;
            AnalyticsAssistant.LogAdWatchOneTimeAppsFlyerIfApplicable(GameConfig.adWatchCountHD.value);
            OnComplete?.Invoke(success);
        });
    }

    static int FirstAdMinimumLevelCrossing 
    {
        get
        {
            switch (ABManager.GetValueInt(ABtype.Ad_Config))
            {
                default:
                case 1:
                case 3:
                    return 1;
                case 2:
                    return 3;
            }
        }
    }
    static float AdCooldown
    {
        get
        {
            switch (ABManager.GetValueInt(ABtype.Ad_Config))
            {
                default:
                case 1:
                case 2:
                    return 15.0f;
                case 3:
                    return 10.0f;
            }
        }
    }

    public static void AdIteration(Action<bool> OnComplete)
    {
        //Debug.LogWarning("we called ad iteration.");
        if (instance == null)
        {
            instance = new TimedAd();
            instance.lastTime = Time.time;
            return;
        }
        Debug.LogErrorFormat("AD CONFIG: {2}/{0}-{3}/{1}",FirstAdMinimumLevelCrossing,AdCooldown, LevelLoader.instance.GetLastLevelCumulativeNumber(), Time.time-instance.lastTime);
        bool willPlayAd = false;
        int firstBootTime = 0;
        if (Time.time > instance.lastTime + AdCooldown && LevelLoader.instance.GetLastLevelCumulativeNumber() > FirstAdMinimumLevelCrossing
            && LevelPrefabManager.hasLevelProgressOccured == true 
            && Mathf.Abs(MainGameManager.firstBootTime - Time.time) > firstBootTime && GameConfig.hasRewardedVideoAdBeenShown == false)
        {
            willPlayAd = true;
        }

        if (willPlayAd)
        {
            AdCore(OnComplete);
        }
        else
        {
            Debug.Log("so we wont play ad at this time since condition is not favorable");
            OnComplete?.Invoke(false);
        }
    }
}