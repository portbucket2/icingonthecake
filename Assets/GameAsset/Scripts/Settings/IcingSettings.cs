﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IcingSettings : ScriptableObject
{

    [Header("Peel Characterstics")]
    public float peelDivisionLength = 0.03f;
    public int arcVertices = 8;

    [Header("Smoothness")]
    public float widthSmoothingLerpRate = 20;
    public float positionSmoothingLerpRate = 10f;

    [Header("Cut Off Limits")]
    public int fiberCountCutOffLimit = 1000;
    public float lengthCutOffLimit = 10000;
    public float transitionLength = 0.25f;
    //public float lengthCutOff_deviation = 0;
    public float angleCutOffLimit = 45;
    public float sustainRegainTime = 0.05f;
}