#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("st/wDZWEWzfFnazvB01B6ehHfH3heatSWAJm9u+CUsdqfAXSszsqR9oqtf273SfU2S2zPoRxfvygrRUwMEJwW/4yVSwPs1JDFhOtl72jBBRbfUIJYweRBNXCBo6JqdzOhsG0Q8Jw89DC//T72HS6dAX/8/Pz9/LxcPP98sJw8/jwcPPz8l+x/LnX3R2iHOv146bZU6SDFU7VFMwGU9hxL94M8WP4djZPCmOWtAS7djTQO+th+PDM8elcyxF/9SNaaP9jQaAcUc3QlMtOXAaMtpxXDrleOLTv/1YEKQB+X2kIMpNXAIA+Jo+D8D5/Uz96b6F24UXlKHHSjHvjjvRfWJAq5zvYcWkeOWmLGMmvcWLkOyd36Im4a7Cjqym4qG48lfDx8/Lz");
        private static int[] order = new int[] { 3,6,12,5,7,5,6,10,10,9,12,12,13,13,14 };
        private static int key = 242;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
