﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class DecorationColorVariation : ScriptableObject
{
    public StandardMatData[] standardMatData;

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Assets/Scriptable/Decoration Color Variation")]
    public static void Create()
    {
        DecorationColorVariation so = ScriptableObject.CreateInstance<DecorationColorVariation>();
        UnityEditor.AssetDatabase.CreateAsset(so, "Assets/Decor.asset");
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.EditorUtility.FocusProjectWindow();
        UnityEditor.Selection.activeObject = so;
    }
#endif
}

[System.Serializable]
public class StandardMatData
{
    public Color mainColor;
    public Color emissionColor;
    public float smoothness;
    public float metallic;
}