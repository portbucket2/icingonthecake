﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IcingFibers
{
    public const int ARC_N = 20;
    public const float ITERATE_TIME_BUDGET = 1;

    public float lastRecordedLength = 0;
    public Vector3 basePoint;
    public Vector3 hitNormal;
    public Vector2 uv_U;
    public Vector2 uv_D;

    public float halfPeelWidth;

    public Vector3 upOffset;
    public Vector2[] offset2D;
    public Vector3[] posOffset;
    public Vector3[] vertices;
    public float[] vTimeBudgetSpent;
    public Vector3[] normals;
    public Vector2[] uvs;
    public float[] updatedNormalHeight;


    public static float startAngle = Mathf.PI * 5 / 4;
    public static float endAngle = -(Mathf.PI / 4);

    Vector3 upOffsetNormalized;
    Vector3 hitNormalNormalized;
    float upOffsetMagnitude;

    public Vector3 ellipseDimensions = new Vector3(1.0f, 1.3f, 0.6f);
    public float edgeScaleFactor ;


    
    
    public IcingFibers(RaycastHit hit, Transform root, float halfPeelWidth) 
    {
        this.edgeScaleFactor = 1;
        this.halfPeelWidth = halfPeelWidth;
        this.hitNormal = root.InverseTransformDirection(hit.normal);
        this.hitNormalNormalized = hitNormal.normalized;



        this.basePoint = root.InverseTransformPoint(hit.point + this.hitNormal * IcingBuilder.EXTRUDE_CONST);

        offset2D = new Vector2[ARC_N];
        posOffset = new Vector3[ARC_N];
        vertices = new Vector3[ARC_N];
        vTimeBudgetSpent = new float[ARC_N];
        normals = new Vector3[ARC_N];
        uvs = new Vector2[ARC_N];
        updatedNormalHeight = new float[ARC_N];


        this.upOffset = Vector3.Cross(Vector3.up, this.hitNormal).normalized * halfPeelWidth;

        if (upOffset.magnitude < halfPeelWidth)
        {
            this.upOffset = Vector3.Cross(Vector3.forward, this.hitNormal).normalized * halfPeelWidth;
        }

        this.upOffsetNormalized = upOffset.normalized;
        this.upOffsetMagnitude = upOffset.magnitude;

        UpdateAllVerticesWithExtendedElements();

    }

    public IcingFibers(IcingFibers origin, float scaleFactor, Vector3 coreShift)
    {
        this.edgeScaleFactor = scaleFactor;
        this.halfPeelWidth = origin.halfPeelWidth;
        this.hitNormal = origin.hitNormal;
        this.basePoint = origin.basePoint + coreShift;
        this.upOffset = origin.upOffset;
        this.ellipseDimensions = origin.ellipseDimensions;

        this.hitNormalNormalized = hitNormal.normalized;
        this.upOffsetNormalized = upOffset.normalized;
        this.upOffsetMagnitude = upOffset.magnitude;

        offset2D = new Vector2[ARC_N];
        posOffset = new Vector3[ARC_N];
        vertices = new Vector3[ARC_N];
        vTimeBudgetSpent = new float[ARC_N];
        normals = new Vector3[ARC_N];
        uvs = new Vector2[ARC_N];
        updatedNormalHeight = new float[ARC_N];
        for (int i = 0; i < ARC_N; i++)
        {
            offset2D[i] = origin.offset2D[i] *edgeScaleFactor;
            normals[i] = origin.normals[i];
            uvs[i] = origin.uvs[i];
            UpdateFiberAtIndex(i);

        }
    }





    public void UpdateAllVerticesWithExtendedElements()
    {
        for (int i = 0; i < ARC_N; i++)
        {
            float angle;
            if (i == 0)
            {
                angle = (Mathf.PI * 11 / 8);
                uvs[i] = Vector2.Lerp(uv_U, uv_D, 0.25f);
            }
            else if (i == ARC_N - 1)
            {

                angle = -(Mathf.PI * 3 / 8);
                uvs[i] = Vector2.Lerp(uv_U, uv_D, 0.75f);
            }
            else
            {
                angle = Mathf.Lerp(startAngle, endAngle, (i - 1) / (ARC_N - 3.0f));

                uvs[i] = Vector2.Lerp(uv_U, uv_D, (i - 1) / (ARC_N - 3.0f));
            }
            offset2D[i] = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

            UpdateFiberAtIndex(i);
        }
    }

    public void UpdateFiberAtIndex(int i)
    {
        //Vector3 magSettings = Vector3.Lerp(initialMags, finalMags, changeProgress[0]);
        Vector2 ellipseMags = new Vector2();
        ellipseMags.x = offset2D[i].x / ellipseDimensions.x; //cos$/a
        if (offset2D[i].y > 0)
            ellipseMags.y = offset2D[i].y / ellipseDimensions.y;//cos$/b
        else
            ellipseMags.y = offset2D[i].y / ellipseDimensions.z; //cos$/b
        float r = 1 / ellipseMags.magnitude; //1/sqrt(m^2+n^2)

        float MagOff = r * edgeScaleFactor * upOffsetMagnitude;// * (Handy.Deviate(1, .065f));
        posOffset[i] = (offset2D[i].y * hitNormalNormalized - offset2D[i].x * upOffsetNormalized) * MagOff;
        vertices[i] = basePoint + posOffset[i];
        normals[i] = posOffset[i].normalized;
        updatedNormalHeight[i] = posOffset[i].magnitude * offset2D[i].y;
    }


    public void SetUpOffset(Vector3 offset)
    {
        this.upOffset = offset;

        this.upOffsetNormalized = upOffset.normalized;
        this.upOffsetMagnitude = upOffset.magnitude;
    }
    public void UpdateUV(Vector2 uvup, Vector2 uvdown)
    {
        this.uv_U = uvup;
        this.uv_D = uvdown;
        for (int i = 0; i < ARC_N; i++)
        {
            uvs[i] = Vector2.Lerp(uv_U, uv_D, Mathf.Clamp01(i / (ARC_N - 1.0f)));
        }

    }

}
