﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropController : MonoBehaviour
{
    public static DropController runningDropPhase;
    public float progress = 0;
    [Range(0, 1)]
    public float tolerance=1;
    public Animator swingAnimator;
    public Animator dropAnimator;
    public Transform dropContent;

    public Transform axisTransform;

    Transform targetPosition;
    public SpriteRenderer indicator;

    private void Awake()
    {
        dropAnimator.enabled = false;
        swingAnimator.enabled = false;

        targetPosition = new GameObject("target").transform;

        targetPosition.position = dropContent.position;
        targetPosition.SetParent(dropContent.parent.transform);


        dropContent.parent = swingAnimator.transform;
        dropContent.localPosition = Vector3.zero;
        dropContent.localRotation = Quaternion.identity;

        dropContent.gameObject.SetActive(false);
        indicator.gameObject.SetActive(false);

    }
    Color tempCol;
    private void Update()
    {
        if (RotationSpeedManager.instance)
        {
            this.transform.rotation = RotationSpeedManager.instance.rotationTarget.rotation;
        }
        if (indicator.gameObject.activeInHierarchy)
        {

            tempCol = indicator.color;
            float x = targetPosition.position.x - dropContent.position.x;
            float z = targetPosition.position.z - dropContent.position.z;
            tempCol.a = 1 - (Mathf.Sqrt(x * x + z * z)*2) ;
            indicator.color = tempCol;
        }
    }
    public void OnPhaseStart()
    {
        this.transform.position = new Vector3(axisTransform.position.x, this.transform.position.y, axisTransform.position.z);
        indicator.transform.position = new Vector3(axisTransform.position.x,indicator.transform.position.y,axisTransform.position.z);

        runningDropPhase = this;
        dropContent.gameObject.SetActive(true);
        Debug.Log("A");
        dropAnimator.enabled = true;
        RotationSpeedManager.instance.BreakRotation(0.5f, false);
    }
    public void OnPhaseEnd()
    {
        runningDropPhase = null;
        RotationSpeedManager.instance.BreakRotation(0.5f, true);
    }
    public void OnDropPositioningComplete()
    {
        swingAnimator.enabled = true;
        dropWindow = true;
        indicator.gameObject.SetActive(true);
    }
    float error;

    bool dropWindow = false;
    public void OnTap()
    {
        if (!dropWindow) return;
        dropWindow = false;
        float x = targetPosition.position.x - dropContent.position.x;
        float z = targetPosition.position.z - dropContent.position.z;
        error = Mathf.Sqrt(x * x + z * z);
        Debug.Log( error);
        if (error < tolerance)
        {

            dropAnimator.SetTrigger("drop");
            swingAnimator.enabled = false;
        }
        else
        {
            x = swingAnimator.transform.localPosition.x;
            if (x > 0)
            {
                dropAnimator.SetTrigger("dropRight");
                swingAnimator.enabled = false;

            }
            else
            {
                dropAnimator.SetTrigger("dropLeft");
                swingAnimator.enabled = false;
            }
        }
    }

    public void OnDropSuccess()
    {
        indicator.gameObject.SetActive(false);

        dropContent.SetParent(targetPosition.parent);
        progress = 1;
    }
    public void OnDropFail()
    {
        indicator.gameObject.SetActive(false);
        dropAnimator.SetTrigger("reset");

    }
}
