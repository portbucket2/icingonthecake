﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MorfMesh : MonoBehaviour
{
    //public bool reverted;
    //public bool isAutoComplete;
    public float localRadMult = 1;
    public List<MorfStep> morfSteps = new List<MorfStep>();
    internal MorfStep[] morfStepArray;


    //private const float RAY_RAD_MULT = 4.5f;
    internal MeshFilter meshFilter;
    MeshRenderer mRenderer;


    [Header("PostBuildReadOnlyData")]
    public bool verticesLoaded = false;
    public float vCount = 0;
    TriList<VertexGroup> vertexGroups;
    [HideInInspector] public List<VertexGroup> vertexGroupList;
    [HideInInspector] public bool isPrimaryMesh = false;


    //Vector3 worldUpLocally;
    [HideInInspector] public Mesh mesh;
    [HideInInspector] public Vector3[] vertices;
    [HideInInspector] public Vector3[] normals;
    [HideInInspector] public Color[] vcolors;
    [HideInInspector] public Color[] vcolors2;
    [HideInInspector] public Vector2[] uvs;

    PhaseOperation opcon;
    OperationSettings settings;
    float groupCheckDistance;
    float r2Min;//secondaryFadeRadius
    float r2Max;//secondaryPaintRadius
    float r1Max;//primaryOuterRadius
    float r1Min;//falloffStartRadius

    //Vector3[] vertexDeforms;
    internal float[][] vxMovementCompletion;
    internal float[][] vxMovementAutoTarget;
    Vector3[][] stateVerticesGrid;
    Vector3[][] stateNormalsGrid;



    public void PrintVCount()
    {
        int c = 0;
        foreach (VertexGroup item in vertexGroupList)
        {
            c += item.vertexIndices.Length;
        }

        //Debug.LogFormat("{0} -vcount {1}",gameObject.name, c);
    }
    public int RegisterVertices(float divs)
    {
        int maxX = 0;
        int maxY = 0;
        int maxZ = 0;
        int minX = 0;
        int minY = 0;
        int minZ = 0;
        if (!meshFilter) meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = meshFilter.sharedMesh;
        Vector3[] vx = mesh.vertices;
        for (int i = 0; i < vx.Length; i++)
        {
            int X = Mathf.RoundToInt(vx[i].x / divs);
            int Y = Mathf.RoundToInt(vx[i].y / divs);
            int Z = Mathf.RoundToInt(vx[i].z / divs);

            if (X > maxX) maxX = X;
            if (Y > maxY) maxY = Y;
            if (Z > maxZ) maxZ = Z;

            if (X < minX) minX = X;
            if (Y < minY) minY = Y;
            if (Z < minZ) minZ = Z;
        }

        vertexGroups = new TriList<VertexGroup>(minX, maxX, minY, maxY, minZ, maxZ);

        for (int i = minX; i <= maxX; i++)
        {
            for (int j = minY; j <= maxY; j++)
            {
                for (int k = minZ; k <= maxZ; k++)
                {
                    vertexGroups[i, j, k] = new VertexGroup(i, j, k, divs);
                }
            }
        }

        for (int i = 0; i < vx.Length; i++)
        {
            int X = Mathf.RoundToInt(vx[i].x / divs);
            int Y = Mathf.RoundToInt(vx[i].y / divs);
            int Z = Mathf.RoundToInt(vx[i].z / divs);
            vertexGroups[X, Y, Z].Add(i);
        }

        vertexGroupList = new List<VertexGroup>();
        for (int i = 0; i < vertexGroups.length; i++)
        {
            vertexGroups[i].Consolidate();
            if (vertexGroups[i].vertexIndices.Length > 0)
            {

                vertexGroupList.Add(vertexGroups[i]);
            }

        }
        Debug.LogFormat("List Size = {0}", vertexGroupList.Count);
        verticesLoaded = true;
        vCount = vx.Length;
        return vx.Length;
    }


    public void ResetLoadedVcols(Color[] vcols)
    {
        meshFilter.mesh.colors = vcols;
        vcolors = vcols;
    }
    public void CheckSecondaryCols()
    {
        float tot;
        for (int i = 0; i < vcolors.Length; i++)
        {
            tot = vcolors[i].r + vcolors[i].g + vcolors[i].b + vcolors[i].a;
            if (tot < 1)
            {
                vcolors[i] = Color.Lerp(vcolors2[i], vcolors[i], Mathf.Pow(tot,10));
            }
        }

        meshFilter.mesh.colors = vcolors;
    }

    private UnityEngine.Profiling.CustomSampler samplerA = UnityEngine.Profiling.CustomSampler.Create("samplerA");
    private UnityEngine.Profiling.CustomSampler samplerB = UnityEngine.Profiling.CustomSampler.Create("samplerB");

    public void OnPhaseStart(PhaseOperation opcon, GamePhase phase)
    {

        if (!meshFilter) meshFilter = GetComponent<MeshFilter>();

        if (meshFilter && (meshFilter.mesh.colors == null || meshFilter.mesh.colors.Length == 0))
        {
            Color[] colors = new Color[meshFilter.mesh.vertices.Length];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = new Color(0, 0, 0, 0);
            }
            meshFilter.mesh.colors = colors;
        }
        if (mRenderer == null)
        {
            mRenderer = meshFilter.GetComponent<MeshRenderer>();
        }

        this.opcon = opcon;
        this.settings = opcon.settings;
        this.groupCheckDistance = opcon.groupCheckDistance;
        this.r1Min = opcon.r_min;
        this.r1Max = opcon.r_max;
        this.r2Max = this.r1Max * 4f;
        this.r2Min = this.r1Max + (r2Max-r1Max)* settings.secondaryInnerRad* settings.secondaryInnerRad;
        mesh = meshFilter.mesh;
        vertices = mesh.vertices;
        vcolors = mesh.colors;
        if(phase == GamePhase.AddCream)vcolors2 = mesh.colors;
        normals = mesh.normals;
        uvs = mesh.uv;

        foreach (var item in morfSteps)
        {
             item.Init(meshFilter);
        }

        MorfStep mstep;
        int tempVCount;
        float morfRatio;
        if (stateVerticesGrid == null)
        {
            morfStepArray = morfSteps.ToArray();
            stateVerticesGrid = new Vector3[morfStepArray.Length + 1][];
            stateNormalsGrid = new Vector3[morfStepArray.Length + 1][];

            for (int i = 0; i < morfStepArray.Length + 1; i++)
            {
                Vector3[] varray = null;
                Vector3[] narray = null;
                if (i == 0)
                {
                    varray = mesh.vertices;
                    narray = mesh.normals;
                }
                else
                {
                    mstep = morfStepArray[i - 1];
                    //tempVCount = mstep.meshFilter.mesh.vertices.Length;
                    //morfRatio = mstep.secondaryWeight;
                    //Debug.Log(tempVCount);
                    varray = mstep.varray;
                    narray = mstep.narray;
                //    for (int msvi = 0; msvi < 5000; msvi++)
                //    {
                //        varray[msvi] = mstep.meshFilter.mesh.vertices[msvi] * (1 - morfRatio) + mstep.secondaryFilter.mesh.vertices[msvi] * morfRatio;
                //        narray[msvi] = mstep.meshFilter.mesh.normals[msvi] * (1 - morfRatio) + mstep.secondaryFilter.mesh.normals[msvi] * morfRatio;
                //    }
                }
                stateVerticesGrid[i] = varray;
                stateNormalsGrid[i] = narray;
            }

        }

        vxMovementCompletion = new float[morfStepArray.Length][];
        vxMovementAutoTarget = new float[morfStepArray.Length][];

        for (int i = 0; i < morfStepArray.Length; i++)
        {
            vxMovementCompletion[i] = new float[vertices.Length];
            vxMovementAutoTarget[i] = new float[vertices.Length];
        }




        switch (phase)
        {
            case GamePhase.AddCream:
                {
                    mRenderer.enabled = false;
                }
                break;
            case GamePhase.Flatten:
                {
                    mRenderer.enabled = true;
                }
                break;
            case GamePhase.Decorate:
                {
                    mRenderer.enabled = true;
                }
                break;
        }
    }

    public void VertexPaintOperation_Sphere(Vector3 rchPoint, Vector3 travelVec, Vector3 travelOrigin)
    {
        int progressIndex = PhaseManager.currentPhaseIndex;

        VertexGroup vg;
        int[] vindices;
        int v;
        float d = 0;
        float tempSqrDist = 0;
        float colTargetVal;
        Vector3 tempVec;
        Color oldColSamp= new Color();
        float oldColTotVal;
        //Debug.Log(vertices.Length);

        float reductionMult;
        float reductionMultCoeff =3.5f;
        float deltaT = Time.deltaTime;
        for (int i = 0; i < vertexGroupList.Count; i++)
        {
            vg = vertexGroupList[i];
            if ((vg.groupPostion - rchPoint).sqrMagnitude < groupCheckDistance)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    tempVec.x = vertices[v].x - rchPoint.x;
                    tempVec.y = vertices[v].y - rchPoint.y;
                    tempVec.z = vertices[v].z - rchPoint.z;
                    d = tempVec.x * tempVec.x + tempVec.y * tempVec.y + tempVec.z * tempVec.z;
                    if (d < r2Max)
                    {

                        oldColSamp = vcolors2[v];
                        if (d > r2Min)
                        {
                            colTargetVal = (r2Max - d) / (r2Max - r2Min);// has to be 0 to 1

                        }
                        else
                        {
                            colTargetVal = 1;
                        }
                        switch (IcePicker.currentChannel)
                        {
                            case ColChannel.R:
                                if (colTargetVal > oldColSamp.r) oldColSamp.r = colTargetVal;
                                if (!MainGameManager.settings.editorPaintMode)
                                {
                                    oldColSamp.g = oldColSamp.g * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.b = oldColSamp.b * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.a = oldColSamp.a * (1 - colTargetVal * colTargetVal);
                                }
                                break;
                            case ColChannel.G:
                                if (colTargetVal > oldColSamp.g) oldColSamp.g = colTargetVal;
                                if (!MainGameManager.settings.editorPaintMode)
                                {
                                    oldColSamp.r = oldColSamp.r * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.b = oldColSamp.b * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.a = oldColSamp.a * (1 - colTargetVal * colTargetVal);
                                }
                                break;
                            case ColChannel.B:
                                if (colTargetVal > oldColSamp.b) oldColSamp.b = colTargetVal;
                                if (!MainGameManager.settings.editorPaintMode)
                                {
                                    oldColSamp.r = oldColSamp.r * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.g = oldColSamp.g * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.a = oldColSamp.a * (1 - colTargetVal * colTargetVal);
                                }
                                break;
                            case ColChannel.A:
                                if (colTargetVal > oldColSamp.a) oldColSamp.a = colTargetVal;
                                if (!MainGameManager.settings.editorPaintMode)
                                {
                                    oldColSamp.r = oldColSamp.r * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.g = oldColSamp.g * (1 - colTargetVal * colTargetVal);
                                    oldColSamp.b = oldColSamp.b * (1 - colTargetVal * colTargetVal);
                                }
                                break;
                        }
                        vcolors2[v] = oldColSamp;

                        if (d < r1Max)
                        {
                            oldColSamp = vcolors[v];
                            oldColTotVal = oldColSamp.r + oldColSamp.g + oldColSamp.b + oldColSamp.a;
                            if (d > r1Min)
                            {
                                colTargetVal = (r1Max - d) / (r1Max - r1Min);// has to be 0 to 1

                            }
                            else
                            {
                                colTargetVal = 1;
                            }
                            switch (IcePicker.currentChannel)
                            {
                                case ColChannel.R:
                                    if (colTargetVal > oldColSamp.r) oldColSamp.r = colTargetVal;
                                    break;
                                case ColChannel.G:
                                    if (colTargetVal > oldColSamp.g) oldColSamp.g = colTargetVal;
                                    break;
                                case ColChannel.B:
                                    if (colTargetVal > oldColSamp.b) oldColSamp.b = colTargetVal;
                                    break;
                                case ColChannel.A:
                                    if (colTargetVal > oldColSamp.a) oldColSamp.a = colTargetVal;
                                    break;
                            }
                            reductionMult = (1 - (colTargetVal * colTargetVal*deltaT*reductionMultCoeff));
                            switch (IcePicker.currentChannel)
                            {
                                case ColChannel.R:
                                    oldColSamp.g = oldColSamp.g * reductionMult;
                                    oldColSamp.b = oldColSamp.b * reductionMult;
                                    oldColSamp.a = oldColSamp.a * reductionMult;
                                    break;
                                case ColChannel.G:
                                    oldColSamp.r = oldColSamp.r * reductionMult;
                                    oldColSamp.b = oldColSamp.b * reductionMult;
                                    oldColSamp.a = oldColSamp.a * reductionMult;
                                    break;
                                case ColChannel.B:
                                    oldColSamp.r = oldColSamp.r * reductionMult;
                                    oldColSamp.g = oldColSamp.g * reductionMult;
                                    oldColSamp.a = oldColSamp.a * reductionMult;
                                    break;
                                case ColChannel.A:
                                    oldColSamp.r = oldColSamp.r * reductionMult;
                                    oldColSamp.g = oldColSamp.g * reductionMult;
                                    oldColSamp.b = oldColSamp.b * reductionMult;
                                    break;
                            }
                            vcolors[v] = oldColSamp;



                            if (isPrimaryMesh)
                            {
                                if (oldColTotVal <= 0.001f && (oldColSamp.r + oldColSamp.g + oldColSamp.b + oldColSamp.a) > 0.001f)
                                {
                                    opcon.progress += 1;
                                }
                                tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                                if (tempSqrDist > opcon.farthestSqrDist)
                                {
                                    opcon.uvUp = uvs[v];
                                    opcon.farthestSqrDist = tempSqrDist;
                                }
                                if (tempSqrDist < opcon.closestSqrDist)
                                {
                                    opcon.uvDown = uvs[v];
                                    opcon.closestSqrDist = tempSqrDist;
                                }
                            }
                        }


                    }
                }
            }

        }

        mesh.colors = vcolors;
    }


    public void VertexPaintOperation_Tunnel(Ray ray, float maxHalfHeight, Vector3 travelVec, Vector3 travelOrigin)
    {
        int progressIndex = PhaseManager.currentPhaseIndex;

        VertexGroup vg;
        int[] vindices;
        int v;
        //float d;
        float tempSqrDist;
        float colTargetVal;
        Color oldColSamp;
        float oldColTotVal;
        //Debug.Log(vertices.Length);

        Vector3 TV;//tempvec
        Vector3 A;
        Vector3 B;
        float x, y, z, lv;
        float crossSqrMag, raySqrDist, rayAlignedDistance;
        Vector3 rayOrigin = this.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = this.transform.InverseTransformDirection(ray.direction).normalized;

        for (int i = 0; i < vertexGroupList.Count; i++)
        {
            vg = vertexGroupList[i];
            TV = (vg.groupPostion - rayOrigin);
            crossSqrMag = Vector3.Cross(rayDir, TV).sqrMagnitude;

            if (crossSqrMag < groupCheckDistance)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    TV.x = vertices[v].x - rayOrigin.x;
                    TV.y = vertices[v].y - rayOrigin.y;
                    TV.z = vertices[v].z - rayOrigin.z;
                    A = rayDir;
                    B = TV;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    raySqrDist = x * x + y * y + z * z;
                    rayAlignedDistance = B.x * A.x + B.y * A.y + B.z * A.z;
                    if (rayAlignedDistance < 0) rayAlignedDistance = -rayAlignedDistance;
                    if ((raySqrDist < r1Max) && (rayAlignedDistance < maxHalfHeight))
                    {
                        //total++;
                        oldColSamp = vcolors[v];
                        oldColTotVal = oldColSamp.r + oldColSamp.g + oldColSamp.b + oldColSamp.a;
                        //dirty++;
                        if (raySqrDist > r1Min)
                        {
                            colTargetVal = (r1Max - raySqrDist) / (r1Max - r1Min);

                        }
                        else
                        {
                            colTargetVal = 1;
                        }
                        //if (ColChannel.A == IcePicker.currentChannel) Debug.LogError("!");
                        switch (IcePicker.currentChannel)
                        {
                            case ColChannel.R:
                                if (colTargetVal > oldColSamp.r) oldColSamp.r = colTargetVal;
                                break;
                            case ColChannel.G:
                                if (colTargetVal > oldColSamp.g) oldColSamp.g = colTargetVal;
                                break;
                            case ColChannel.B:
                                if (colTargetVal > oldColSamp.b) oldColSamp.b = colTargetVal;
                                break;
                            case ColChannel.A:
                                if (colTargetVal > oldColSamp.a) oldColSamp.a = colTargetVal;
                                break;
                        }
                        vcolors[v] = oldColSamp;


                        if (isPrimaryMesh)
                        {
                            if (oldColTotVal <= 0.001f && (oldColSamp.r + oldColSamp.g + oldColSamp.b + oldColSamp.a) > 0.001f)
                            {
                                opcon.progress += 1;
                            }
                            tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                            //distCumulitive += tempSqrDist;
                            if (tempSqrDist > opcon.farthestSqrDist)
                            {
                                opcon.uvUp = uvs[v];
                                opcon.farthestSqrDist = tempSqrDist;
                            }
                            if (tempSqrDist < opcon.closestSqrDist)
                            {
                                opcon.uvDown = uvs[v];
                                opcon.closestSqrDist = tempSqrDist;
                            }
                        }
                    }

                }
            }

        }

        mesh.colors = vcolors;
    }
    public Vector2 VertexShiftOperation_Mixed(RaycastHit hit, Ray ray, float maxHalfHeight)
    {
        //Debug.Log("should draw");
        //Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 1);
        float angle = Vector3.Angle(hit.normal, Vector3.up);
        bool[] morfStepActive = new bool[morfStepArray.Length];
        for (int i = 0; i < morfStepArray.Length; i++)
        {
            morfStepActive[i] = !(angle < morfStepArray[i].minMaxAngle.x || angle > morfStepArray[i].minMaxAngle.y);
        }

        int progressIndex = PhaseManager.currentPhaseIndex;
        VertexGroup vg;
        int[] vindices;
        int v;


        Vector3 TV;//tempvec
        float crossSqrMag, raySqrDist, rayAlignedDistance;
        Vector3 A;
        Vector3 B;
        float x, y, z, lv;
        Vector3 rayOrigin = this.transform.InverseTransformPoint(ray.origin);
        Vector3 rayDir = this.transform.InverseTransformDirection(ray.direction).normalized;
        MorfStep mStep;
        float progress = 0;
        float maxPossibleProgress = 0;
        float deltaTime = Time.deltaTime;
        float radMult = settings.defaultRadMult * localRadMult;

        bool vdirty;
        Vector3 vertexEndPoint;
        Vector3 normalEndPoint;
        // float tempVal;
        samplerA.Begin();
        for (int i = 0; i < vertexGroupList.Count; i++)
        {
            vg = vertexGroupList[i];
            TV = (vg.groupPostion - rayOrigin);
            crossSqrMag = Vector3.Cross(rayDir, TV).sqrMagnitude;
            if (crossSqrMag < groupCheckDistance * radMult)
            {
                vindices = vg.vertexIndices;
                vdirty = false;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    TV.x = vertices[v].x - rayOrigin.x;
                    TV.y = vertices[v].y - rayOrigin.y;
                    TV.z = vertices[v].z - rayOrigin.z;
                    A = rayDir;
                    B = TV;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    raySqrDist = x * x + y * y + z * z;
                    rayAlignedDistance = B.x * A.x + B.y * A.y + B.z * A.z;
                    if (rayAlignedDistance < 0) rayAlignedDistance = -rayAlignedDistance;
                    if ((raySqrDist < r1Max * radMult) && (rayAlignedDistance < maxHalfHeight))
                    {
                        for (int mi = 0; mi < morfStepActive.Length; mi++)
                        {
                            float radialFrac = (1 - (raySqrDist / (r1Max * radMult)));
                            mStep = morfStepArray[mi];

                            bool lastStepOk = true;

                            float lastStepValue;
                            float currentStepValue;

                            if (mi > 0)
                            {

                                    lastStepValue = vxMovementCompletion[mi - 1][v];
                                    currentStepValue = vxMovementCompletion[mi][v];
                                    lastStepOk = lastStepValue >= mStep.previousCompletionRequirement; 
                            }

                            if (morfStepActive[mi] && vxMovementCompletion[mi][v] < 1 && lastStepOk)
                            {
                                lv = vxMovementCompletion[mi][v];
                                lv = lv + (deltaTime / mStep.completionTime);
                                if (lv > 1) lv = 1;
                                vxMovementCompletion[mi][v] = lv;
                                vdirty=true;
                            }

                        }

                        if (vdirty)
                        {
                            //if (!reverted)
                            //{
                            //    vertexEndPoint = stateVerticesGrid[morfStepArray.Length][v];
                            //    normalEndPoint = stateNormalsGrid[morfStepArray.Length][v];
                            //    for (int mi = morfStepArray.Length - 1; mi >= 0; mi--)
                            //    {
                            //        lv = vxMovementCompletion[mi][v];
                            //        vertexEndPoint.x = stateVerticesGrid[mi][v].x * (1 - lv) + vertexEndPoint.x * lv;
                            //        vertexEndPoint.y = stateVerticesGrid[mi][v].y * (1 - lv) + vertexEndPoint.y * lv;
                            //        vertexEndPoint.z = stateVerticesGrid[mi][v].z * (1 - lv) + vertexEndPoint.z * lv;

                            //        normalEndPoint.x = stateNormalsGrid[mi][v].x * (1 - lv) + normalEndPoint.x * lv;
                            //        normalEndPoint.y = stateNormalsGrid[mi][v].y * (1 - lv) + normalEndPoint.y * lv;
                            //        normalEndPoint.z = stateNormalsGrid[mi][v].z * (1 - lv) + normalEndPoint.z * lv;
                            //    }
                            //}
                            //else
                            //{
                            vertexEndPoint = stateVerticesGrid[0][v];
                            normalEndPoint = stateNormalsGrid[0][v];
                            for (int mi = 0; mi < morfStepArray.Length; mi++)
                            {
                                lv = vxMovementCompletion[mi][v];
                                vertexEndPoint.x = vertexEndPoint.x * (1 - lv) + stateVerticesGrid[mi + 1][v].x * (lv);
                                vertexEndPoint.y = vertexEndPoint.y * (1 - lv) + stateVerticesGrid[mi + 1][v].y * (lv);
                                vertexEndPoint.z = vertexEndPoint.z * (1 - lv) + stateVerticesGrid[mi + 1][v].z * (lv);

                                normalEndPoint.x = normalEndPoint.x * (1 - lv) + stateNormalsGrid[mi + 1][v].x * (lv);
                                normalEndPoint.y = normalEndPoint.y * (1 - lv) + stateNormalsGrid[mi + 1][v].y * (lv);
                                normalEndPoint.z = normalEndPoint.z * (1 - lv) + stateNormalsGrid[mi + 1][v].z * (lv);
                            }
                            //}

                            vertices[v] = vertexEndPoint;
                            normals[v] = normalEndPoint;
                        }
                    }
                }


            }
        }
        samplerA.End();
        float pr;
        float mpr;
        samplerB.Begin();
        for (int i = 0; i < vertices.Length; i++)
        {
            pr = 0;
            mpr = 0;
            for (int os = 0; os < morfStepActive.Length; os++)
            {
                pr += vxMovementCompletion[os][i];
                mpr += 1;

                //maxPossibleProgress += 1/ morfStepActive.Length;
                //progress += vxMovementCompletion[os][i]/ morfStepActive.Length;
            }
            pr /= morfStepActive.Length;
            mpr /= morfStepActive.Length;
            progress += 1-((1-pr)*(1-pr)*(1-pr));// Mathf.Pow(pr,0.32f);
            maxPossibleProgress += mpr ;
           
        }
        samplerB.End();

        mesh.vertices = vertices;
        mesh.normals = normals;

        return new Vector2(progress, maxPossibleProgress)* (isPrimaryMesh?1:0);
    }
}

[System.Serializable]
public class MorfStep
{
    public bool log = false;
    public MeshFilter meshFilter;
    [Range(0, 1)]
    public float secondaryWeight = 0f;
    public MeshFilter secondaryFilter;

    internal Vector3[] varray;
    internal Vector3[] narray;
    public void Init(MeshFilter optionalSecondary)
    {
        if (log) Debug.Log(meshFilter.mesh.vertices.Length);
        if (log) Debug.Log(secondaryFilter.mesh.vertices.Length);
        if (log) Debug.Log(optionalSecondary.mesh.vertices.Length);
        if (secondaryFilter == null) secondaryFilter = optionalSecondary;
        Vector3[] vA = meshFilter.mesh.vertices;
        Vector3[] vB = secondaryFilter.mesh.vertices;
        Vector3[] nA = meshFilter.mesh.normals;
        Vector3[] nB = secondaryFilter.mesh.normals;


        int N = vA.Length;
        varray = new Vector3[N];
        narray = new Vector3[N];
        for (int i = 0; i < varray.Length; i++)
        {
            varray[i].x = vA[i].x * (1 - secondaryWeight) + vB[i].x * secondaryWeight;
            varray[i].y = vA[i].y * (1 - secondaryWeight) + vB[i].y * secondaryWeight;
            varray[i].z = vA[i].z * (1 - secondaryWeight) + vB[i].z * secondaryWeight;

            narray[i].x = nA[i].x * (1 - secondaryWeight) + nB[i].x * secondaryWeight;
            narray[i].y = nA[i].y * (1 - secondaryWeight) + nB[i].y * secondaryWeight;
            narray[i].z = nA[i].z * (1 - secondaryWeight) + nB[i].z * secondaryWeight;
        }
    }

    public float completionTime = 1f;
    public float previousCompletionRequirement = 1f;
    //public bool autoCompleteMode = false;
    public Vector2 minMaxAngle;
    //public float colorAdoptionRate = 0;
    public MorfStep()
    {
        minMaxAngle = new Vector2(0, 180);
    }
}
