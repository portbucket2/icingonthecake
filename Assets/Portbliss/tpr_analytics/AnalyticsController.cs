using com.adjust.sdk;
using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Analytics
{
    public enum UpgradeType { Ghost, Vampire, Zombie, UserTap};
    public class AnalyticsController : MonoBehaviour
    {
        static AnalyticsController instance;
        static bool fbInitDone;
        const string iOS_AppToken = "fkj5de2klvcw";
        const string android_AppToken = "345cj8h1np1c";
        [SerializeField] AdjustEnvironment environment = AdjustEnvironment.Sandbox;
        [SerializeField] AdjustLogLevel logLevel = AdjustLogLevel.Verbose;

        void Awake()
        {
            
            if (instance == null)
            {
                instance = this;
                InitAnalytics();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (instance.gameObject != gameObject)
                {
                    DestroyImmediate(this);
                    return;
                }
               
            }
        }

        void InitAnalytics()
        {
#if UNITY_IOS
            /* Mandatory - set your iOS app token here */
            var adjustAppToken = iOS_AppToken;
#elif UNITY_ANDROID
                /* Mandatory - set your Android app token here */
                var adjustAppToken = android_AppToken;
#endif
            var config = new AdjustConfig(adjustAppToken, environment, true);
            config.setLogLevel(logLevel); // AdjustLogLevel.Suppress to disable logs
            config.setSendInBackground(true);
            new GameObject("Adjust").AddComponent<Adjust>(); // do not remove or rename

            // Adjust.addSessionCallbackParameter("foo", "bar"); // if requested to set session - level parameters
            Adjust.start(config);


            fbInitDone = false;
            //Debug.Log("<color='blue'>facebook initialization method!</color>");
            if (!FB.IsInitialized)
            {
                //Debug.Log("<color='blue'>will try to initialize facebook!</color>");
                // Initialize the Facebook SDK
                FB.Init(() => {
                    //Debug.Log("<color='blue'>facebook initialized!</color>");
                    fbInitDone = true;
                    FB.ActivateApp();
                });
            }
            else
            {
                //Debug.Log("<color='blue'>already facebook initialized!</color>");
                // Already initialized, signal an app activation App Event
                fbInitDone = true;
                FB.ActivateApp();
            }
        }

        public static void LogSpeedChanged()
        {
            if (FB.IsInitialized)
            {
                FB.LogAppEvent("Speed Changed");
                //Debug.Log("<color='blue'>LogSpeedChanged()</color>");
            }
            else
            {
                Debug.Log("<color='blue'>LogSpeedChanged() before</color>");
                WaitForInit(() =>
                {
                    FB.LogAppEvent("Speed Changed");
                    //Debug.Log("<color='blue'>LogSpeedChanged() after</color>");
                });
            }
        }
        
        

        public static void LogToolSwitchedTo(int levelNum, string tool="Spatula")
        {
            if (FB.IsInitialized)
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["Tool"] = tool;
                FB.LogAppEvent("Tool Switched To", 0, param);
                //Debug.Log("<color='blue'>LogToolSwitchedTo()</color>");
            }
            else
            {
                Debug.Log("<color='blue'>LogToolSwitchedTo() before</color>");
                WaitForInit(() =>
                {
                    var param = new Dictionary<string, object>();
                    param[AppEventParameterName.Level] = levelNum;
                    param["Tool"] = tool;
                    FB.LogAppEvent("Tool Switched To", 0, param);
                    //Debug.Log("<color='blue'>LogToolSwitchedTo() after</color>");
                });
            }
        }

        public static void LogLevelStarted(int levelNum, string gameplay, int layerCount)
        {
            if (FB.IsInitialized)
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["Type"] = gameplay;
                param["Height"] = layerCount;
                FB.LogAppEvent("Level Started", 0, param);
                //Debug.Log("<color='blue'>LogLevelStarted()</color>");
            }
            else
            {
                Debug.Log("<color='blue'>LogLevelStarted() before</color>");
                WaitForInit(() =>
                {
                    var param = new Dictionary<string, object>();
                    param[AppEventParameterName.Level] = levelNum;
                    param["Type"] = gameplay;
                    param["Height"] = layerCount;
                    FB.LogAppEvent("Level Started", 0, param);
                    //Debug.Log("<color='blue'>LogLevelStarted() after</color>");
                });
            }
        }

        public static void LogLevelReStarted(int levelNum, string gameplay, int layerCount)
        {
            if (FB.IsInitialized)
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["Type"] = gameplay;
                param["Height"] = layerCount;
                FB.LogAppEvent("Level Reset", 0, param);
                //Debug.Log("<color='blue'>LogLevelReStarted()</color>");
            }
            else
            {
                Debug.Log("<color='blue'>LogLevelReStarted() before</color>");
                WaitForInit(() =>
                {
                    var param = new Dictionary<string, object>();
                    param[AppEventParameterName.Level] = levelNum;
                    param["Type"] = gameplay;
                    param["Height"] = layerCount;
                    FB.LogAppEvent("Level Reset", 0, param);
                    //Debug.Log("<color='blue'>LogLevelReStarted() after</color>");
                });
            }
        }

        public static void LogLevelCompleted(int levelNum, int stars, string gameplay, int layerCount)
        {
            if (FB.IsInitialized)
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                param["Type"] = gameplay;
                param["Height"] = layerCount;
                param["Stars"] = stars + " " + "Stars";
                FB.LogAppEvent(AppEventName.AchievedLevel, 0, param);
                //Debug.Log("<color='blue'>LogLevelCompleted()</color>");
            }
            else
            {
                //Debug.Log("<color='blue'>LogLevelCompleted() before</color>");
                WaitForInit(() =>
                {
                    var param = new Dictionary<string, object>();
                    param[AppEventParameterName.Level] = levelNum;
                    param["Type"] = gameplay;
                    param["Height"] = layerCount;
                    param["Stars"] = stars + " " + "Stars";
                    FB.LogAppEvent(AppEventName.AchievedLevel, 0, param);
                    //Debug.Log("<color='blue'>LogLevelCompleted() after</color>");
                });
            }
        }

        public static void LogLevelSkipped(int levelNum)
        {
            if (FB.IsInitialized)
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = levelNum;
                FB.LogAppEvent("Level Skipped", 0, param);
                //Debug.Log("<color='blue'>LogLevelSkipped()</color>");
            }
            else
            {
                //Debug.Log("<color='blue'>LogLevelSkipped() before</color>");
                WaitForInit(() =>
                {
                    var param = new Dictionary<string, object>();
                    param[AppEventParameterName.Level] = levelNum;
                    FB.LogAppEvent("Level Skipped", 0, param);
                    //Debug.Log("<color='blue'>LogLevelSkipped() after</color>");
                });
            }
        }

        public static void LogRewardedVideoAdStart(int levelnumber)
        {
            if (fbInitDone == false)
            {
                Debug.Log("<color=magenta>can not finally call 'LogRewardedVideoAdStart' because FB has not been initialized.</color>");
                return;
            }
            var Params = new Dictionary<string, object>();
            Params[AppEventParameterName.Level] = "" + levelnumber;
            Debug.Log("<color=magenta>finally 'LogRewardedVideoAdStart' call</color>");
            FB.LogAppEvent("Rewarded Video Start", null, Params);
        }

        public static void LogRewardedVideoAdComplete(int levelnumber)
        {
            if (fbInitDone == false)
            {
                Debug.Log("<color=magenta>can not finally call 'LogRewardedVideoAdComplete' because FB has not been initialized.</color>");
                return;
            }
            var Params = new Dictionary<string, object>();
            Params[AppEventParameterName.Level] = "" + levelnumber;
            Debug.Log("<color=magenta>finally 'LogRewardedVideoAdComplete' call</color>");
            FB.LogAppEvent("Rewarded Video Complete", null, Params);
        }

        public static void LogObjectSelected(string objectName)
        {
            if (FB.IsInitialized)
            {
                var param = new Dictionary<string, object>();
                param[AppEventParameterName.Level] = objectName;
                FB.LogAppEvent("Object Selected", 0, param);
                //Debug.Log("<color='blue'>LogObjectSelected() after</color>");
            }
            else
            {
                Debug.Log("<color='blue'>LogObjectSelected() before</color>");
                WaitForInit(() =>
                {
                    var param = new Dictionary<string, object>();
                    param[AppEventParameterName.Level] = objectName;
                    FB.LogAppEvent("Object Selected", 0, param);
                    //Debug.Log("<color='blue'>LogObjectSelected() after</color>");
                });
            }
        }

        public static void LogABTesting(string abType, string abValue)
        {
            //Debug.Log("<color='blue'>AB value choice made type:" + abType + " and ab value: " + abValue + " in fb manager-before wait method</color>");
            WaitForInit(() => {
                var Params = new Dictionary<string, object>();
                Params[AppEventParameterName.Level] = "" + abValue;

                FB.LogAppEvent(abType, null, Params);
                //Debug.Log("<color='blue'>AB value choice made type:" + abType + " and ab value: " + abValue + " in fb manager-after wait method</color>");
            });
        }

        static void WaitForInit(Action OnComplete)
        {
            instance.StartCoroutine(instance.WaitForInitCOR(OnComplete));
        }

        IEnumerator WaitForInitCOR(Action OnComplete)
        {
            yield return null;
            while (FB.IsInitialized == false)
            {
                yield return null;
            }
            OnComplete?.Invoke();
        }
    }
}
