﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolBehaviourController : MonoBehaviour
{
    //public static KnifeOutOfAreaMode OUT_OF_AREA_MODE = KnifeOutOfAreaMode.Follow;

    public Camera cam;
    public Animator hoverAnim;
    public MeshRenderer IceBagColorRenderer;
    public SmoothToolVisualController spatulaObject;
    public GameObject creamerObject;
    //public GameObject rendererRef
    //{
    //    get
    //    {
    //        switch (ModeController.mode)
    //        {
    //            case PeelerType.PEELER:
    //                return peelerRenderer;
    //            case PeelerType.KNIFE:
    //                return knifeRenderer;
    //            default:
    //                return null;
    //        }
    //    }
    //}

    public Transform targetTrans;
    public Transform visualTrans;
    public float perFrameLerpRate_position= 5;
    public float perFrameLerpRate_rotation = 2;
    public float perFrameLerpRate_scale = 3;

    float radiusGrowth = 0.1f;
    int radiusGrowthIteration = 5;
    //float sphereRad = 0.1f;


    private void Awake()
    {
        targetBagColor = IceBagColorRenderer.material.GetColor("_Color");
        currentBagColor = targetBagColor;
        PhaseController.onPhaseStart_StaticEvent += OnPhaseStart;
    }
    private void OnDestroy()
    {
        PhaseController.onPhaseStart_StaticEvent -= OnPhaseStart;
    }
    void OnPhaseStart(PhaseController pc)
    {
        UpdateToolVisual(false);
    }
    void UpdateToolVisual(bool didHit)
    {
        hoverAnim.SetBool("hover",!didHit);
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                if (IcePicker.currentMat)
                {
                    targetBagColor = IcePicker.currentMat.GetColor("_Color");
                    targetBagColor.a = IceBagColorRenderer.material.GetColor("_Color").a;
                }
                creamerObject.SetActive(true);
                spatulaObject.SetVisibility(false);
                break;
            case GamePhase.Flatten:
                creamerObject.SetActive(false);
                spatulaObject.SetVisibility(true);
                break;
            case GamePhase.DropPatty:
            case GamePhase.Decorate:
                creamerObject.SetActive(false);
                spatulaObject.SetVisibility(false);
                break;
        }
    }


    Color targetBagColor;
    Color currentBagColor;
    //bool knifeEnabled;
    void Update()
    {
        if (PhaseManager.instance && PhaseManager.phase == GamePhase.AddCream)
        {
            currentBagColor = Color.Lerp(currentBagColor,targetBagColor,10*Time.deltaTime);
            IceBagColorRenderer.material.SetColor("_Color", currentBagColor);
        }

        visualTrans.position = Vector3.Lerp(visualTrans.position, targetTrans.position, perFrameLerpRate_position * Time.deltaTime);
        visualTrans.rotation = Quaternion.Lerp(visualTrans.rotation,targetTrans.rotation, perFrameLerpRate_rotation*Time.deltaTime);
        visualTrans.localScale = Vector3.Lerp(visualTrans.localScale, targetTrans.localScale, perFrameLerpRate_scale * Time.deltaTime);
    }

    public static bool successfullyInteracting = false;

    public void KnifeMoveUpdate(Vector2 mousePos,System.Action<float> onToolPrepare)
    {
        savedMousePos = mousePos;
        Ray ray = cam.ScreenPointToRay(mousePos);
        RaycastHit rch;
        bool isMain;

        bool rayHit = GetRayCastResult(ray, mousePos, out rch, out isMain);
        if (rayHit)
        {
            float angle = Vector3.Angle(rch.normal, Vector3.up);
            float camVal = 1 - Mathf.Clamp01(angle / 90.0f);
            switch (PhaseManager.phase)
            {
                case GamePhase.AddCream:
                case GamePhase.Flatten:
                    RotationSpeedManager.SetNewTargets(camVal, 1);
                    break;
            }
        }

        System.Action onHit = ()=> {
            UpdateToolVisual(true);
            if (PhaseOperation.instance != null) PhaseOperation.instance.OnToolUpdate(rch, ray, isMain);
            KnifePosUpdate(rch);
            successfullyInteracting = true;
        };
        System.Action onMiss = ()=> {
            successfullyInteracting = false;
            UpdateToolVisual(false);
            KnifeFailedToHitUpdate(mousePos);
            IcingMaker.EndCurrentIcing("Ray cast missed");
        };
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                if (ToolPrepare_ReturnReady(onToolPrepare))
                {

                    if (rayHit)
                    {
                        onHit?.Invoke();

                    }
                    else
                    {

                        onMiss?.Invoke();
                    }
                }
                else
                {
                    onMiss?.Invoke();
                }
                break;
            default:
            case GamePhase.Flatten:
                onToolPrepare(0);
                if (rayHit)
                {
                    onHit?.Invoke();

                }
                else
                {

                    onMiss?.Invoke();
                }
                break;
        }

        
    }

    internal Vector2 savedMousePos;
    public void NoKnifeUpdate()
    {
        ToolReset();
        successfullyInteracting = false;
        UpdateToolVisual(false);
        IcingMaker.EndCurrentIcing("No Input");
        targetTrans.position = cam.ScreenToWorldPoint(new Vector3(savedMousePos.x, savedMousePos.y, 2.75f));
        targetTrans.LookAt(cam.transform);
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                targetTrans.localScale = Vector3.one * 0.85f;
                break;
            case GamePhase.Flatten:
            default:
                targetTrans.localScale = Vector3.one * 0.7f;
                break;
        }
    }

    bool lastUpdateWasAMiss = true;
    Vector3 lastHitPoint;
    public void KnifePosUpdate(RaycastHit hit)
    {
        targetTrans.position = hit.point;
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                targetTrans.localScale = Vector3.one;
                break;
            case GamePhase.Flatten:
            default:
                targetTrans.localScale = Vector3.one;
                break;
        }
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                if (lastUpdateWasAMiss)
                    targetTrans.LookAt(this.transform.position + hit.normal);
                else
                    targetTrans.LookAt(this.transform.position + hit.normal, lastHitPoint - hit.point);// -peeler.GetTravelDirectionFromPeelBuilder());

                break;
            case GamePhase.Flatten:
            default:
                {
                    targetTrans.LookAt(this.transform.position + hit.normal);
                }
                break;
        }
        lastHitPoint = hit.point;
        lastUpdateWasAMiss = false;
    }

    public void KnifeFailedToHitUpdate(Vector2 mousePos)
    {
        switch (PhaseManager.phase)
        {
            case GamePhase.AddCream:
                targetTrans.localScale = Vector3.one * 0.85f;
                break;
            case GamePhase.Flatten:
            default:
                targetTrans.localScale = Vector3.one * 0.7f;
                break;
        }
        lastUpdateWasAMiss = true;
        targetTrans.position = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 2.75f));
        targetTrans.LookAt(cam.transform);

    }





    public const float WARM_UP_TIME = 0.85f;
    [Range(0, 1)]
    public float warmUpProgress;
    public bool ToolPrepare_ReturnReady(System.Action<float> onToolPrepare)
    {
        warmUpProgress += Time.deltaTime / (WARM_UP_TIME);
        onToolPrepare?.Invoke(Mathf.Clamp01(warmUpProgress));
        return warmUpProgress > 1;
    }
    public void ToolReset()
    {
        warmUpProgress = 0;
    }


    public bool GetRayCastResult(Ray ray,Vector2 touchPoint, out RaycastHit rch, out bool isMain)
    {
        isMain = true;
        if (Physics.Raycast(ray, out rch, 100, layerMask: PhaseManager.paintLayerMask))
        {
            if (MainGameManager.settings.drawDebugRays) Debug.DrawLine(ray.origin, rch.point, Color.blue, 1);
            
            return true;
        }
        else if (Physics.Raycast(ray, out rch, 100, layerMask: PhaseManager.paintLayerMask2))
        {
            if (MainGameManager.settings.drawDebugRays) Debug.DrawLine(ray.origin, rch.point, Color.cyan, 1);
            isMain = false;
            return true;
        }
        return false;
        {

            for (int i = 0; i < radiusGrowthIteration; i++)
            {
                if (Physics.SphereCast(ray, radiusGrowth*(i+1), out rch, 100, layerMask: PhaseManager.paintLayerMask))
                {
                    if (MainGameManager.settings.drawDebugRays) Debug.DrawLine(ray.origin, rch.point, Color.cyan, 1);
                    return true;
                }
            }

            return false;
        }
    }




}
public enum KnifeOutOfAreaMode
{
    Disappear=0,
    LastPosition=1,
    Follow=2,

}

