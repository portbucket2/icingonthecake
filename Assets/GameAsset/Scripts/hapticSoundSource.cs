﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class hapticSoundSource : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] audios = new AudioClip[6];
    public bool soundOn
    {
        get
        {
            return ToolBehaviourController.successfullyInteracting;
        }
    }
    public bool plaining
    {
        get
        {
            if (PhaseManager.CurrentPhaseController) return PhaseManager.CurrentPhaseController.phase == GamePhase.Flatten;
            return false;
        }
    }
    public static HardData<bool> _hapticEnabled;
    public static bool hapticEnabled
    {
        get
        {
            if (_hapticEnabled == null) _hapticEnabled = new HardData<bool>("SETTINGS_HAPTIC",true);
            return _hapticEnabled.value;
        }
        set
        {
            if (_hapticEnabled == null) _hapticEnabled = new HardData<bool>("SETTINGS_HAPTIC", true);
            _hapticEnabled.value = value;
        }
    }

    public static HardData<bool> _soundEnabled;
    public static bool soundEnabled
    {
        get
        {
            if (_soundEnabled == null) _soundEnabled = new HardData<bool>("SETTINGS_sound", true);
            return _soundEnabled.value;
        }
        set
        {
            if (_soundEnabled == null) _soundEnabled = new HardData<bool>("SETTINGS_sound", true);
            _soundEnabled.value = value;
        }
    }

    bool clipChanged;
    AudioClip currentClip;
    public Toggle hapticToggle;
    public Toggle soundToggle;

    bool isiPad;
    float screenRatio;
    public float maxVolume;

    
    //public bool dummyBool;
    // Start is called before the first frame update
    private void Awake()
    {
        screenRatio =(float) (Screen.width)/ (Screen.height);
        if ((screenRatio)> 0.7f)
        {
            isiPad = true;
        }
        else
        {
            isiPad = false;
        }
    }
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        audioSource.clip = audios[0];
        audioSource.Play();
        audioSource.volume = 0;
        currentClip = audios[0];

        hapticToggle.isOn = hapticEnabled;
        hapticToggle.onValueChanged.AddListener(OnToggle);

        soundToggle.isOn = soundEnabled;
        soundToggle.onValueChanged.AddListener(OnSoundToggle);


    }
    void OnToggle(bool newValue)
    {
        hapticEnabled = newValue;
    }
    private void OnDestroy()
    {
        hapticToggle.onValueChanged.RemoveListener(OnToggle);
        soundToggle.onValueChanged.RemoveListener(OnSoundToggle);
    }

    void OnSoundToggle(bool newValue)
    {
        soundEnabled = newValue;
    }
    //private void OnDestroy()
    //{
    //    
    //}
    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButton(0))
        //{
        //    soundOn = true;
        //}
        //else
        //{
        //    soundOn = false;
        //}

        if (soundOn && soundEnabled)
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, maxVolume , Time.deltaTime *8f);
        }
        else
        {
            audioSource.volume = Mathf.Lerp(audioSource.volume, 0, Time.deltaTime *8f);
        }
        if (Input.GetMouseButtonDown(0))
        {
            audioSource.Play();
        }

        AssignAudioClip();

        if (currentClip != audioSource.clip)
        {
            currentClip = audioSource.clip;
            audioSource.Play();
        }

        //if (hapticToggle.isOn)
        //{
        //    hapticEnabled = true;
        //    //dummyBool = true;
        //}
        //else
        //{
        //    hapticEnabled = false;
        //    //dummyBool = false;
        //}


        //audioSource.Play();
    }

    void AssignAudioClip()
    {
        if (plaining)
        {
            if (hapticEnabled)
            {
                if(isiPad)
                    audioSource.clip = audios[5];
                else
                    audioSource.clip = audios[1];
            }
            else
            {
                audioSource.clip = audios[3];
            }
            

            //currentClip = audios[1];
        }
        else
        {
            if (hapticEnabled)
            {
                if (isiPad)
                    audioSource.clip = audios[4];
                else
                    audioSource.clip = audios[0];
            }
            else
            {
                audioSource.clip = audios[2];
            }
            
            //currentClip = audios[0];
            //if (!clipChanged)
            //{
            //    audioSource.Play();
            //    clipChanged = true;
            //}
        }
        //audioSource.Play();
    }

    //public void ToggleHaptic(bool haptic)
    //{
    //    haptic = hapticEnabled;
    //}
}
