﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;


public class ThreadHandler
{
    public bool isBusy { get; private set; }
    public bool scheduledTaskComplete { get; private set; }

    public ThreadHandler(MonoBehaviour mono)
    {
        mono.StartCoroutine(Routine());
    }
    IEnumerator Routine()
    {

        while (true)
        {
            yield return null;
            if (scheduledTaskComplete)
            {
                OnComplete();
                scheduledTaskComplete = false;
                isBusy = false;
            }
            if (!isBusy)
            {
                Scheduler();
            }
        }
    }
    private void Scheduler()
    {
        if (ScheduleConditionMet())
        {
            isBusy = true;
            scheduledTaskComplete = false;
            thread = new Thread(ScheduledTask);
            thread.Start();
        }
    }

    private Thread thread;
    private void ScheduledTask()
    {
        Task();
        scheduledTaskComplete = true;
    }

    protected virtual bool ScheduleConditionMet()
    {
        throw new System.Exception("Must override");
    }
    protected virtual void Task()
    {

    }
    protected virtual void OnComplete()
    {

    }
}


public class ThreadHandler_GalleryVColRead : ThreadHandler
{
    public ThreadHandler_GalleryVColRead(GalleryManager gm) : base(gm)
    {
        this.gm = gm;
    }
    GalleryManager gm;
    string vcolReadPath;
    GalleryItemVcolorInfo vcolReadData;
    protected override bool ScheduleConditionMet()
    {
        if (gm.vcolDataKeeper.Count < gm.gidList.Count)
        {

            for (int i = 0; i < gm.gidList.Count; i++)
            {
                vcolReadPath = gm.gidList[i].vcolDataPath;
                if (!gm.vcolDataKeeper.ContainsKey(vcolReadPath))
                {
                    return true;
                }
            }
        }
        return false;
    }


    protected override void OnComplete()
    {
        if (vcolReadData != null)
        {
            //Debug.LogFormat("<color='cyan'>Thread Read Complete {0}</color>", vcolReadPath);
            if (!gm.vcolDataKeeper.ContainsKey(vcolReadPath))
            {
                gm.vcolDataKeeper.Add(vcolReadPath, vcolReadData);
            }
            //Debug.LogFormat("<color='cyan'>vcolCount {0}</color>", gm.vcolDataKeeper.Count);
            vcolReadData = null;
        }
    }

    protected override void Task()
    {
        vcolReadData = GXMLSerializer.Load<GalleryItemVcolorInfo>(vcolReadPath);
    }
}

//public class ThreadHandler_GalleryVColRead2
//{
//    public bool isBusy { get; private set; }


//    Thread threadReadVcol;
//    string vcolReadPath;
//    GalleryItemVcolorInfo vcolReadData;

//    void ThreadScheduler_ReadVCol()
//    {
//        if (vcolReadData != null)
//        {
//            Debug.LogFormat("<color='cyan'>Thread Read Complete {0}</color>", vcolReadPath);
//            if (!vcolDataKeeper.ContainsKey(vcolReadPath))
//            {
//                vcolDataKeeper.Add(vcolReadPath, vcolReadData);
//            }
//            Debug.LogFormat("<color='cyan'>vcolCount {0}</color>", vcolDataKeeper.Count);
//            vcolReadData = null;
//        }

//        if (vcolDataKeeper.Count < gidList.Count)
//        {

//            for (int i = 0; i < gidList.Count; i++)
//            {
//                vcolReadPath = gidList[i].vcolDataPath;
//                if (!vcolDataKeeper.ContainsKey(vcolReadPath))
//                {
//                    Debug.LogFormat("<color='yellow'>Thread Load {0}</color>", vcolReadPath);
//                    threadReadVcol = new Thread(ReadVcolorThreadWork);
//                    threadReadVcol.Start();
//                    isBusy = true;
//                    return;
//                }
//            }
//        }
//    }


//    void ReadVcolorThreadWork()
//    {
//        vcolReadData = GXMLSerializer.Load<GalleryItemVcolorInfo>(vcolReadPath);
//        isBusy = false;
//    }
//}

public class ThreadHandler_GalleryVColWrite : ThreadHandler
{
    public ThreadHandler_GalleryVColWrite(GalleryManager gm) : base(gm)
    {
        this.gm = gm;
    }
    GalleryManager gm;
    GalleryItemVcolorInfo vcolDataToSave;
    GalleryItemData gidTarget;

    public void ScheduleNewSave(GalleryItemData gid, GalleryItemVcolorInfo vcolData)
    {
        gidTarget = gid;
        vcolDataToSave = vcolData;

        //Debug.LogFormat("<color='cyan'>Scheduled to save {0}</color>", gidTarget.vcolDataPath);
    }
    protected override bool ScheduleConditionMet()
    {
        return vcolDataToSave != null;
    }


    protected override void OnComplete()
    {
        //Debug.LogFormat("<color='cyan'>Save Complete {0}</color>", gidTarget.vcolDataPath);
        GalleryManager.GalleryItemCount++;
        gm.gidList.Add(gidTarget);
        gm.vcolDataKeeper.Add(gidTarget.vcolDataPath, vcolDataToSave);
        gm.Refresh();
        gidTarget = null;
        vcolDataToSave = null;
    }

    protected override void Task()
    {

        GXMLSerializer.Save<GalleryItemVcolorInfo>(vcolDataToSave, gidTarget.vcolDataPath);
    }
}
//public class ThreadHandler_GalleryVColWrite2
//{
//    public bool isBusy { get; private set; }

//    Thread threadWriteVcol;
//    bool readyToIncrement = false;
//    string vcolDatapath;
//    GalleryItemVcolorInfo vcolDataToSave;
//    GalleryItemData gidTargetForVcolSave;


//    void ThreadScheduler_WriteVCol()
//    {
//        if (readyToIncrement)
//        {
//            GalleryItemCount++;
//            gidList.Add(gidTargetForVcolSave);
//            vcolDataKeeper.Add(gidTargetForVcolSave.vcolDataPath, vcolDataToSave);
//            Refresh();
//            gidTargetForVcolSave = null;
//            vcolDataToSave = null;
//            readyToIncrement = false;
//        }
//        if (vcolDataToSave != null)
//        {
//            threadWriteVcol = new Thread(WriteVcolorThreadWork);
//            threadWriteVcol.Start();
//            isBusy = true;
//            readyToIncrement = false;
//        }
//    }



//    void WriteVcolorThreadWork()
//    {
//        GXMLSerializer.Save<GalleryItemVcolorInfo>(vcolDataToSave, vcolDatapath);
//        readyToIncrement = true;
//        isBusy = false;
//    }
//}