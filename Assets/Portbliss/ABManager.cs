﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;


public enum ABtype
{
    //AD_AGRESSION = 0,
    //LEVEL_ORDER = 1,
    //KNIFE_BEHAVIOUR = 2,
    //SKIP_OPTION = 3,
    //FOCUS_SPEED = 4,
    //First_Ad = 0,
    //Ad_Frequency = 1,

    //Match_Screen = 2,
    //Level_Order = 3,

    Ad_Config = 4,
}
public static class ABManager
{
    public static bool isDataFetchComplete { get; private set; }
    public static void SetFetchComplete() { isDataFetchComplete = true; }
    private static void Init()
    {
        _allSettings = new Dictionary<ABtype, HardAB>();

        //_allSettings.Add(ABtype.AD_AGRESSION, new HardAB("peel_ad_intensity_type"));
        //_allSettings.Add(ABtype.LEVEL_ORDER, new HardAB("level_order"));
        //_allSettings.Add(ABtype.KNIFE_BEHAVIOUR, new HardAB("knife"));
        //_allSettings.Add(ABtype.SKIP_OPTION, new HardAB("skip_option"));
        //_allSettings.Add(ABtype.FOCUS_SPEED, new HardAB("focus_speed"));
        //_allSettings.Add(ABtype.First_Ad, new HardAB("first_ad"));
        //_allSettings.Add(ABtype.Ad_Frequency, new HardAB("ad_frequency"));
        _allSettings.Add(ABtype.Ad_Config, new HardAB(ABtype.Ad_Config, "ad_config"));
        //_allSettings.Add(ABtype.Match_Screen, new HardAB("match"));
        //_allSettings.Add(ABtype.Level_Order, new HardAB("order"));
    }

    private static Dictionary<ABtype, HardAB> _allSettings;
    public static Dictionary<ABtype, HardAB> allSettings
    {
        get
        {
            if (_allSettings == null) Init();
            return _allSettings;
        }
    }

    public static string GetValueString(ABtype type)
    {
        return allSettings[type].GetValue();
    }

    public static int GetValueInt(ABtype type)
    {
        var s = allSettings[type].GetValue();
        int result = 0;
        if (!int.TryParse(s, out result))
        {
            allSettings[type].ForceSetValue("0");
        }
        return result;
    }

    public static float GetValueFloat(ABtype type)
    {
        var s = allSettings[type].GetValue();
        float result = 0f;
        if (!float.TryParse(s, out result))
        {
            allSettings[type].ForceSetValue("0");
        }
        return result;
    }

    public static bool GetValueBool(ABtype type)
    {
        var s = allSettings[type].GetValue();
        bool result = false;
        if (!bool.TryParse(s, out result))
        {
            allSettings[type].ForceSetValue("false");
        }
        return result;
    }


    public static HardAB GetABAccess(ABtype type)
    {
        return allSettings[type];
    }


    public class HardAB
    {
        ABtype type;
        string id;
        string key;
        HardData<string> data;
        bool isVolatile;

        public HardAB(ABtype type, string id, string editorDefaultValue = "", bool isVolatile = false)
        {
            this.isVolatile = isVolatile;
            this.type = type;
            this.id = id;
            key = string.Format("AB_KEY_{0}", id);
#if UNITY_EDITOR
            data = new HardData<string>(key, editorDefaultValue);
#else
            data = new HardData<string>(key, "");
#endif
        }

        public string GetID()
        {
            return id;
        }
        public void Assign_IfUnassigned(string abStringValue)
        {
            if (!isVolatile)
            {
                if (data.value == "" && abStringValue !="")
                {
                    data.value = abStringValue;
                    AnalyticsAssistant.LogABTesting(id, data.value);
                }
            }
            else
            {
                data.value = abStringValue;
            }
        }

        public void ForceSetValue(string newValue)
        {
            data.value = newValue;
            if(newValue!="" && !isVolatile)AnalyticsAssistant.LogABTesting(id, data.value);
        }
        public string GetValue()
        {
            return data.value;
        }
    }
}

//public static class ABManager2 
//{
//    private static void Init()
//    {
//        _allSettings = new Dictionary<ABtype,HardAB>();

//        //_allSettings.Add(ABtype.AD_AGRESSION, new HardAB("peel_ad_intensity_type"));
//        //_allSettings.Add(ABtype.LEVEL_ORDER, new HardAB("level_order"));
//        //_allSettings.Add(ABtype.KNIFE_BEHAVIOUR, new HardAB("knife"));
//        //_allSettings.Add(ABtype.SKIP_OPTION, new HardAB("skip_option"));
//        //_allSettings.Add(ABtype.FOCUS_SPEED, new HardAB("focus_speed"));
//        //_allSettings.Add(ABtype.First_Ad, new HardAB("first_ad"));
//        //_allSettings.Add(ABtype.Ad_Frequency, new HardAB("ad_frequency"));
//        _allSettings.Add(ABtype.Ad_Config, new HardAB("ad_config"));
//        //_allSettings.Add(ABtype.Match_Screen, new HardAB("match"));
//        //_allSettings.Add(ABtype.Level_Order, new HardAB("order"));
//    }


//    private static Dictionary<ABtype,HardAB> _allSettings;
//    public static Dictionary<ABtype, HardAB> allSettings
//    {
//        get
//        {
//            if (_allSettings == null) Init();
//            return _allSettings;
//        }
//    }

//    public static int GetValue(ABtype type)
//    {
//        return allSettings[type].GetValue();
//    }


//    public class HardAB
//    {
//        string id;
//        string key;
//        HardData<int> data;

//        public HardAB(string id, int editorDefaultValue = -1)
//        {
//            this.id = id;
//            key = string.Format("AB_KEY_{0}", id);
//#if UNITY_EDITOR
//            data = new HardData<int>(key, editorDefaultValue);
//#else
//            data = new HardData<int>(key, -1);
//#endif
//        }


//        public string GetID()
//        {
//            return id;
//        }
//        public void Assign_IfUnassigned(int i)
//        {
//            if (data.value == -1)
//            {
//                data.value = i;
//                //if (data.value != -1) AnalyticsAssistant.LogABTesting(id, data.value);
//            }
//        }

//        public int GetValue()
//        {
//            if (data.value <= -1)
//            {
//                data.value = 0;
//                //AnalyticsAssistant.LogABTesting(id, data.value);
//                Debug.LogFormat("AB value was set to default for key <{0}>", id);
//            }
//            return data.value;
//        }

//    }


//}
