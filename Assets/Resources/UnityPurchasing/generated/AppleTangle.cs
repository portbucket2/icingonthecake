#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("lpGSl5OQlfm0rpCWk5GTmpGSl5Pxxs/Kws3AxoPMzYPXy8rQg8DG0YdBSHIU03ys5kKEaVLO205EFrS0+gSmqt+04/WyvddwFCiAmOQAdsyQlfmTwZKok6qloPanpbCh9vCSsIPCzceDwMbR18rFysDC18rMzYPT08/Gg/HMzNeD4OKTvbSuk5WTl5GupaqJJeslVK6ioqamo6AhoqKj/yi6Kn1a6M9WpAiBk6FLu51b86pwkyGnGJMhoAADoKGioaGioZOuparXysXKwMLXxoPB2oPCzdqD08LR15OypaD2p6mwqeLT08/Gg+rNwI2Sg8zFg9fLxoPXy8bNg8LT08/KwMKVOu+O2xROLzh/UNQ4UdVx1JPsYoPg4pMhooGTrqWqiSXrJVSuoqKiHVfQOE1xx6xo2uyXewGdWttcyGvNx4PAzM3HytfKzM3Qg8zFg9bQxjY92a8H5Cj4d7WUkGhnrO5tt8py2oPC0NDWzsbQg8LAwMbT18LNwMaMkyJgpauIpaKmpqShoZMiFbkiEI+DwMbR18rFysDC18aD08zPysDaz8aD6s3AjZKFk4eloPanqLC+4tOFk4eloPanqLC+4tPTz8aD4MbR16uIpaKmpqShorW9y9fX09CZjIzUxCyrF4NUaA+Pg8zTFZyiky8U4GzZkyGi1ZOtpaD2vqyiolynp6ChomPAkNRUmaSP9Uh5rIKteRnQuuwW5t2878jzNeIqZ9fBqLMg4iSQKSKloPa+rae1p7eIc8rkN9WqXVfILtPPxoPgxtHXysXKwMLXyszNg+LWIaKjpaqJJeslVMDHpqKTIlGTiaUSk/tP+aeRL8sQLL59xtBcxP3GH6w+nlCI6ou5a11tFhqtev2/dWieCADSMeTw9mIMjOIQW1hA025FAO+kT96aICjwg3CbZxIcOeypyFyIXyO3iHPK5DfVql1XyC6N4wVU5O7ciSXrJVSuoqKmpqOTwZKok6qloPbRwsDXysDGg9DXwtfGzsbN19CNk9fLzNHK19qStZO3paD2p6CwruLTepXcYiT2egQ6GpHhWHt20j3dAvHKxcrAwtfKzM2D4tbXy8zRytfaktziCztacmnFP4fIsnMAGEe4iWC8p6WwofbwkrCTsqWg9qepsKni09MLf92BlmmGdnqsdch3AYeAslQCD8eWgLbotvq+EDdUVT89bPMZYvvzq/2TIaKypaD2voOnIaKrkyGip5MWmQ5XrK2jMagSgrWN13afrnjBtep71TyQt8YC1DdqjqGgoqOiACGitZO3paD2p6CwruLT08/Gg/HMzNfU1I3C09PPxo3AzM6MwtPTz8bAwo3jBVTk7tyr/ZO8paD2voCnu5O1LNAiw2W4+KqMMRFb5+tTw5s9tla8JiAmuDqe5JRRCjjjLY93EjOxexS4HjDhh7GJZKy+Fe4//cBr6CO0pZOspaD2vrCiolynppOgoqJck76ehcSDKZDJVK4hbH1IAIxa8Mn4x6ajoCGirKOTIaKpoSGioqNHMgqqarrRVv6tdtz8OFGGoBn2LO7+rlK8Mni95PNIpk792ieOSJUB9O/2T8HPxoPQ18LNx8LRx4PXxtHO0IPC8wkpdnlHX3OqpJQT1taC");
        private static int[] order = new int[] { 7,38,53,52,8,45,6,47,16,47,39,47,41,48,48,50,43,18,40,37,44,52,39,35,52,28,57,28,30,43,34,57,39,52,51,39,38,46,45,58,47,47,53,54,44,52,58,51,51,52,57,55,52,59,54,56,56,58,58,59,60 };
        private static int key = 163;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
