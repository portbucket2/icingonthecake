﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerChooser : MonoBehaviour
{
    public UnityEngine.UI.Image itemImage;
    public MarkerChoice[] mchoices;
    public void ShowMarkerOnTransform(Transform tr, Color col)
    {
        this.transform.SetParent(tr);
        this.transform.localPosition = Vector3.zero;

        float lowestDist = float.MaxValue;
        int chosenIndex =- 1;
        for (int i = 0; i < mchoices.Length; i++)
        {
            float thisColDist = mchoices[i].ColDistance(col);
            if (thisColDist < lowestDist)
            {
                lowestDist = thisColDist;
                chosenIndex = i;
            }
        }

        //Color finalCol = new Color(0,0,0,0);
        //float weights = 0;
        //for (int i = 0; i < mchoices.Length; i++)
        //{
        //    float weight = (1 / mchoices[i].distance);
        //    weights += weight;
        //    finalCol += mchoices[i].itemColor *weight;
        //}
        //itemImage.color = finalCol / weights;
        itemImage.color = mchoices[chosenIndex].itemColor ;
    }
}

[System.Serializable]
public class MarkerChoice
{

    public Color bgColor;
    public Color itemColor;
    internal float distance;
    public float ColDistance(Color colBG)
    {
        float r = bgColor.r - colBG.r;
        float g = bgColor.g - colBG.g;
        float b = bgColor.b - colBG.b;
        float a = bgColor.a - colBG.a;

        distance = Mathf.Sqrt(  r * r + g * g + b * b + a * a);
        if (distance == 0) distance = 0.0000001f;
        return distance;
    }
    //public  SetActive(bool isEnabled)
    //{
    //    go.SetActive(isEnabled);
    //}
}
