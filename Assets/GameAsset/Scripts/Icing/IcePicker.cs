﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class IcePicker : MonoBehaviour
{
    public static IcePicker instance;
    public static int currentChannelIndex;
    public static ColChannel currentChannel { get { return (ColChannel)currentChannelIndex; } }
    public static Material currentMat { get { return instance ? instance.icingPhase ? instance.icingPhase.mats[currentChannelIndex]: null :null; } }

    public GameObject moduleObject;
    public List<Button> buttons;
    public List<Image> buttonImages;
    public Transform marker;
    public MarkerChooser markerChooser;
    void Awake()
    {
        //Debug.Log("IcePicker Awake");
        instance = this;
        for (int i = 0; i < buttons.Count; i++)
        {
            int index = i;
            buttons[i].onClick.AddListener(()=> { MakeChoice(index); });
        }
        PhaseController.onPhaseStart_StaticEvent += OnPhaseStart;
    }
    //void OnEnable()
    //{
    //}
    void  OnDestroy()
    {
        PhaseController.onPhaseStart_StaticEvent -= OnPhaseStart;
    }
    void MakeChoice(int i)
    {
        currentChannelIndex = i;
        //marker.SetParent(buttons[i].transform);
        //marker.localPosition = Vector3.zero;
        markerChooser.ShowMarkerOnTransform(buttons[i].transform, buttonImages[i].color);
    }
    
    public PhaseController_Icing icingPhase;
    List<Material> mats { get { if (icingPhase) return icingPhase.mats; else return null; } }
    void OnPhaseStart(PhaseController startingPhase)
    {
        //Debug.LogFormat("<color='yellow'>IcePicker Load: {0}</color>",startingPhase.phase);
        icingPhase = startingPhase as PhaseController_Icing;

        moduleObject.SetActive(icingPhase && icingPhase.cols.Count>1);
        if (icingPhase)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                if (i >= mats.Count)
                {
                    buttons[i].gameObject.SetActive(false);
                }
                else
                {
                    buttons[i].gameObject.SetActive(true);
                    buttonImages[i].color = mats[i].GetColor("_Color");
                }
            }
            MakeChoice(LevelPrefabManager.currentLevel.levelDefinition.initialColorIndex);
        }
    }
    
}
