﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

//public class TrashManager : MonoBehaviour
//{
//    public static TrashManager instance;

//    [SerializeField] bool dontEjectWithPhysics = false;
//    float ejectForce = 0.75f;
//    [SerializeField] Vector3 targetRelativeSpeed = Vector3.zero;
//    [SerializeField] Transform floor;
//    [SerializeField] Transform trashDiscardPoint;
//    //[SerializeField] float windForce = 1;


//    [Header("Wind Settings")]
//    //[SerializeField] float windGap = 2.5f;
//    [SerializeField] Transform windDirectionKeeper;
//    public WindSetting[] windSettings;
//    public WindSetting finalWindSettings;


//    ChancedList<WindSetting> windTypeSelector = new ChancedList<WindSetting>();
//    private void Awake()
//    {
//        instance = this ;
//        foreach (var item in windSettings)
//        {
//            windTypeSelector.Add(item, item.chance);
//        }

//        //StartCoroutine(BreakWind());
//        StartCoroutine(SuckIn());
//    }
//    private void Start()
//    {
//        //LevelPrefabManager.currentLevel.onPreDestroy += OnPreDestroy;
//    }
//    // Update is called once per frame
//    void Update()
//    {
//        this.transform.Rotate( (RotationSpeedManager.instance.ChosenSpeed- targetRelativeSpeed) * Time.deltaTime);
//    }


//    WindSetting currentSetting;
//    float endTime;
//    float duration;
    //public void LoadWindSetting(WindSetting settings)
    //{
    //    duration = settings.GetNextDuration();// windDuration * (1+windDuration_deviation);
    //    endTime = Time.time + duration;
    //    foreach (var item in oldPeels)
    //    {
    //        if (item.rgbd)
    //        {
    //            item.currentWindValue = settings.GetNextForceStrength();
    //            item.currentWindEndTime = Time.time + duration;// Handy.Deviate(windDuration, windDuration_deviation);
    //        }
    //    }
    //    currentSetting = settings;
    //}
    //IEnumerator BreakWind()
    //{
    //    while (true)
    //    {
    //        if(currentSetting == null)LoadWindSetting(windTypeSelector.Roll());

    //        while (Time.time < endTime)
    //        {
    //            //Debug.Log(1/Time.deltaTime);
    //            foreach (var item in oldPeels)
    //            {
    //                if (item.rgbd && Time.time<item.currentWindEndTime)
    //                {
    //                    item.rgbd.AddForce(windDirectionKeeper.forward * item.currentWindValue*Time.deltaTime, ForceMode.Acceleration);
    //                }
    //            }
    //            yield return null;
    //        }
    //        currentSetting = null;
    //        yield return null;
    //    }
    //}
    //IEnumerator SuckIn()
    //{
    //    while (true)
    //    {
    //        foreach (var item in oldPeels)
    //        {
    //            if (Time.time < item.ejectTime + 1.5f)
    //            {
    //                float maxY = item.ejectYpos - floor.position.y;
    //                float y = item.root.transform.position.y - floor.position.y;

    //                float scaleM = Mathf.Lerp(0.6f, 1f, Mathf.Clamp01(y / maxY));
    //                if (item.root.localScale.sqrMagnitude > scaleM * scaleM)
    //                    item.root.localScale = Vector3.one * scaleM;
    //            }
    //            else if(Time.time> item.ejectTime+2)
    //            {
    //                if (!item.rgbd.isKinematic) {
    //                    item.rgbd.isKinematic = true;
    //                    item.UnloadPoolColliders();
    //                }

    //                float suckRate = Mathf.Lerp(0.02f, 0.2f, Mathf.Clamp01(item.fibers.Count / 350));
    //                item.root.localPosition += new Vector3(0, -suckRate * Time.deltaTime, 0);
    //            }

    //        }

    //        yield return null;
    //    }
    //}
    //private void FixedUpdate()
    //{
    //    for (int i = oldPeels.Count-1; i >=0; i--)
    //    {
    //        if (trashDiscardPoint && oldPeels[i].root.position.y < trashDiscardPoint.position.y)
    //        {
    //            oldPeels[i].TrashItems();
    //            oldPeels.RemoveAt(i);
    //        }
    //    }
    //}
    //public static void FinalBlow()
    //{
    //    if (instance)
    //    {
    //        instance.LoadWindSetting(instance.finalWindSettings);
    //    }
    //}

    //List<IcingBuilder> oldPeels = new List<IcingBuilder>();
    //public void AddPeelToTrash(IcingBuilder peelBuilder)
    //{
    //    peelBuilder.EjectPeel(disablePhysics: dontEjectWithPhysics,ejectForce: ejectForce);
    //    peelBuilder.root.SetParent(PhaseManager.instance.transform);

    //    if (currentSetting!=null)
    //    {
    //        peelBuilder.currentWindValue = currentSetting.GetNextForceStrength();
    //        peelBuilder.currentWindEndTime = Time.time + duration;
    //    }
    //   // oldPeels.Add(peelBuilder);
    //}
    //private void OnDestroy()
    //{
    //    foreach (var peel in oldPeels)
    //    {
    //        peel.TrashItems();
    //    }
    //}

//}
//public enum WindType
//{
//    BREEZE,
//    LIGHT,
//    HEAVY,
//    GUST,
//}

//[System.Serializable]
//public class WindSetting
//{
//    public string name { get { return type.ToString(); } }
//    public WindType type;
//    [Range(0, 1)] public float chance = 1;
//    public float forceStrength = 1;
//    [Range(0, 100)] public float strengthDeviation = 0;
//    public float duration = 0.5f;
//    [Range(0, 100)]  public float durationDeviation = 20;
//    public float GetNextForceStrength()
//    {
//        return Handy.Deviate(forceStrength,strengthDeviation/100);
//    }
//    public float GetNextDuration()
//    {
//        return Handy.Deviate(duration, durationDeviation / 100);
//    }
//    //public float GetMaxPossibleDuration()
//    //{
//    //    return Handy.Deviate(duration, durationDeviation / 100);
//    //}
//}